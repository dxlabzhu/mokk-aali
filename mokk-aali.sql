-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2020. Sze 30. 13:38
-- Kiszolgáló verziója: 10.4.8-MariaDB
-- PHP verzió: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `mokk-aali`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT 0,
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'comment',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2017-10-31 08:28:29', '2017-10-31 08:28:29', 'Üdvözlet! Ez egy hozzászólás.\nA hozzászólások moderálásához, szerkesztéséhez és törléséhez látogassunk el a honlapunk vezérlőpultjának\nHozzászólások menüpontjához.\nA hozzászólok avatarját a <a href=\"https://gravatar.com\">Gravatar</a> biztosítja.', 0, 'post-trashed', '', 'comment', 0, 0);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_db7_forms`
--

CREATE TABLE `wp_db7_forms` (
  `form_id` bigint(20) NOT NULL,
  `form_post_id` bigint(20) NOT NULL,
  `form_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `form_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT 1,
  `link_rating` int(11) NOT NULL DEFAULT 0,
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_newsletter`
--

CREATE TABLE `wp_newsletter` (
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `token` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `language` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `status` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'S',
  `id` int(11) NOT NULL,
  `profile` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated` int(11) NOT NULL DEFAULT 0,
  `last_activity` int(11) NOT NULL DEFAULT 0,
  `followup_step` tinyint(4) NOT NULL DEFAULT 0,
  `followup_time` bigint(20) NOT NULL DEFAULT 0,
  `followup` tinyint(4) NOT NULL DEFAULT 0,
  `surname` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sex` char(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'n',
  `feed_time` bigint(20) NOT NULL DEFAULT 0,
  `feed` tinyint(4) NOT NULL DEFAULT 0,
  `referrer` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `ip` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `wp_user_id` int(11) NOT NULL DEFAULT 0,
  `http_referer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `geo` tinyint(4) NOT NULL DEFAULT 0,
  `country` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `region` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `city` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `bounce_type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `bounce_time` int(11) NOT NULL DEFAULT 0,
  `unsub_email_id` int(11) NOT NULL DEFAULT 0,
  `unsub_time` int(11) NOT NULL DEFAULT 0,
  `list_1` tinyint(4) NOT NULL DEFAULT 0,
  `list_2` tinyint(4) NOT NULL DEFAULT 0,
  `list_3` tinyint(4) NOT NULL DEFAULT 0,
  `list_4` tinyint(4) NOT NULL DEFAULT 0,
  `list_5` tinyint(4) NOT NULL DEFAULT 0,
  `list_6` tinyint(4) NOT NULL DEFAULT 0,
  `list_7` tinyint(4) NOT NULL DEFAULT 0,
  `list_8` tinyint(4) NOT NULL DEFAULT 0,
  `list_9` tinyint(4) NOT NULL DEFAULT 0,
  `list_10` tinyint(4) NOT NULL DEFAULT 0,
  `list_11` tinyint(4) NOT NULL DEFAULT 0,
  `list_12` tinyint(4) NOT NULL DEFAULT 0,
  `list_13` tinyint(4) NOT NULL DEFAULT 0,
  `list_14` tinyint(4) NOT NULL DEFAULT 0,
  `list_15` tinyint(4) NOT NULL DEFAULT 0,
  `list_16` tinyint(4) NOT NULL DEFAULT 0,
  `list_17` tinyint(4) NOT NULL DEFAULT 0,
  `list_18` tinyint(4) NOT NULL DEFAULT 0,
  `list_19` tinyint(4) NOT NULL DEFAULT 0,
  `list_20` tinyint(4) NOT NULL DEFAULT 0,
  `list_21` tinyint(4) NOT NULL DEFAULT 0,
  `list_22` tinyint(4) NOT NULL DEFAULT 0,
  `list_23` tinyint(4) NOT NULL DEFAULT 0,
  `list_24` tinyint(4) NOT NULL DEFAULT 0,
  `list_25` tinyint(4) NOT NULL DEFAULT 0,
  `list_26` tinyint(4) NOT NULL DEFAULT 0,
  `list_27` tinyint(4) NOT NULL DEFAULT 0,
  `list_28` tinyint(4) NOT NULL DEFAULT 0,
  `list_29` tinyint(4) NOT NULL DEFAULT 0,
  `list_30` tinyint(4) NOT NULL DEFAULT 0,
  `list_31` tinyint(4) NOT NULL DEFAULT 0,
  `list_32` tinyint(4) NOT NULL DEFAULT 0,
  `list_33` tinyint(4) NOT NULL DEFAULT 0,
  `list_34` tinyint(4) NOT NULL DEFAULT 0,
  `list_35` tinyint(4) NOT NULL DEFAULT 0,
  `list_36` tinyint(4) NOT NULL DEFAULT 0,
  `list_37` tinyint(4) NOT NULL DEFAULT 0,
  `list_38` tinyint(4) NOT NULL DEFAULT 0,
  `list_39` tinyint(4) NOT NULL DEFAULT 0,
  `list_40` tinyint(4) NOT NULL DEFAULT 0,
  `profile_1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_3` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_4` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_5` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_6` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_7` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_8` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_9` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_10` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_11` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_12` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_13` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_14` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_15` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_16` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_17` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_18` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_19` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_20` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `test` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `wp_newsletter`
--

INSERT INTO `wp_newsletter` (`name`, `email`, `token`, `language`, `status`, `id`, `profile`, `created`, `updated`, `last_activity`, `followup_step`, `followup_time`, `followup`, `surname`, `sex`, `feed_time`, `feed`, `referrer`, `ip`, `wp_user_id`, `http_referer`, `geo`, `country`, `region`, `city`, `bounce_type`, `bounce_time`, `unsub_email_id`, `unsub_time`, `list_1`, `list_2`, `list_3`, `list_4`, `list_5`, `list_6`, `list_7`, `list_8`, `list_9`, `list_10`, `list_11`, `list_12`, `list_13`, `list_14`, `list_15`, `list_16`, `list_17`, `list_18`, `list_19`, `list_20`, `list_21`, `list_22`, `list_23`, `list_24`, `list_25`, `list_26`, `list_27`, `list_28`, `list_29`, `list_30`, `list_31`, `list_32`, `list_33`, `list_34`, `list_35`, `list_36`, `list_37`, `list_38`, `list_39`, `list_40`, `profile_1`, `profile_2`, `profile_3`, `profile_4`, `profile_5`, `profile_6`, `profile_7`, `profile_8`, `profile_9`, `profile_10`, `profile_11`, `profile_12`, `profile_13`, `profile_14`, `profile_15`, `profile_16`, `profile_17`, `profile_18`, `profile_19`, `profile_20`, `test`) VALUES
('', 'doxa84@hotmail.com', 'b95d6c98a7', '', 'C', 1, NULL, '2020-03-10 09:54:34', 0, 0, 0, 0, 0, '', 'n', 0, 0, '', '', 0, '', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_newsletter_emails`
--

CREATE TABLE `wp_newsletter_emails` (
  `id` int(11) NOT NULL,
  `language` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `message` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `message2` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` enum('new','sending','sent','paused') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'new',
  `total` int(11) NOT NULL DEFAULT 0,
  `last_id` int(11) NOT NULL DEFAULT 0,
  `sent` int(11) NOT NULL DEFAULT 0,
  `track` int(11) NOT NULL DEFAULT 0,
  `list` int(11) NOT NULL DEFAULT 0,
  `type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `query` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `editor` tinyint(4) NOT NULL DEFAULT 0,
  `sex` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `theme` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `message_text` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `preferences` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `send_on` int(11) NOT NULL DEFAULT 0,
  `token` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `options` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `private` tinyint(1) NOT NULL DEFAULT 0,
  `click_count` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `version` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `open_count` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `unsub_count` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `error_count` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `stats_time` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_newsletter_sent`
--

CREATE TABLE `wp_newsletter_sent` (
  `email_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `open` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `time` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `error` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `ip` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_newsletter_stats`
--

CREATE TABLE `wp_newsletter_stats` (
  `id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL DEFAULT 0,
  `email_id` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `ip` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_newsletter_user_logs`
--

CREATE TABLE `wp_newsletter_user_logs` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `ip` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `source` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `data` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/mokk-aali.hu', 'yes'),
(2, 'home', 'http://localhost/mokk-aali.hu', 'yes'),
(3, 'blogname', 'MOKK - Adatkutató Alintézet', 'yes'),
(4, 'blogdescription', '', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'doxa84@hotmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '3', 'yes'),
(23, 'date_format', 'Y F j', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'Y-m-d H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:17:{i:0;s:30:\"advanced-custom-fields/acf.php\";i:1;s:33:\"classic-editor/classic-editor.php\";i:2;s:36:\"contact-form-7/wp-contact-form-7.php\";i:3;s:42:\"contact-form-cfdb7/contact-form-cfdb-7.php\";i:4;s:43:\"custom-post-type-ui/custom-post-type-ui.php\";i:5;s:23:\"debug-bar/debug-bar.php\";i:6;s:37:\"disable-comments/disable-comments.php\";i:7;s:32:\"duplicate-page/duplicatepage.php\";i:8;s:45:\"enable-media-replace/enable-media-replace.php\";i:9;s:23:\"loco-translate/loco.php\";i:10;s:37:\"rocket-lazy-load/rocket-lazy-load.php\";i:11;s:47:\"show-current-template/show-current-template.php\";i:12;s:35:\"simply-show-ids/simply-show-ids.php\";i:13;s:27:\"theme-check/theme-check.php\";i:14;s:24:\"wordpress-seo/wp-seo.php\";i:15;s:31:\"wp-migrate-db/wp-migrate-db.php\";i:16;s:23:\"wp-smushit/wp-smush.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'themeplate', 'yes'),
(41, 'stylesheet', 'themeplate-child', 'yes'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '48748', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:4:{s:39:\"svg-vector-icon-plugin/wp-svg-icons.php\";s:22:\"uninstall_wp_svg_icons\";s:27:\"wp-asset-clean-up/wpacu.php\";a:2:{i:0;s:8:\"Freemius\";i:1;s:22:\"_uninstall_plugin_hook\";}s:28:\"fast-velocity-minify/fvm.php\";s:29:\"fastvelocity_plugin_uninstall\";s:33:\"classic-editor/classic-editor.php\";a:2:{i:0;s:14:\"Classic_Editor\";i:1;s:9:\"uninstall\";}}', 'no'),
(82, 'timezone_string', 'Europe/Budapest', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '2', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '79', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'initial_db_version', '38590', 'yes'),
(92, 'wp_user_roles', 'a:8:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:64:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:20:\"wpseo_manage_options\";b:1;s:12:\"cfdb7_access\";b:1;s:10:\"loco_admin\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:35:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:13:\"wpseo_manager\";a:2:{s:4:\"name\";s:11:\"SEO Manager\";s:12:\"capabilities\";a:38:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;s:28:\"wpseo_edit_advanced_metadata\";b:1;s:20:\"wpseo_manage_options\";b:1;s:23:\"view_site_health_checks\";b:1;}}s:12:\"wpseo_editor\";a:2:{s:4:\"name\";s:10:\"SEO Editor\";s:12:\"capabilities\";a:36:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;s:28:\"wpseo_edit_advanced_metadata\";b:1;}}s:10:\"translator\";a:2:{s:4:\"name\";s:10:\"Translator\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:10:\"loco_admin\";b:1;}}}', 'yes'),
(93, 'fresh_site', '0', 'yes'),
(94, 'WPLANG', 'hu_HU', 'yes'),
(95, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(96, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'sidebars_widgets', 'a:6:{s:19:\"wp_inactive_widgets\";a:0:{}s:7:\"sidebar\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:6:\"footer\";a:0:{}s:8:\"footer-2\";a:0:{}s:8:\"footer-3\";a:0:{}s:13:\"array_version\";i:3;}', 'yes'),
(101, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'cron', 'a:10:{i:1600778405;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1600804614;a:1:{s:17:\"wmac_cachechecker\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1600806510;a:3:{s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1600848801;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1600849729;a:1:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1600851751;a:1:{s:19:\"wpseo-reindex-links\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1600853676;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1600854005;a:1:{s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1600950730;a:1:{s:30:\"wp_site_health_scheduled_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}s:7:\"version\";i:2;}', 'yes'),
(110, 'theme_mods_twentyseventeen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1509438587;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}', 'yes'),
(144, 'current_theme', 'ThemePlate Child', 'yes'),
(145, 'theme_mods_understrap-master', 'a:5:{i:0;b:0;s:28:\"understrap_posts_index_style\";s:7:\"default\";s:27:\"understrap_sidebar_position\";s:5:\"right\";s:25:\"understrap_container_type\";s:9:\"container\";s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1509438639;s:4:\"data\";a:6:{s:19:\"wp_inactive_widgets\";a:0:{}s:13:\"right-sidebar\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:12:\"left-sidebar\";a:0:{}s:4:\"hero\";a:0:{}s:10:\"statichero\";N;s:10:\"footerfull\";N;}}}', 'yes'),
(146, 'theme_switched', '', 'yes'),
(147, 'theme_mods_twentyfifteen', 'a:2:{i:0;b:0;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1509438657;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(148, 'theme_mods_understrap', 'a:5:{i:0;b:0;s:28:\"understrap_posts_index_style\";s:7:\"default\";s:27:\"understrap_sidebar_position\";s:5:\"right\";s:25:\"understrap_container_type\";s:9:\"container\";s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1509438669;s:4:\"data\";a:6:{s:19:\"wp_inactive_widgets\";a:0:{}s:13:\"right-sidebar\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:12:\"left-sidebar\";N;s:4:\"hero\";N;s:10:\"statichero\";N;s:10:\"footerfull\";N;}}}', 'yes'),
(149, 'theme_mods_understrap-child', 'a:7:{i:0;b:0;s:28:\"understrap_posts_index_style\";s:7:\"default\";s:27:\"understrap_sidebar_position\";s:5:\"right\";s:25:\"understrap_container_type\";s:9:\"container\";s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1548498991;s:4:\"data\";a:6:{s:19:\"wp_inactive_widgets\";a:0:{}s:13:\"right-sidebar\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:12:\"left-sidebar\";a:0:{}s:4:\"hero\";a:0:{}s:10:\"statichero\";a:0:{}s:10:\"footerfull\";a:0:{}}}s:18:\"nav_menu_locations\";a:0:{}}', 'yes'),
(181, 'recently_activated', 'a:1:{s:21:\"newsletter/plugin.php\";i:1599658709;}', 'yes'),
(186, 'wpseo', 'a:30:{s:8:\"tracking\";b:0;s:22:\"license_server_version\";s:1:\"2\";s:15:\"ms_defaults_set\";b:0;s:40:\"ignore_search_engines_discouraged_notice\";b:0;s:25:\"ignore_indexation_warning\";b:0;s:29:\"indexation_warning_hide_until\";b:0;s:18:\"indexation_started\";b:0;s:28:\"indexables_indexation_reason\";s:0:\"\";s:31:\"indexables_indexation_completed\";b:1;s:7:\"version\";s:4:\"14.9\";s:16:\"previous_version\";s:4:\"13.4\";s:20:\"disableadvanced_meta\";b:1;s:30:\"enable_headless_rest_endpoints\";b:1;s:17:\"ryte_indexability\";b:0;s:11:\"baiduverify\";s:0:\"\";s:12:\"googleverify\";s:0:\"\";s:8:\"msverify\";s:0:\"\";s:12:\"yandexverify\";s:0:\"\";s:9:\"site_type\";s:0:\"\";s:20:\"has_multiple_authors\";s:0:\"\";s:16:\"environment_type\";s:0:\"\";s:23:\"content_analysis_active\";b:1;s:23:\"keyword_analysis_active\";b:1;s:21:\"enable_admin_bar_menu\";b:1;s:26:\"enable_cornerstone_content\";b:1;s:18:\"enable_xml_sitemap\";b:1;s:24:\"enable_text_link_counter\";b:1;s:22:\"show_onboarding_notice\";b:1;s:18:\"first_activated_on\";i:1527930151;s:13:\"myyoast-oauth\";b:0;}', 'yes'),
(187, 'wpseo_titles', 'a:130:{s:17:\"forcerewritetitle\";b:0;s:9:\"separator\";s:7:\"sc-dash\";s:16:\"title-home-wpseo\";s:42:\"%%sitename%% %%page%% %%sep%% %%sitedesc%%\";s:18:\"title-author-wpseo\";s:41:\"%%name%%, Author at %%sitename%% %%page%%\";s:19:\"title-archive-wpseo\";s:38:\"%%date%% %%page%% %%sep%% %%sitename%%\";s:18:\"title-search-wpseo\";s:63:\"You searched for %%searchphrase%% %%page%% %%sep%% %%sitename%%\";s:15:\"title-404-wpseo\";s:35:\"Page not found %%sep%% %%sitename%%\";s:19:\"metadesc-home-wpseo\";s:0:\"\";s:21:\"metadesc-author-wpseo\";s:0:\"\";s:22:\"metadesc-archive-wpseo\";s:0:\"\";s:9:\"rssbefore\";s:0:\"\";s:8:\"rssafter\";s:53:\"The post %%POSTLINK%% appeared first on %%BLOGLINK%%.\";s:20:\"noindex-author-wpseo\";b:0;s:28:\"noindex-author-noposts-wpseo\";b:1;s:21:\"noindex-archive-wpseo\";b:1;s:14:\"disable-author\";b:0;s:12:\"disable-date\";b:0;s:19:\"disable-post_format\";b:0;s:18:\"disable-attachment\";b:1;s:23:\"is-media-purge-relevant\";b:0;s:20:\"breadcrumbs-404crumb\";s:25:\"Error 404: Page not found\";s:29:\"breadcrumbs-display-blog-page\";b:0;s:20:\"breadcrumbs-boldlast\";b:0;s:25:\"breadcrumbs-archiveprefix\";s:12:\"Archives for\";s:18:\"breadcrumbs-enable\";b:0;s:16:\"breadcrumbs-home\";s:4:\"Home\";s:18:\"breadcrumbs-prefix\";s:0:\"\";s:24:\"breadcrumbs-searchprefix\";s:16:\"You searched for\";s:15:\"breadcrumbs-sep\";s:2:\"»\";s:12:\"website_name\";s:0:\"\";s:11:\"person_name\";s:0:\"\";s:11:\"person_logo\";s:0:\"\";s:14:\"person_logo_id\";i:0;s:22:\"alternate_website_name\";s:0:\"\";s:12:\"company_logo\";s:0:\"\";s:15:\"company_logo_id\";i:0;s:12:\"company_name\";s:0:\"\";s:17:\"company_or_person\";s:7:\"company\";s:25:\"company_or_person_user_id\";b:0;s:17:\"stripcategorybase\";b:0;s:10:\"title-post\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-post\";s:0:\"\";s:12:\"noindex-post\";b:0;s:23:\"display-metabox-pt-post\";b:1;s:23:\"post_types-post-maintax\";i:0;s:21:\"schema-page-type-post\";s:7:\"WebPage\";s:24:\"schema-article-type-post\";s:7:\"Article\";s:10:\"title-page\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-page\";s:0:\"\";s:12:\"noindex-page\";b:0;s:23:\"display-metabox-pt-page\";b:1;s:23:\"post_types-page-maintax\";s:1:\"0\";s:21:\"schema-page-type-page\";s:7:\"WebPage\";s:24:\"schema-article-type-page\";s:4:\"None\";s:16:\"title-attachment\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:19:\"metadesc-attachment\";s:0:\"\";s:18:\"noindex-attachment\";b:0;s:29:\"display-metabox-pt-attachment\";b:1;s:29:\"post_types-attachment-maintax\";s:1:\"0\";s:27:\"schema-page-type-attachment\";s:7:\"WebPage\";s:30:\"schema-article-type-attachment\";s:4:\"None\";s:18:\"title-tax-category\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-category\";s:0:\"\";s:28:\"display-metabox-tax-category\";b:1;s:20:\"noindex-tax-category\";b:0;s:18:\"title-tax-post_tag\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-post_tag\";s:0:\"\";s:28:\"display-metabox-tax-post_tag\";b:1;s:20:\"noindex-tax-post_tag\";b:0;s:21:\"title-tax-post_format\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:24:\"metadesc-tax-post_format\";s:0:\"\";s:31:\"display-metabox-tax-post_format\";b:0;s:23:\"noindex-tax-post_format\";b:1;s:14:\"title-our_team\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:17:\"metadesc-our_team\";s:0:\"\";s:16:\"noindex-our_team\";b:0;s:27:\"display-metabox-pt-our_team\";b:1;s:27:\"post_types-our_team-maintax\";s:1:\"0\";s:25:\"schema-page-type-our_team\";s:7:\"WebPage\";s:28:\"schema-article-type-our_team\";s:4:\"None\";s:24:\"title-ptarchive-our_team\";s:55:\"%%pt_plural%% Archívált %%page%% %%sep%% %%sitename%%\";s:27:\"metadesc-ptarchive-our_team\";s:0:\"\";s:26:\"bctitle-ptarchive-our_team\";s:0:\"\";s:26:\"noindex-ptarchive-our_team\";b:0;s:17:\"title-testimonial\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:20:\"metadesc-testimonial\";s:0:\"\";s:19:\"noindex-testimonial\";b:0;s:30:\"display-metabox-pt-testimonial\";b:1;s:30:\"post_types-testimonial-maintax\";s:1:\"0\";s:28:\"schema-page-type-testimonial\";s:7:\"WebPage\";s:31:\"schema-article-type-testimonial\";s:4:\"None\";s:13:\"title-partner\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:16:\"metadesc-partner\";s:0:\"\";s:15:\"noindex-partner\";b:0;s:26:\"display-metabox-pt-partner\";b:1;s:26:\"post_types-partner-maintax\";s:1:\"0\";s:24:\"schema-page-type-partner\";s:7:\"WebPage\";s:27:\"schema-article-type-partner\";s:4:\"None\";s:18:\"title-publications\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-publications\";s:0:\"\";s:20:\"noindex-publications\";b:0;s:31:\"display-metabox-pt-publications\";b:1;s:31:\"post_types-publications-maintax\";i:0;s:29:\"schema-page-type-publications\";s:7:\"WebPage\";s:32:\"schema-article-type-publications\";s:4:\"None\";s:28:\"title-ptarchive-publications\";s:42:\"%%pt_plural%%%%page%% %%sep%% %%sitename%%\";s:31:\"metadesc-ptarchive-publications\";s:0:\"\";s:30:\"bctitle-ptarchive-publications\";s:0:\"\";s:30:\"noindex-ptarchive-publications\";b:0;s:18:\"title-int_contacts\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-int_contacts\";s:0:\"\";s:20:\"noindex-int_contacts\";b:0;s:31:\"display-metabox-pt-int_contacts\";b:1;s:31:\"post_types-int_contacts-maintax\";s:1:\"0\";s:29:\"schema-page-type-int_contacts\";s:7:\"WebPage\";s:32:\"schema-article-type-int_contacts\";s:4:\"None\";s:12:\"title-events\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:15:\"metadesc-events\";s:0:\"\";s:14:\"noindex-events\";b:0;s:25:\"display-metabox-pt-events\";b:1;s:25:\"post_types-events-maintax\";i:0;s:23:\"schema-page-type-events\";s:7:\"WebPage\";s:26:\"schema-article-type-events\";s:4:\"None\";s:22:\"title-ptarchive-events\";s:43:\"%%pt_plural%% %%page%% %%sep%% %%sitename%%\";s:25:\"metadesc-ptarchive-events\";s:0:\"\";s:24:\"bctitle-ptarchive-events\";s:0:\"\";s:24:\"noindex-ptarchive-events\";b:0;s:26:\"taxonomy-category-ptparent\";s:1:\"0\";s:26:\"taxonomy-post_tag-ptparent\";s:1:\"0\";s:29:\"taxonomy-post_format-ptparent\";s:1:\"0\";}', 'yes'),
(188, 'wpseo_social', 'a:19:{s:13:\"facebook_site\";s:0:\"\";s:13:\"instagram_url\";s:0:\"\";s:12:\"linkedin_url\";s:0:\"\";s:11:\"myspace_url\";s:0:\"\";s:16:\"og_default_image\";s:0:\"\";s:19:\"og_default_image_id\";s:0:\"\";s:18:\"og_frontpage_title\";s:0:\"\";s:17:\"og_frontpage_desc\";s:0:\"\";s:18:\"og_frontpage_image\";s:0:\"\";s:21:\"og_frontpage_image_id\";s:0:\"\";s:9:\"opengraph\";b:1;s:13:\"pinterest_url\";s:0:\"\";s:15:\"pinterestverify\";s:0:\"\";s:7:\"twitter\";b:1;s:12:\"twitter_site\";s:0:\"\";s:17:\"twitter_card_type\";s:19:\"summary_large_image\";s:11:\"youtube_url\";s:0:\"\";s:13:\"wikipedia_url\";s:0:\"\";s:10:\"fbadminapp\";s:0:\"\";}', 'yes'),
(189, 'wpseo_flush_rewrite', '1', 'yes'),
(198, 'wdev-frash', 'a:3:{s:7:\"plugins\";a:1:{s:23:\"wp-smushit/wp-smush.php\";i:1527930198;}s:5:\"queue\";a:2:{s:32:\"7de3619981caadc55f30a002bfb299f6\";a:4:{s:6:\"plugin\";s:23:\"wp-smushit/wp-smush.php\";s:4:\"type\";s:5:\"email\";s:7:\"show_at\";i:1527930198;s:6:\"sticky\";b:1;}s:32:\"fc50097023d0d34c5a66f6cddcf77694\";a:3:{s:6:\"plugin\";s:23:\"wp-smushit/wp-smush.php\";s:4:\"type\";s:4:\"rate\";s:7:\"show_at\";i:1600780423;}}s:4:\"done\";a:0:{}}', 'no'),
(199, 'wp-smush-install-type', 'existing', 'no'),
(200, 'wp-smush-version', '3.7.0', 'no'),
(201, 'wp-smush-skip-redirect', '1', 'no'),
(203, 'smush_global_stats', 'a:9:{s:11:\"size_before\";i:0;s:10:\"size_after\";i:0;s:7:\"percent\";d:0;s:5:\"human\";s:5:\"0,0 B\";s:5:\"bytes\";i:0;s:12:\"total_images\";i:0;s:12:\"resize_count\";i:0;s:14:\"resize_savings\";i:0;s:18:\"conversion_savings\";i:0;}', 'no'),
(208, 'dir_smush_stats', 'a:2:{s:9:\"dir_smush\";a:2:{s:5:\"total\";s:1:\"0\";s:9:\"optimised\";i:0;}s:14:\"combined_stats\";a:0:{}}', 'no'),
(209, 'skip-smush-setup', '1', 'no'),
(212, 'wpmdb_settings', 'a:12:{s:3:\"key\";s:40:\"YKzrgMpkTQ45FVfxnL2vd5Qdz6m1N59NeWokLPTA\";s:10:\"allow_pull\";b:0;s:10:\"allow_push\";b:0;s:8:\"profiles\";a:0:{}s:7:\"licence\";s:0:\"\";s:10:\"verify_ssl\";b:0;s:17:\"whitelist_plugins\";a:0:{}s:11:\"max_request\";i:1048576;s:22:\"delay_between_requests\";i:0;s:18:\"prog_tables_hidden\";b:1;s:21:\"pause_before_finalize\";b:0;s:14:\"allow_tracking\";N;}', 'no'),
(213, 'wpmdb_schema_version', '2', 'no'),
(222, 'a8c_developer', 'a:1:{s:12:\"project_type\";s:11:\"wporg-theme\";}', 'yes'),
(257, 'wpcf7', 'a:2:{s:7:\"version\";s:5:\"5.2.2\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";i:1527930759;s:7:\"version\";s:5:\"5.0.2\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes'),
(263, 'cptui_new_install', 'false', 'yes'),
(282, 'sm_options', 'a:52:{s:18:\"sm_b_prio_provider\";s:41:\"GoogleSitemapGeneratorPrioByCountProvider\";s:9:\"sm_b_ping\";b:1;s:10:\"sm_b_stats\";b:0;s:12:\"sm_b_pingmsn\";b:1;s:12:\"sm_b_autozip\";b:1;s:11:\"sm_b_memory\";s:0:\"\";s:9:\"sm_b_time\";i:-1;s:18:\"sm_b_style_default\";b:1;s:10:\"sm_b_style\";s:0:\"\";s:12:\"sm_b_baseurl\";s:0:\"\";s:11:\"sm_b_robots\";b:1;s:9:\"sm_b_html\";b:1;s:12:\"sm_b_exclude\";a:0:{}s:17:\"sm_b_exclude_cats\";a:0:{}s:10:\"sm_in_home\";b:1;s:11:\"sm_in_posts\";b:1;s:15:\"sm_in_posts_sub\";b:0;s:11:\"sm_in_pages\";b:1;s:10:\"sm_in_cats\";b:0;s:10:\"sm_in_arch\";b:0;s:10:\"sm_in_auth\";b:0;s:10:\"sm_in_tags\";b:0;s:9:\"sm_in_tax\";a:0:{}s:17:\"sm_in_customtypes\";a:0:{}s:13:\"sm_in_lastmod\";b:1;s:10:\"sm_cf_home\";s:5:\"daily\";s:11:\"sm_cf_posts\";s:7:\"monthly\";s:11:\"sm_cf_pages\";s:6:\"weekly\";s:10:\"sm_cf_cats\";s:6:\"weekly\";s:10:\"sm_cf_auth\";s:6:\"weekly\";s:15:\"sm_cf_arch_curr\";s:5:\"daily\";s:14:\"sm_cf_arch_old\";s:6:\"yearly\";s:10:\"sm_cf_tags\";s:6:\"weekly\";s:10:\"sm_pr_home\";d:1;s:11:\"sm_pr_posts\";d:0.6;s:15:\"sm_pr_posts_min\";d:0.2;s:11:\"sm_pr_pages\";d:0.6;s:10:\"sm_pr_cats\";d:0.3;s:10:\"sm_pr_arch\";d:0.3;s:10:\"sm_pr_auth\";d:0.3;s:10:\"sm_pr_tags\";d:0.3;s:12:\"sm_i_donated\";b:0;s:17:\"sm_i_hide_donated\";b:0;s:17:\"sm_i_install_date\";i:1527932310;s:16:\"sm_i_hide_survey\";b:0;s:14:\"sm_i_hide_note\";b:0;s:15:\"sm_i_hide_works\";b:0;s:16:\"sm_i_hide_donors\";b:0;s:9:\"sm_i_hash\";s:20:\"9da2b23434bcc2725652\";s:13:\"sm_i_lastping\";i:1548579132;s:16:\"sm_i_supportfeed\";b:1;s:22:\"sm_i_supportfeed_cache\";i:1583827338;}', 'yes'),
(286, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(290, 'disable_comments_options', 'a:5:{s:19:\"disabled_post_types\";a:0:{}s:17:\"remove_everywhere\";b:0;s:9:\"permanent\";b:0;s:16:\"extra_post_types\";b:0;s:10:\"db_version\";i:6;}', 'yes'),
(291, 'duplicate_page_options', 'a:3:{s:21:\"duplicate_post_status\";s:5:\"draft\";s:23:\"duplicate_post_redirect\";s:7:\"to_list\";s:21:\"duplicate_post_suffix\";s:0:\"\";}', 'yes'),
(295, '_transient_understrap_categories', '1', 'yes'),
(325, 'theme_mods_themeplate', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:7:\"primary\";i:2;}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1548620296;s:4:\"data\";a:5:{s:19:\"wp_inactive_widgets\";a:0:{}s:7:\"sidebar\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:6:\"footer\";a:0:{}s:8:\"footer-2\";a:0:{}s:8:\"footer-3\";a:0:{}}}}', 'yes'),
(326, 'widget_themeplate_about', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(327, 'widget_themeplate_contact', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(403, 'wp-smush-settings', 'a:15:{s:11:\"networkwide\";b:0;s:4:\"auto\";b:1;s:5:\"lossy\";b:0;s:10:\"strip_exif\";b:1;s:6:\"resize\";b:0;s:9:\"detection\";b:0;s:8:\"original\";b:0;s:6:\"backup\";b:0;s:10:\"png_to_jpg\";b:0;s:7:\"nextgen\";b:0;s:2:\"s3\";b:0;s:9:\"gutenberg\";b:0;s:3:\"cdn\";b:0;s:11:\"auto_resize\";b:0;s:4:\"webp\";b:1;}', 'yes'),
(427, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(429, 'sm_status', 'O:28:\"GoogleSitemapGeneratorStatus\":4:{s:39:\"\0GoogleSitemapGeneratorStatus\0startTime\";d:1548579130.359318;s:37:\"\0GoogleSitemapGeneratorStatus\0endTime\";d:1548579131.346869;s:41:\"\0GoogleSitemapGeneratorStatus\0pingResults\";a:2:{s:6:\"google\";a:5:{s:9:\"startTime\";d:1548579130.368825;s:7:\"endTime\";d:1548579130.7352221;s:7:\"success\";b:0;s:3:\"url\";s:128:\"http://www.google.com/webmasters/sitemaps/ping?sitemap=https%3A%2F%2Flocalhost%2Fwordpress-bootstrap-4-boilerplate%2Fsitemap.xml\";s:4:\"name\";s:6:\"Google\";}s:4:\"bing\";a:5:{s:9:\"startTime\";d:1548579130.7504101;s:7:\"endTime\";d:1548579131.3391559;s:7:\"success\";b:1;s:3:\"url\";s:121:\"http://www.bing.com/webmaster/ping.aspx?siteMap=https%3A%2F%2Flocalhost%2Fwordpress-bootstrap-4-boilerplate%2Fsitemap.xml\";s:4:\"name\";s:4:\"Bing\";}}s:38:\"\0GoogleSitemapGeneratorStatus\0autoSave\";b:1;}', 'no'),
(460, 'theme_mods_themeplate-child', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:7:\"primary\";i:2;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(787, 'factory_plugin_versions', 'a:2:{s:13:\"wbcr_gonzales\";s:10:\"free-1.0.7\";s:12:\"wbcr_clearfy\";s:10:\"free-1.5.0\";}', 'yes'),
(798, 'wbcr_clearfy_deactive_preinstall_components', 'a:1:{i:0;s:9:\"cyrlitera\";}', 'yes'),
(799, 'wbcr_clearfy_plugin_activated', '1554148614', 'yes'),
(837, 'fs_active_plugins', 'O:8:\"stdClass\":0:{}', 'yes'),
(838, 'fs_debug_mode', '', 'yes'),
(839, 'fs_accounts', 'a:6:{s:21:\"id_slug_type_path_map\";a:1:{i:2951;a:2:{s:4:\"slug\";s:17:\"wp-asset-clean-up\";s:4:\"type\";s:6:\"plugin\";}}s:11:\"plugin_data\";a:1:{s:17:\"wp-asset-clean-up\";a:15:{s:16:\"plugin_main_file\";O:8:\"stdClass\":1:{s:9:\"prev_path\";s:27:\"wp-asset-clean-up/wpacu.php\";}s:17:\"install_timestamp\";i:1554149200;s:16:\"sdk_last_version\";N;s:11:\"sdk_version\";s:5:\"2.2.4\";s:16:\"sdk_upgrade_mode\";b:1;s:18:\"sdk_downgrade_mode\";b:0;s:19:\"plugin_last_version\";N;s:14:\"plugin_version\";s:7:\"1.3.2.5\";s:19:\"plugin_upgrade_mode\";b:1;s:21:\"plugin_downgrade_mode\";b:0;s:21:\"is_plugin_new_install\";b:1;s:17:\"was_plugin_loaded\";b:1;s:17:\"connectivity_test\";a:6:{s:12:\"is_connected\";b:1;s:4:\"host\";s:9:\"localhost\";s:9:\"server_ip\";s:3:\"::1\";s:9:\"is_active\";b:1;s:9:\"timestamp\";i:1554149203;s:7:\"version\";s:7:\"1.3.2.5\";}s:15:\"prev_is_premium\";b:0;s:16:\"uninstall_reason\";O:8:\"stdClass\":3:{s:2:\"id\";s:2:\"15\";s:4:\"info\";s:0:\"\";s:12:\"is_anonymous\";b:0;}}}s:13:\"file_slug_map\";a:1:{s:27:\"wp-asset-clean-up/wpacu.php\";s:17:\"wp-asset-clean-up\";}s:7:\"plugins\";a:1:{s:17:\"wp-asset-clean-up\";O:9:\"FS_Plugin\":20:{s:16:\"parent_plugin_id\";N;s:5:\"title\";s:33:\"Asset CleanUp: Page Speed Booster\";s:4:\"slug\";s:17:\"wp-asset-clean-up\";s:12:\"premium_slug\";s:25:\"wp-asset-clean-up-premium\";s:4:\"type\";s:6:\"plugin\";s:20:\"affiliate_moderation\";b:0;s:19:\"is_wp_org_compliant\";b:1;s:4:\"file\";s:27:\"wp-asset-clean-up/wpacu.php\";s:7:\"version\";s:7:\"1.3.2.5\";s:11:\"auto_update\";N;s:4:\"info\";N;s:10:\"is_premium\";b:0;s:14:\"premium_suffix\";s:9:\"(Premium)\";s:7:\"is_live\";b:1;s:10:\"public_key\";s:32:\"pk_70ecc6600cb03b5168150b4c99257\";s:10:\"secret_key\";N;s:2:\"id\";s:4:\"2951\";s:7:\"updated\";N;s:7:\"created\";N;s:22:\"\0FS_Entity\0_is_updated\";b:0;}}s:9:\"unique_id\";s:32:\"51fa79bb6ef83171be728be84b413a81\";s:13:\"admin_notices\";a:1:{s:17:\"wp-asset-clean-up\";a:0:{}}}', 'yes'),
(840, 'fs_api_cache', 'a:0:{}', 'yes'),
(841, 'wpassetcleanup_do_activation_redirect_first_time', '1', 'yes'),
(844, 'fs_gdpr', 'a:1:{s:2:\"u1\";a:1:{s:8:\"required\";b:0;}}', 'yes'),
(894, 'aqm-dequeued', 'a:2:{s:6:\"styles\";a:2:{s:22:\"current-template-style\";a:4:{s:6:\"handle\";s:22:\"current-template-style\";s:3:\"src\";s:106:\"https://localhost/wordpress-bootstrap-4-boilerplate/wp-content/plugins/show-current-template/css/style.css\";s:3:\"ver\";s:5:\"false\";s:4:\"args\";s:3:\"all\";}s:14:\"contact-form-7\";a:4:{s:6:\"handle\";s:14:\"contact-form-7\";s:3:\"src\";s:109:\"https://localhost/wordpress-bootstrap-4-boilerplate/wp-content/plugins/contact-form-7/includes/css/styles.css\";s:3:\"ver\";s:5:\"5.1.1\";s:4:\"args\";s:3:\"all\";}}s:7:\"scripts\";a:1:{s:14:\"contact-form-7\";a:6:{s:6:\"handle\";s:14:\"contact-form-7\";s:3:\"src\";s:108:\"https://localhost/wordpress-bootstrap-4-boilerplate/wp-content/plugins/contact-form-7/includes/js/scripts.js\";s:4:\"deps\";a:1:{i:0;s:6:\"jquery\";}s:3:\"ver\";s:5:\"5.1.1\";s:4:\"args\";s:0:\"\";s:5:\"extra\";a:2:{s:5:\"group\";s:1:\"1\";s:4:\"data\";s:173:\"var wpcf7 = {\\\"apiSettings\\\":{\\\"root\\\":\\\"https:\\\\/\\\\/localhost\\\\/wordpress-bootstrap-4-boilerplate\\\\/wp-json\\\\/contact-form-7\\\\/v1\\\",\\\"namespace\\\":\\\"contact-form-7\\\\/v1\\\"}};\";}}}}', 'yes'),
(919, '_transient_timeout_wpseo_link_table_inaccessible', '1615363338', 'no'),
(920, '_transient_wpseo_link_table_inaccessible', '0', 'no'),
(921, '_transient_timeout_wpseo_meta_table_inaccessible', '1615363338', 'no'),
(922, '_transient_wpseo_meta_table_inaccessible', '0', 'no'),
(1007, 'cfdb7_view_install_date', '2020-03-10 8:10:09', 'yes'),
(1020, 'fvm-last-cache-update', '1583836194', 'yes'),
(1021, 'fastvelocity_min_fvm_fix_editor', '1', 'yes'),
(1022, 'fastvelocity_min_remove_print_mediatypes', '1', 'yes'),
(1023, 'fastvelocity_fvm_clean_header_one', '1', 'yes'),
(1024, 'fastvelocity_min_skip_google_fonts', '1', 'yes'),
(1025, 'fastvelocity_min_force_inline_css_footer', '1', 'yes'),
(1026, 'fastvelocity_min_skip_cssorder', '1', 'yes'),
(1027, 'fastvelocity_gfonts_method', '1', 'yes'),
(1028, 'fastvelocity_fontawesome_method', '1', 'yes'),
(1029, 'fastvelocity_min_disable_css_inline_merge', '1', 'yes'),
(1030, 'fastvelocity_min_blacklist', '/html5shiv.js\r\n/html5shiv-printshiv.min.js\r\n/excanvas.js\r\n/avada-ie9.js\r\n/respond.js\r\n/respond.min.js\r\n/selectivizr.js\r\n/Avada/assets/css/ie.css\r\n/html5.js\r\n/IE9.js\r\n/fusion-ie9.js\r\n/vc_lte_ie9.min.css\r\n/old-ie.css\r\n/ie.css\r\n/vc-ie8.min.css\r\n/mailchimp-for-wp/assets/js/third-party/placeholders.min.js\r\n/assets/js/plugins/wp-enqueue/min/webfontloader.js\r\n/a.optnmstr.com/app/js/api.min.js\r\n/pixelyoursite/js/public.js\r\n/assets/js/wcdrip-drip.js', 'yes'),
(1031, 'fastvelocity_min_ignorelist', '/Avada/assets/js/main.min.js\r\n/woocommerce-product-search/js/product-search.js\r\n/includes/builder/scripts/frontend-builder-scripts.js\r\n/assets/js/jquery.themepunch.tools.min.js\r\n/js/TweenMax.min.js\r\n/jupiter/assets/js/min/full-scripts\r\n/wp-content/themes/Divi/core/admin/js/react-dom.production.min.js\r\n/LayerSlider/static/layerslider/js/greensock.js\r\n/themes/kalium/assets/js/main.min.js\r\n/elementor/assets/js/common.min.js\r\n/elementor/assets/js/frontend.min.js\r\n/elementor-pro/assets/js/frontend.min.js\r\n/Divi/core/admin/js/react-dom.production.min.js\r\n/kalium/assets/js/main.min.js', 'yes'),
(1032, 'fastvelocity_plugin_version', '2.7.9', 'yes'),
(1033, 'fastvelocity_preserve_settings_on_uninstall', '1', 'yes'),
(1038, 'recovery_keys', 'a:0:{}', 'yes'),
(1039, 'wp_page_for_privacy_policy', '0', 'yes'),
(1040, 'show_comments_cookies_opt_in', '1', 'yes'),
(1041, 'admin_email_lifespan', '1615627903', 'yes'),
(1042, 'db_upgraded', '', 'yes'),
(1074, 'newsletter_logger_secret', '290f647a', 'yes'),
(1076, 'newsletter_main_first_install_time', '1583834072', 'no'),
(1077, 'newsletter_main', 'a:31:{s:11:\"return_path\";s:0:\"\";s:8:\"reply_to\";s:0:\"\";s:12:\"sender_email\";s:20:\"newsletter@localhost\";s:11:\"sender_name\";s:14:\"WP Bootstrap 4\";s:6:\"editor\";i:0;s:13:\"scheduler_max\";i:100;s:9:\"phpmailer\";i:0;s:5:\"debug\";i:0;s:5:\"track\";i:1;s:3:\"css\";s:0:\"\";s:12:\"css_disabled\";i:0;s:2:\"ip\";s:0:\"\";s:4:\"page\";i:9;s:19:\"disable_cron_notice\";i:0;s:13:\"do_shortcodes\";i:0;s:11:\"header_logo\";s:0:\"\";s:12:\"header_title\";s:14:\"WP Bootstrap 4\";s:10:\"header_sub\";s:34:\"... egy újabb WordPress honlap...\";s:12:\"footer_title\";s:0:\"\";s:14:\"footer_contact\";s:0:\"\";s:12:\"footer_legal\";s:0:\"\";s:12:\"facebook_url\";s:0:\"\";s:11:\"twitter_url\";s:0:\"\";s:13:\"instagram_url\";s:0:\"\";s:14:\"googleplus_url\";s:0:\"\";s:13:\"pinterest_url\";s:0:\"\";s:12:\"linkedin_url\";s:0:\"\";s:10:\"tumblr_url\";s:0:\"\";s:11:\"youtube_url\";s:0:\"\";s:9:\"vimeo_url\";s:0:\"\";s:14:\"soundcloud_url\";s:0:\"\";}', 'yes'),
(1078, 'newsletter_main_info', 'a:16:{s:11:\"header_logo\";a:1:{s:2:\"id\";i:0;}s:12:\"header_title\";s:14:\"WP Bootstrap 4\";s:10:\"header_sub\";s:34:\"... egy újabb WordPress honlap...\";s:12:\"footer_title\";s:0:\"\";s:14:\"footer_contact\";s:0:\"\";s:12:\"footer_legal\";s:0:\"\";s:12:\"facebook_url\";s:0:\"\";s:11:\"twitter_url\";s:0:\"\";s:13:\"instagram_url\";s:0:\"\";s:14:\"googleplus_url\";s:0:\"\";s:13:\"pinterest_url\";s:0:\"\";s:12:\"linkedin_url\";s:0:\"\";s:10:\"tumblr_url\";s:0:\"\";s:11:\"youtube_url\";s:0:\"\";s:9:\"vimeo_url\";s:0:\"\";s:14:\"soundcloud_url\";s:0:\"\";}', 'yes'),
(1079, 'newsletter_main_smtp', 'a:7:{s:7:\"enabled\";i:0;s:4:\"host\";s:0:\"\";s:4:\"user\";s:0:\"\";s:4:\"pass\";s:0:\"\";s:4:\"port\";i:25;s:6:\"secure\";s:0:\"\";s:12:\"ssl_insecure\";i:0;}', 'yes'),
(1080, 'newsletter_main_version', '1.6.3', 'yes'),
(1081, 'newsletter_subscription_first_install_time', '1583834072', 'no'),
(1082, 'newsletter_subscription_antibot', 'a:6:{s:12:\"ip_blacklist\";a:0:{}s:17:\"address_blacklist\";a:0:{}s:9:\"antiflood\";i:60;s:7:\"akismet\";i:0;s:7:\"captcha\";i:0;s:8:\"disabled\";i:0;}', 'yes'),
(1083, 'newsletter', 'a:14:{s:14:\"noconfirmation\";i:1;s:12:\"notify_email\";s:18:\"doxa84@hotmail.com\";s:8:\"multiple\";i:1;s:6:\"notify\";i:0;s:10:\"error_text\";s:102:\"<p>You cannot subscribe with the email address you entered, please contact the site administrator.</p>\";s:17:\"subscription_text\";s:19:\"{subscription_form}\";s:17:\"confirmation_text\";s:104:\"<p>A confirmation email is on the way. Follow the instructions and check the spam folder. Thank you.</p>\";s:20:\"confirmation_subject\";s:32:\"Please confirm your subscription\";s:21:\"confirmation_tracking\";s:0:\"\";s:20:\"confirmation_message\";s:94:\"<p>Please confirm your subscription <a href=\"{subscription_confirm_url}\">clicking here</a></p>\";s:14:\"confirmed_text\";s:43:\"<p>Your subscription has been confirmed</p>\";s:17:\"confirmed_subject\";s:7:\"Welcome\";s:17:\"confirmed_message\";s:130:\"<p>This message confirms your subscription to our newsletter. Thank you!</p><hr><p><a href=\"{profile_url}\">Change your profile</p>\";s:18:\"confirmed_tracking\";s:0:\"\";}', 'yes'),
(1084, 'newsletter_subscription_lists', 'a:160:{s:6:\"list_1\";s:0:\"\";s:13:\"list_1_status\";i:0;s:14:\"list_1_checked\";i:0;s:13:\"list_1_forced\";i:0;s:6:\"list_2\";s:0:\"\";s:13:\"list_2_status\";i:0;s:14:\"list_2_checked\";i:0;s:13:\"list_2_forced\";i:0;s:6:\"list_3\";s:0:\"\";s:13:\"list_3_status\";i:0;s:14:\"list_3_checked\";i:0;s:13:\"list_3_forced\";i:0;s:6:\"list_4\";s:0:\"\";s:13:\"list_4_status\";i:0;s:14:\"list_4_checked\";i:0;s:13:\"list_4_forced\";i:0;s:6:\"list_5\";s:0:\"\";s:13:\"list_5_status\";i:0;s:14:\"list_5_checked\";i:0;s:13:\"list_5_forced\";i:0;s:6:\"list_6\";s:0:\"\";s:13:\"list_6_status\";i:0;s:14:\"list_6_checked\";i:0;s:13:\"list_6_forced\";i:0;s:6:\"list_7\";s:0:\"\";s:13:\"list_7_status\";i:0;s:14:\"list_7_checked\";i:0;s:13:\"list_7_forced\";i:0;s:6:\"list_8\";s:0:\"\";s:13:\"list_8_status\";i:0;s:14:\"list_8_checked\";i:0;s:13:\"list_8_forced\";i:0;s:6:\"list_9\";s:0:\"\";s:13:\"list_9_status\";i:0;s:14:\"list_9_checked\";i:0;s:13:\"list_9_forced\";i:0;s:7:\"list_10\";s:0:\"\";s:14:\"list_10_status\";i:0;s:15:\"list_10_checked\";i:0;s:14:\"list_10_forced\";i:0;s:7:\"list_11\";s:0:\"\";s:14:\"list_11_status\";i:0;s:15:\"list_11_checked\";i:0;s:14:\"list_11_forced\";i:0;s:7:\"list_12\";s:0:\"\";s:14:\"list_12_status\";i:0;s:15:\"list_12_checked\";i:0;s:14:\"list_12_forced\";i:0;s:7:\"list_13\";s:0:\"\";s:14:\"list_13_status\";i:0;s:15:\"list_13_checked\";i:0;s:14:\"list_13_forced\";i:0;s:7:\"list_14\";s:0:\"\";s:14:\"list_14_status\";i:0;s:15:\"list_14_checked\";i:0;s:14:\"list_14_forced\";i:0;s:7:\"list_15\";s:0:\"\";s:14:\"list_15_status\";i:0;s:15:\"list_15_checked\";i:0;s:14:\"list_15_forced\";i:0;s:7:\"list_16\";s:0:\"\";s:14:\"list_16_status\";i:0;s:15:\"list_16_checked\";i:0;s:14:\"list_16_forced\";i:0;s:7:\"list_17\";s:0:\"\";s:14:\"list_17_status\";i:0;s:15:\"list_17_checked\";i:0;s:14:\"list_17_forced\";i:0;s:7:\"list_18\";s:0:\"\";s:14:\"list_18_status\";i:0;s:15:\"list_18_checked\";i:0;s:14:\"list_18_forced\";i:0;s:7:\"list_19\";s:0:\"\";s:14:\"list_19_status\";i:0;s:15:\"list_19_checked\";i:0;s:14:\"list_19_forced\";i:0;s:7:\"list_20\";s:0:\"\";s:14:\"list_20_status\";i:0;s:15:\"list_20_checked\";i:0;s:14:\"list_20_forced\";i:0;s:7:\"list_21\";s:0:\"\";s:14:\"list_21_status\";i:0;s:15:\"list_21_checked\";i:0;s:14:\"list_21_forced\";i:0;s:7:\"list_22\";s:0:\"\";s:14:\"list_22_status\";i:0;s:15:\"list_22_checked\";i:0;s:14:\"list_22_forced\";i:0;s:7:\"list_23\";s:0:\"\";s:14:\"list_23_status\";i:0;s:15:\"list_23_checked\";i:0;s:14:\"list_23_forced\";i:0;s:7:\"list_24\";s:0:\"\";s:14:\"list_24_status\";i:0;s:15:\"list_24_checked\";i:0;s:14:\"list_24_forced\";i:0;s:7:\"list_25\";s:0:\"\";s:14:\"list_25_status\";i:0;s:15:\"list_25_checked\";i:0;s:14:\"list_25_forced\";i:0;s:7:\"list_26\";s:0:\"\";s:14:\"list_26_status\";i:0;s:15:\"list_26_checked\";i:0;s:14:\"list_26_forced\";i:0;s:7:\"list_27\";s:0:\"\";s:14:\"list_27_status\";i:0;s:15:\"list_27_checked\";i:0;s:14:\"list_27_forced\";i:0;s:7:\"list_28\";s:0:\"\";s:14:\"list_28_status\";i:0;s:15:\"list_28_checked\";i:0;s:14:\"list_28_forced\";i:0;s:7:\"list_29\";s:0:\"\";s:14:\"list_29_status\";i:0;s:15:\"list_29_checked\";i:0;s:14:\"list_29_forced\";i:0;s:7:\"list_30\";s:0:\"\";s:14:\"list_30_status\";i:0;s:15:\"list_30_checked\";i:0;s:14:\"list_30_forced\";i:0;s:7:\"list_31\";s:0:\"\";s:14:\"list_31_status\";i:0;s:15:\"list_31_checked\";i:0;s:14:\"list_31_forced\";i:0;s:7:\"list_32\";s:0:\"\";s:14:\"list_32_status\";i:0;s:15:\"list_32_checked\";i:0;s:14:\"list_32_forced\";i:0;s:7:\"list_33\";s:0:\"\";s:14:\"list_33_status\";i:0;s:15:\"list_33_checked\";i:0;s:14:\"list_33_forced\";i:0;s:7:\"list_34\";s:0:\"\";s:14:\"list_34_status\";i:0;s:15:\"list_34_checked\";i:0;s:14:\"list_34_forced\";i:0;s:7:\"list_35\";s:0:\"\";s:14:\"list_35_status\";i:0;s:15:\"list_35_checked\";i:0;s:14:\"list_35_forced\";i:0;s:7:\"list_36\";s:0:\"\";s:14:\"list_36_status\";i:0;s:15:\"list_36_checked\";i:0;s:14:\"list_36_forced\";i:0;s:7:\"list_37\";s:0:\"\";s:14:\"list_37_status\";i:0;s:15:\"list_37_checked\";i:0;s:14:\"list_37_forced\";i:0;s:7:\"list_38\";s:0:\"\";s:14:\"list_38_status\";i:0;s:15:\"list_38_checked\";i:0;s:14:\"list_38_forced\";i:0;s:7:\"list_39\";s:0:\"\";s:14:\"list_39_status\";i:0;s:15:\"list_39_checked\";i:0;s:14:\"list_39_forced\";i:0;s:7:\"list_40\";s:0:\"\";s:14:\"list_40_status\";i:0;s:15:\"list_40_checked\";i:0;s:14:\"list_40_forced\";i:0;}', 'yes'),
(1085, 'newsletter_subscription_template', 'a:1:{s:8:\"template\";s:2365:\"<!DOCTYPE html>\n<html>\n    <head>\n        <!-- General styles, not used by all email clients -->\n        <style type=\"text/css\" media=\"all\">\n            a {\n                text-decoration: none;\n                color: #0088cc;\n            }\n            hr {\n                border-top: 1px solid #999;\n            }\n        </style>\n    </head>\n\n    <!-- KEEP THE LAYOUT SIMPLE: THOSE ARE SERVICE MESSAGES. -->\n    <body style=\"margin: 0; padding: 0;\">\n\n        <!-- Top title with dark background -->\n        <table style=\"background-color: #444; width: 100%;\" cellspacing=\"0\" cellpadding=\"0\">\n            <tr>\n                <td style=\"padding: 20px; text-align: center; font-family: sans-serif; color: #fff; font-size: 28px\">\n                    {blog_title}\n                </td>\n            </tr>\n        </table>\n\n        <!-- Main table 100% wide with background color #eee -->    \n        <table style=\"background-color: #eee; width: 100%;\">\n            <tr>\n                <td align=\"center\" style=\"padding: 15px;\">\n\n                    <!-- Content table with backgdound color #fff, width 500px -->\n                    <table style=\"background-color: #fff; max-width: 600px; width: 100%; border: 1px solid #ddd;\">\n                        <tr>\n                            <td style=\"padding: 15px; color: #333; font-size: 16px; font-family: sans-serif\">\n                                <!-- The \"message\" tag below is replaced with one of confirmation, welcome or goodbye messages -->\n                                <!-- Messages content can be configured on Newsletter List Building panels --> \n\n                                {message}\n\n                                <hr>\n                                <!-- Signature if not already added to single messages (surround with <p>) -->\n                                <p>\n                                    <small>\n                                        <a href=\"{blog_url}\">{blog_url}</a><br>\n                                        {company_name}<br>\n                                        {company_address}\n                                    </small>\n                                </p>\n                                \n\n                            </td>\n                        </tr>\n                    </table>\n\n                </td>\n            </tr>\n        </table>\n\n    </body>\n</html>\";}', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1086, 'newsletter_profile', 'a:184:{s:5:\"email\";s:5:\"Email\";s:11:\"email_error\";s:28:\"Email address is not correct\";s:4:\"name\";s:23:\"First name or full name\";s:10:\"name_error\";s:16:\"Name is required\";s:11:\"name_status\";i:0;s:10:\"name_rules\";i:0;s:7:\"surname\";s:9:\"Last name\";s:13:\"surname_error\";s:21:\"Last name is required\";s:14:\"surname_status\";i:0;s:10:\"sex_status\";i:0;s:3:\"sex\";s:3:\"I\'m\";s:7:\"privacy\";s:44:\"By continuing, you accept the privacy policy\";s:13:\"privacy_error\";s:34:\"You must accept the privacy policy\";s:14:\"privacy_status\";i:0;s:11:\"privacy_url\";s:0:\"\";s:18:\"privacy_use_wp_url\";i:0;s:9:\"subscribe\";s:9:\"Subscribe\";s:12:\"title_female\";s:3:\"Ms.\";s:10:\"title_male\";s:3:\"Mr.\";s:10:\"title_none\";s:4:\"Dear\";s:8:\"sex_male\";s:3:\"Man\";s:10:\"sex_female\";s:5:\"Woman\";s:8:\"sex_none\";s:13:\"Not specified\";s:13:\"profile_error\";s:34:\"A mandatory field is not filled in\";s:16:\"profile_1_status\";i:0;s:9:\"profile_1\";s:0:\"\";s:14:\"profile_1_type\";s:4:\"text\";s:21:\"profile_1_placeholder\";s:0:\"\";s:15:\"profile_1_rules\";i:0;s:17:\"profile_1_options\";s:0:\"\";s:16:\"profile_2_status\";i:0;s:9:\"profile_2\";s:0:\"\";s:14:\"profile_2_type\";s:4:\"text\";s:21:\"profile_2_placeholder\";s:0:\"\";s:15:\"profile_2_rules\";i:0;s:17:\"profile_2_options\";s:0:\"\";s:16:\"profile_3_status\";i:0;s:9:\"profile_3\";s:0:\"\";s:14:\"profile_3_type\";s:4:\"text\";s:21:\"profile_3_placeholder\";s:0:\"\";s:15:\"profile_3_rules\";i:0;s:17:\"profile_3_options\";s:0:\"\";s:16:\"profile_4_status\";i:0;s:9:\"profile_4\";s:0:\"\";s:14:\"profile_4_type\";s:4:\"text\";s:21:\"profile_4_placeholder\";s:0:\"\";s:15:\"profile_4_rules\";i:0;s:17:\"profile_4_options\";s:0:\"\";s:16:\"profile_5_status\";i:0;s:9:\"profile_5\";s:0:\"\";s:14:\"profile_5_type\";s:4:\"text\";s:21:\"profile_5_placeholder\";s:0:\"\";s:15:\"profile_5_rules\";i:0;s:17:\"profile_5_options\";s:0:\"\";s:16:\"profile_6_status\";i:0;s:9:\"profile_6\";s:0:\"\";s:14:\"profile_6_type\";s:4:\"text\";s:21:\"profile_6_placeholder\";s:0:\"\";s:15:\"profile_6_rules\";i:0;s:17:\"profile_6_options\";s:0:\"\";s:16:\"profile_7_status\";i:0;s:9:\"profile_7\";s:0:\"\";s:14:\"profile_7_type\";s:4:\"text\";s:21:\"profile_7_placeholder\";s:0:\"\";s:15:\"profile_7_rules\";i:0;s:17:\"profile_7_options\";s:0:\"\";s:16:\"profile_8_status\";i:0;s:9:\"profile_8\";s:0:\"\";s:14:\"profile_8_type\";s:4:\"text\";s:21:\"profile_8_placeholder\";s:0:\"\";s:15:\"profile_8_rules\";i:0;s:17:\"profile_8_options\";s:0:\"\";s:16:\"profile_9_status\";i:0;s:9:\"profile_9\";s:0:\"\";s:14:\"profile_9_type\";s:4:\"text\";s:21:\"profile_9_placeholder\";s:0:\"\";s:15:\"profile_9_rules\";i:0;s:17:\"profile_9_options\";s:0:\"\";s:17:\"profile_10_status\";i:0;s:10:\"profile_10\";s:0:\"\";s:15:\"profile_10_type\";s:4:\"text\";s:22:\"profile_10_placeholder\";s:0:\"\";s:16:\"profile_10_rules\";i:0;s:18:\"profile_10_options\";s:0:\"\";s:17:\"profile_11_status\";i:0;s:10:\"profile_11\";s:0:\"\";s:15:\"profile_11_type\";s:4:\"text\";s:22:\"profile_11_placeholder\";s:0:\"\";s:16:\"profile_11_rules\";i:0;s:18:\"profile_11_options\";s:0:\"\";s:17:\"profile_12_status\";i:0;s:10:\"profile_12\";s:0:\"\";s:15:\"profile_12_type\";s:4:\"text\";s:22:\"profile_12_placeholder\";s:0:\"\";s:16:\"profile_12_rules\";i:0;s:18:\"profile_12_options\";s:0:\"\";s:17:\"profile_13_status\";i:0;s:10:\"profile_13\";s:0:\"\";s:15:\"profile_13_type\";s:4:\"text\";s:22:\"profile_13_placeholder\";s:0:\"\";s:16:\"profile_13_rules\";i:0;s:18:\"profile_13_options\";s:0:\"\";s:17:\"profile_14_status\";i:0;s:10:\"profile_14\";s:0:\"\";s:15:\"profile_14_type\";s:4:\"text\";s:22:\"profile_14_placeholder\";s:0:\"\";s:16:\"profile_14_rules\";i:0;s:18:\"profile_14_options\";s:0:\"\";s:17:\"profile_15_status\";i:0;s:10:\"profile_15\";s:0:\"\";s:15:\"profile_15_type\";s:4:\"text\";s:22:\"profile_15_placeholder\";s:0:\"\";s:16:\"profile_15_rules\";i:0;s:18:\"profile_15_options\";s:0:\"\";s:17:\"profile_16_status\";i:0;s:10:\"profile_16\";s:0:\"\";s:15:\"profile_16_type\";s:4:\"text\";s:22:\"profile_16_placeholder\";s:0:\"\";s:16:\"profile_16_rules\";i:0;s:18:\"profile_16_options\";s:0:\"\";s:17:\"profile_17_status\";i:0;s:10:\"profile_17\";s:0:\"\";s:15:\"profile_17_type\";s:4:\"text\";s:22:\"profile_17_placeholder\";s:0:\"\";s:16:\"profile_17_rules\";i:0;s:18:\"profile_17_options\";s:0:\"\";s:17:\"profile_18_status\";i:0;s:10:\"profile_18\";s:0:\"\";s:15:\"profile_18_type\";s:4:\"text\";s:22:\"profile_18_placeholder\";s:0:\"\";s:16:\"profile_18_rules\";i:0;s:18:\"profile_18_options\";s:0:\"\";s:17:\"profile_19_status\";i:0;s:10:\"profile_19\";s:0:\"\";s:15:\"profile_19_type\";s:4:\"text\";s:22:\"profile_19_placeholder\";s:0:\"\";s:16:\"profile_19_rules\";i:0;s:18:\"profile_19_options\";s:0:\"\";s:17:\"profile_20_status\";i:0;s:10:\"profile_20\";s:0:\"\";s:15:\"profile_20_type\";s:4:\"text\";s:22:\"profile_20_placeholder\";s:0:\"\";s:16:\"profile_20_rules\";i:0;s:18:\"profile_20_options\";s:0:\"\";s:13:\"list_1_forced\";i:0;s:13:\"list_2_forced\";i:0;s:13:\"list_3_forced\";i:0;s:13:\"list_4_forced\";i:0;s:13:\"list_5_forced\";i:0;s:13:\"list_6_forced\";i:0;s:13:\"list_7_forced\";i:0;s:13:\"list_8_forced\";i:0;s:13:\"list_9_forced\";i:0;s:14:\"list_10_forced\";i:0;s:14:\"list_11_forced\";i:0;s:14:\"list_12_forced\";i:0;s:14:\"list_13_forced\";i:0;s:14:\"list_14_forced\";i:0;s:14:\"list_15_forced\";i:0;s:14:\"list_16_forced\";i:0;s:14:\"list_17_forced\";i:0;s:14:\"list_18_forced\";i:0;s:14:\"list_19_forced\";i:0;s:14:\"list_20_forced\";i:0;s:14:\"list_21_forced\";i:0;s:14:\"list_22_forced\";i:0;s:14:\"list_23_forced\";i:0;s:14:\"list_24_forced\";i:0;s:14:\"list_25_forced\";i:0;s:14:\"list_26_forced\";i:0;s:14:\"list_27_forced\";i:0;s:14:\"list_28_forced\";i:0;s:14:\"list_29_forced\";i:0;s:14:\"list_30_forced\";i:0;s:14:\"list_31_forced\";i:0;s:14:\"list_32_forced\";i:0;s:14:\"list_33_forced\";i:0;s:14:\"list_34_forced\";i:0;s:14:\"list_35_forced\";i:0;s:14:\"list_36_forced\";i:0;s:14:\"list_37_forced\";i:0;s:14:\"list_38_forced\";i:0;s:14:\"list_39_forced\";i:0;s:14:\"list_40_forced\";i:0;}', 'yes'),
(1087, 'newsletter_subscription_version', '2.2.7', 'yes'),
(1088, 'newsletter_unsubscription_first_install_time', '1583834072', 'no'),
(1089, 'newsletter_unsubscription', 'a:6:{s:16:\"unsubscribe_text\";s:103:\"<p>Please confirm you want to unsubscribe <a href=\"{unsubscription_confirm_url}\">clicking here</a>.</p>\";s:10:\"error_text\";s:99:\"<p>Subscriber not found, it probably has already been removed. No further actions are required.</p>\";s:17:\"unsubscribed_text\";s:124:\"<p>Your subscription has been deleted. If that was an error you can <a href=\"{reactivate_url}\">subscribe again here</a>.</p>\";s:20:\"unsubscribed_subject\";s:7:\"Goodbye\";s:20:\"unsubscribed_message\";s:87:\"<p>This message confirms that you have unsubscribed from our newsletter. Thank you.</p>\";s:16:\"reactivated_text\";s:46:\"<p>Your subscription has been reactivated.</p>\";}', 'yes'),
(1090, 'newsletter_unsubscription_version', '1.0.3', 'yes'),
(1091, 'newsletter_profile_first_install_time', '1583834072', 'no'),
(1092, 'newsletter_profile_main', 'a:8:{s:4:\"text\";s:188:\"{profile_form}\n    <p>If you change your email address, a confirmation email will be sent to activate it.</p>\n    <p><a href=\"{unsubscription_confirm_url}\">Cancel your subscription</a></p>\";s:13:\"email_changed\";s:81:\"Your email has been changed, an activation email has been sent with instructions.\";s:5:\"error\";s:42:\"Your email is not valid or already in use.\";s:10:\"save_label\";s:4:\"Save\";s:13:\"privacy_label\";s:21:\"Read our privacy note\";s:5:\"saved\";s:14:\"Profile saved.\";s:18:\"export_newsletters\";i:0;s:3:\"url\";s:0:\"\";}', 'yes'),
(1093, 'newsletter_profile_version', '1.1.0', 'yes'),
(1094, 'newsletter_emails_first_install_time', '1583834072', 'no'),
(1095, 'newsletter_emails', 'a:1:{s:5:\"theme\";s:7:\"default\";}', 'yes'),
(1096, 'newsletter_emails_theme_default', 'a:0:{}', 'no'),
(1097, 'newsletter_emails_version', '1.1.5', 'yes'),
(1098, 'newsletter_users_first_install_time', '1583834072', 'no'),
(1099, 'newsletter_users', 'a:0:{}', 'yes'),
(1100, 'newsletter_users_version', '1.3.0', 'yes'),
(1101, 'newsletter_statistics_first_install_time', '1583834072', 'no'),
(1102, 'newsletter_statistics', 'a:1:{s:3:\"key\";s:32:\"5785cca00b51a12adef903c5b8e9fdf8\";}', 'yes'),
(1103, 'newsletter_statistics_version', '1.2.7', 'yes'),
(1104, 'newsletter_install_time', '1583834072', 'no'),
(1105, 'widget_newsletterwidget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1106, 'widget_newsletterwidgetminimal', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1109, 'newsletter_page', '9', 'no'),
(1112, 'newsletter_diagnostic_cron_calls', 'a:100:{i:0;i:1585559010;i:1;i:1585559018;i:2;i:1585559080;i:3;i:1585559440;i:4;i:1585559855;i:5;i:1585559928;i:6;i:1585559975;i:7;i:1585560021;i:8;i:1585560081;i:9;i:1585560109;i:10;i:1585560116;i:11;i:1585560127;i:12;i:1585560133;i:13;i:1585560144;i:14;i:1585560266;i:15;i:1585560356;i:16;i:1585560429;i:17;i:1585560455;i:18;i:1585560458;i:19;i:1585560524;i:20;i:1585560534;i:21;i:1585560594;i:22;i:1585560732;i:23;i:1585560735;i:24;i:1585560756;i:25;i:1585560795;i:26;i:1585560821;i:27;i:1585560823;i:28;i:1585560883;i:29;i:1585560909;i:30;i:1585560925;i:31;i:1585560926;i:32;i:1585560935;i:33;i:1585560957;i:34;i:1585560981;i:35;i:1585561025;i:36;i:1585561030;i:37;i:1585561043;i:38;i:1585561106;i:39;i:1585561111;i:40;i:1585561113;i:41;i:1585561162;i:42;i:1585561185;i:43;i:1585561257;i:44;i:1585561478;i:45;i:1585561798;i:46;i:1585566404;i:47;i:1585566628;i:48;i:1585567116;i:49;i:1585567175;i:50;i:1585567685;i:51;i:1585568512;i:52;i:1585569440;i:53;i:1585570113;i:54;i:1585570173;i:55;i:1585570547;i:56;i:1585570879;i:57;i:1585571213;i:58;i:1585571393;i:59;i:1585571678;i:60;i:1585571993;i:61;i:1585572097;i:62;i:1585572309;i:63;i:1585653454;i:64;i:1585653539;i:65;i:1585653568;i:66;i:1585653801;i:67;i:1585653862;i:68;i:1585654057;i:69;i:1585654140;i:70;i:1585654160;i:71;i:1585654175;i:72;i:1585654179;i:73;i:1585654192;i:74;i:1585654204;i:75;i:1585654219;i:76;i:1585654521;i:77;i:1585654763;i:78;i:1585654872;i:79;i:1599654555;i:80;i:1599654585;i:81;i:1599654589;i:82;i:1599654593;i:83;i:1599654731;i:84;i:1599654747;i:85;i:1599654757;i:86;i:1599655048;i:87;i:1599655311;i:88;i:1599655635;i:89;i:1599655973;i:90;i:1599655998;i:91;i:1599656054;i:92;i:1599656062;i:93;i:1599656063;i:94;i:1599656367;i:95;i:1599656668;i:96;i:1599657032;i:97;i:1599657273;i:98;i:1599658690;i:99;i:1599658708;}', 'no'),
(1113, 'newsletter_diagnostic_cron_data', 'a:3:{s:4:\"mean\";d:140995.98;s:3:\"max\";i:13999683;s:3:\"min\";i:1;}', 'no'),
(1138, '_transient_wpassetcleanup_do_activation_redirect_first_time', '1', 'yes'),
(1141, 'wpassetcleanup_first_usage', '1583836277', 'no'),
(1142, 'wpassetcleanup_tracking_notice', '1', 'yes'),
(1147, 'wpassetcleanup_global_data', '{\"styles\":{\"assets_info\":{\"wp-block-library\":{\"src\":\"\\/wp-includes\\/css\\/dist\\/block-library\\/style.min.css\",\"extra\":{\"rtl\":\"replace\",\"suffix\":\".min\"}},\"contact-form-7\":{\"src\":\"https:\\/\\/localhost\\/wordpress-bootstrap-4-boilerplate\\/wp-content\\/plugins\\/contact-form-7\\/includes\\/css\\/styles.css\",\"ver\":\"5.1.7\",\"args\":\"all\"},\"newsletter\":{\"src\":\"https:\\/\\/localhost\\/wordpress-bootstrap-4-boilerplate\\/wp-content\\/plugins\\/newsletter\\/style.css\",\"ver\":\"6.5.4\",\"args\":\"all\"},\"current-template-style\":{\"src\":\"https:\\/\\/localhost\\/wordpress-bootstrap-4-boilerplate\\/wp-content\\/plugins\\/show-current-template\\/css\\/style.css\",\"args\":\"all\"},\"moove_gdpr_frontend\":{\"src\":\"https:\\/\\/localhost\\/wordpress-bootstrap-4-boilerplate\\/wp-content\\/plugins\\/gdpr-cookie-compliance\\/dist\\/styles\\/gdpr-main.css\",\"ver\":\"4.1.6\",\"args\":\"all\"}}},\"scripts\":{\"assets_info\":{\"jquery-core\":{\"src\":\"\\/wp-includes\\/js\\/jquery\\/jquery.js\",\"ver\":\"1.12.4-wp\"},\"jquery-migrate\":{\"src\":\"\\/wp-includes\\/js\\/jquery\\/jquery-migrate.min.js\",\"ver\":\"1.4.1\"},\"jquery\":{\"deps\":[\"jquery-core\",\"jquery-migrate\"],\"ver\":\"1.12.4-wp\"},\"contact-form-7\":{\"src\":\"https:\\/\\/localhost\\/wordpress-bootstrap-4-boilerplate\\/wp-content\\/plugins\\/contact-form-7\\/includes\\/js\\/scripts.js\",\"deps\":[\"jquery\"],\"ver\":\"5.1.7\",\"extra\":{\"group\":1,\"data\":\"var wpcf7 = {\\\"apiSettings\\\":{\\\"root\\\":\\\"https:\\\\\\/\\\\\\/localhost\\\\\\/wordpress-bootstrap-4-boilerplate\\\\\\/wp-json\\\\\\/contact-form-7\\\\\\/v1\\\",\\\"namespace\\\":\\\"contact-form-7\\\\\\/v1\\\"}};\"}},\"newsletter-subscription\":{\"src\":\"https:\\/\\/localhost\\/wordpress-bootstrap-4-boilerplate\\/wp-content\\/plugins\\/newsletter\\/subscription\\/validate.js\",\"ver\":\"6.5.4\",\"extra\":{\"group\":1,\"data\":\"var newsletter = {\\\"messages\\\":{\\\"email_error\\\":\\\"Email address is not correct\\\",\\\"name_error\\\":\\\"Name is required\\\",\\\"surname_error\\\":\\\"Last name is required\\\",\\\"profile_error\\\":\\\"A mandatory field is not filled in\\\",\\\"privacy_error\\\":\\\"You must accept the privacy policy\\\"},\\\"profile_max\\\":\\\"20\\\"};\"}},\"themeplate-script\":{\"src\":\"https:\\/\\/localhost\\/wordpress-bootstrap-4-boilerplate\\/wp-content\\/themes\\/themeplate-child\\/assets\\/js\\/themeplate.js\",\"ver\":\"0.1.0\",\"extra\":{\"group\":1,\"data\":\"var themeplate_options = {\\\"ajaxurl\\\":\\\"https:\\\\\\/\\\\\\/localhost\\\\\\/wordpress-bootstrap-4-boilerplate\\\\\\/wp-admin\\\\\\/admin-ajax.php\\\"};\"}},\"moove_gdpr_frontend\":{\"src\":\"https:\\/\\/localhost\\/wordpress-bootstrap-4-boilerplate\\/wp-content\\/plugins\\/gdpr-cookie-compliance\\/dist\\/scripts\\/main.js\",\"deps\":[\"jquery\"],\"ver\":\"4.1.6\",\"extra\":{\"group\":1,\"data\":\"var moove_frontend_gdpr_scripts = {\\\"ajaxurl\\\":\\\"https:\\\\\\/\\\\\\/localhost\\\\\\/wordpress-bootstrap-4-boilerplate\\\\\\/wp-admin\\\\\\/admin-ajax.php\\\",\\\"post_id\\\":\\\"2\\\",\\\"plugin_dir\\\":\\\"https:\\\\\\/\\\\\\/localhost\\\\\\/wordpress-bootstrap-4-boilerplate\\\\\\/wp-content\\\\\\/plugins\\\\\\/gdpr-cookie-compliance\\\",\\\"is_page\\\":\\\"1\\\",\\\"strict_init\\\":\\\"1\\\",\\\"enabled_default\\\":{\\\"third_party\\\":0,\\\"advanced\\\":0},\\\"geo_location\\\":\\\"false\\\",\\\"force_reload\\\":\\\"false\\\",\\\"is_single\\\":\\\"\\\",\\\"current_user\\\":\\\"1\\\",\\\"load_lity\\\":\\\"true\\\",\\\"cookie_expiration\\\":\\\"365\\\"};\"}}},\"preloads\":{\"contact-form-7\":\"basic\"}}}', 'no'),
(1150, 'wpassetcleanup_settings', '{\"dashboard_show\":\"1\",\"dom_get_type\":\"direct\",\"frontend_show_exceptions\":\"et_fb=1\\nct_builder=true\\nvc_editable=true\\npreview_nonce=\\n\",\"assets_list_layout\":\"by-location\",\"assets_list_layout_areas_status\":\"expanded\",\"assets_list_layout_plugin_area_status\":\"expanded\",\"assets_list_inline_code_status\":\"contracted\",\"minify_loaded_css_exceptions\":\"(.*?)\\\\.min.css\\n\\/plugins\\/wd-instagram-feed\\/(.*?).css\",\"minify_loaded_js_exceptions\":\"(.*?)\\\\.min.js\\n\\/plugins\\/wd-instagram-feed\\/(.*?).js\",\"inline_css_files_below_size\":\"1\",\"inline_css_files_below_size_input\":\"3\",\"inline_js_files_below_size_input\":\"3\",\"move_scripts_to_body_exceptions\":\"\\/\\/cdn.ampproject.org\\/\",\"combine_loaded_css_exceptions\":\"\\/plugins\\/wd-instagram-feed\\/(.*?).css\",\"combine_loaded_css_append_handle_extra\":\"1\",\"combine_loaded_js_exceptions\":\"\\/plugins\\/wd-instagram-feed\\/(.*?).js\",\"combine_loaded_js_append_handle_extra\":\"1\",\"defer_css_loaded_body\":\"moved\",\"cache_dynamic_loaded_css\":1,\"cache_dynamic_loaded_js\":1,\"input_style\":\"enhanced\",\"hide_core_files\":\"1\",\"fetch_cached_files_details_from\":\"disk\",\"clear_cached_files_after\":\"10\"}', 'no'),
(1151, 'wpassetcleanup_global_unload', '{\"styles\":[\"contact-form-7\",\"moove_gdpr_frontend\"]}', 'no'),
(1152, 'wpassetcleanup_bulk_unload', '[]', 'no'),
(1207, 'new_admin_email', 'doxa84@hotmail.com', 'yes'),
(1429, 'acf_version', '5.9.1', 'yes'),
(1432, 'cptui_post_types', 'a:6:{s:8:\"our_team\";a:30:{s:4:\"name\";s:8:\"our_team\";s:5:\"label\";s:8:\"Our Team\";s:14:\"singular_label\";s:8:\"Our Team\";s:11:\"description\";s:0:\"\";s:6:\"public\";s:4:\"true\";s:18:\"publicly_queryable\";s:4:\"true\";s:7:\"show_ui\";s:4:\"true\";s:17:\"show_in_nav_menus\";s:4:\"true\";s:16:\"delete_with_user\";s:5:\"false\";s:12:\"show_in_rest\";s:4:\"true\";s:9:\"rest_base\";s:0:\"\";s:21:\"rest_controller_class\";s:0:\"\";s:11:\"has_archive\";s:4:\"true\";s:18:\"has_archive_string\";s:0:\"\";s:19:\"exclude_from_search\";s:5:\"false\";s:15:\"capability_type\";s:4:\"post\";s:12:\"hierarchical\";s:4:\"true\";s:7:\"rewrite\";s:4:\"true\";s:12:\"rewrite_slug\";s:0:\"\";s:17:\"rewrite_withfront\";s:4:\"true\";s:9:\"query_var\";s:4:\"true\";s:14:\"query_var_slug\";s:0:\"\";s:13:\"menu_position\";s:2:\"20\";s:12:\"show_in_menu\";s:4:\"true\";s:19:\"show_in_menu_string\";s:0:\"\";s:9:\"menu_icon\";s:0:\"\";s:8:\"supports\";a:3:{i:0;s:5:\"title\";i:1;s:6:\"editor\";i:2;s:9:\"thumbnail\";}s:10:\"taxonomies\";a:0:{}s:6:\"labels\";a:30:{s:9:\"menu_name\";s:8:\"Our Team\";s:9:\"all_items\";s:12:\"All Our Team\";s:7:\"add_new\";s:7:\"Add new\";s:12:\"add_new_item\";s:16:\"Add new Our Team\";s:9:\"edit_item\";s:13:\"Edit Our Team\";s:8:\"new_item\";s:12:\"New Our Team\";s:9:\"view_item\";s:13:\"View Our Team\";s:10:\"view_items\";s:13:\"View Our Team\";s:12:\"search_items\";s:15:\"Search Our Team\";s:9:\"not_found\";s:17:\"No Our Team found\";s:18:\"not_found_in_trash\";s:26:\"No Our Team found in trash\";s:6:\"parent\";s:16:\"Parent Our Team:\";s:14:\"featured_image\";s:32:\"Featured image for this Our Team\";s:18:\"set_featured_image\";s:36:\"Set featured image for this Our Team\";s:21:\"remove_featured_image\";s:39:\"Remove featured image for this Our Team\";s:18:\"use_featured_image\";s:39:\"Use as featured image for this Our Team\";s:8:\"archives\";s:17:\"Our Team archives\";s:16:\"insert_into_item\";s:20:\"Insert into Our Team\";s:21:\"uploaded_to_this_item\";s:23:\"Upload to this Our Team\";s:17:\"filter_items_list\";s:20:\"Filter Our Team list\";s:21:\"items_list_navigation\";s:24:\"Our Team list navigation\";s:10:\"items_list\";s:13:\"Our Team list\";s:10:\"attributes\";s:19:\"Our Team attributes\";s:14:\"name_admin_bar\";s:8:\"Our Team\";s:14:\"item_published\";s:18:\"Our Team published\";s:24:\"item_published_privately\";s:29:\"Our Team published privately.\";s:22:\"item_reverted_to_draft\";s:27:\"Our Team reverted to draft.\";s:14:\"item_scheduled\";s:18:\"Our Team scheduled\";s:12:\"item_updated\";s:17:\"Our Team updated.\";s:17:\"parent_item_colon\";s:16:\"Parent Our Team:\";}s:15:\"custom_supports\";s:0:\"\";}s:11:\"testimonial\";a:30:{s:4:\"name\";s:11:\"testimonial\";s:5:\"label\";s:11:\"Testimonial\";s:14:\"singular_label\";s:12:\"Testimonials\";s:11:\"description\";s:0:\"\";s:6:\"public\";s:4:\"true\";s:18:\"publicly_queryable\";s:4:\"true\";s:7:\"show_ui\";s:4:\"true\";s:17:\"show_in_nav_menus\";s:4:\"true\";s:16:\"delete_with_user\";s:5:\"false\";s:12:\"show_in_rest\";s:4:\"true\";s:9:\"rest_base\";s:0:\"\";s:21:\"rest_controller_class\";s:0:\"\";s:11:\"has_archive\";s:5:\"false\";s:18:\"has_archive_string\";s:0:\"\";s:19:\"exclude_from_search\";s:5:\"false\";s:15:\"capability_type\";s:4:\"post\";s:12:\"hierarchical\";s:4:\"true\";s:7:\"rewrite\";s:4:\"true\";s:12:\"rewrite_slug\";s:0:\"\";s:17:\"rewrite_withfront\";s:4:\"true\";s:9:\"query_var\";s:4:\"true\";s:14:\"query_var_slug\";s:0:\"\";s:13:\"menu_position\";s:2:\"21\";s:12:\"show_in_menu\";s:4:\"true\";s:19:\"show_in_menu_string\";s:0:\"\";s:9:\"menu_icon\";s:0:\"\";s:8:\"supports\";a:2:{i:0;s:5:\"title\";i:1;s:6:\"editor\";}s:10:\"taxonomies\";a:0:{}s:6:\"labels\";a:30:{s:9:\"menu_name\";s:11:\"Testimonial\";s:9:\"all_items\";s:15:\"All Testimonial\";s:7:\"add_new\";s:7:\"Add new\";s:12:\"add_new_item\";s:20:\"Add new Testimonials\";s:9:\"edit_item\";s:17:\"Edit Testimonials\";s:8:\"new_item\";s:16:\"New Testimonials\";s:9:\"view_item\";s:17:\"View Testimonials\";s:10:\"view_items\";s:16:\"View Testimonial\";s:12:\"search_items\";s:18:\"Search Testimonial\";s:9:\"not_found\";s:20:\"No Testimonial found\";s:18:\"not_found_in_trash\";s:29:\"No Testimonial found in trash\";s:6:\"parent\";s:20:\"Parent Testimonials:\";s:14:\"featured_image\";s:36:\"Featured image for this Testimonials\";s:18:\"set_featured_image\";s:40:\"Set featured image for this Testimonials\";s:21:\"remove_featured_image\";s:43:\"Remove featured image for this Testimonials\";s:18:\"use_featured_image\";s:43:\"Use as featured image for this Testimonials\";s:8:\"archives\";s:21:\"Testimonials archives\";s:16:\"insert_into_item\";s:24:\"Insert into Testimonials\";s:21:\"uploaded_to_this_item\";s:27:\"Upload to this Testimonials\";s:17:\"filter_items_list\";s:23:\"Filter Testimonial list\";s:21:\"items_list_navigation\";s:27:\"Testimonial list navigation\";s:10:\"items_list\";s:16:\"Testimonial list\";s:10:\"attributes\";s:22:\"Testimonial attributes\";s:14:\"name_admin_bar\";s:12:\"Testimonials\";s:14:\"item_published\";s:22:\"Testimonials published\";s:24:\"item_published_privately\";s:33:\"Testimonials published privately.\";s:22:\"item_reverted_to_draft\";s:31:\"Testimonials reverted to draft.\";s:14:\"item_scheduled\";s:22:\"Testimonials scheduled\";s:12:\"item_updated\";s:21:\"Testimonials updated.\";s:17:\"parent_item_colon\";s:20:\"Parent Testimonials:\";}s:15:\"custom_supports\";s:0:\"\";}s:7:\"partner\";a:30:{s:4:\"name\";s:7:\"partner\";s:5:\"label\";s:7:\"Partner\";s:14:\"singular_label\";s:9:\"Partnerek\";s:11:\"description\";s:0:\"\";s:6:\"public\";s:4:\"true\";s:18:\"publicly_queryable\";s:4:\"true\";s:7:\"show_ui\";s:4:\"true\";s:17:\"show_in_nav_menus\";s:4:\"true\";s:16:\"delete_with_user\";s:5:\"false\";s:12:\"show_in_rest\";s:4:\"true\";s:9:\"rest_base\";s:0:\"\";s:21:\"rest_controller_class\";s:0:\"\";s:11:\"has_archive\";s:5:\"false\";s:18:\"has_archive_string\";s:0:\"\";s:19:\"exclude_from_search\";s:5:\"false\";s:15:\"capability_type\";s:4:\"post\";s:12:\"hierarchical\";s:4:\"true\";s:7:\"rewrite\";s:4:\"true\";s:12:\"rewrite_slug\";s:0:\"\";s:17:\"rewrite_withfront\";s:4:\"true\";s:9:\"query_var\";s:4:\"true\";s:14:\"query_var_slug\";s:0:\"\";s:13:\"menu_position\";s:2:\"22\";s:12:\"show_in_menu\";s:4:\"true\";s:19:\"show_in_menu_string\";s:0:\"\";s:9:\"menu_icon\";s:0:\"\";s:8:\"supports\";a:2:{i:0;s:5:\"title\";i:1;s:9:\"thumbnail\";}s:10:\"taxonomies\";a:0:{}s:6:\"labels\";a:30:{s:9:\"menu_name\";s:7:\"Partner\";s:9:\"all_items\";s:11:\"All Partner\";s:7:\"add_new\";s:7:\"Add new\";s:12:\"add_new_item\";s:17:\"Add new Partnerek\";s:9:\"edit_item\";s:14:\"Edit Partnerek\";s:8:\"new_item\";s:13:\"New Partnerek\";s:9:\"view_item\";s:14:\"View Partnerek\";s:10:\"view_items\";s:12:\"View Partner\";s:12:\"search_items\";s:14:\"Search Partner\";s:9:\"not_found\";s:16:\"No Partner found\";s:18:\"not_found_in_trash\";s:25:\"No Partner found in trash\";s:6:\"parent\";s:17:\"Parent Partnerek:\";s:14:\"featured_image\";s:33:\"Featured image for this Partnerek\";s:18:\"set_featured_image\";s:37:\"Set featured image for this Partnerek\";s:21:\"remove_featured_image\";s:40:\"Remove featured image for this Partnerek\";s:18:\"use_featured_image\";s:40:\"Use as featured image for this Partnerek\";s:8:\"archives\";s:18:\"Partnerek archives\";s:16:\"insert_into_item\";s:21:\"Insert into Partnerek\";s:21:\"uploaded_to_this_item\";s:24:\"Upload to this Partnerek\";s:17:\"filter_items_list\";s:19:\"Filter Partner list\";s:21:\"items_list_navigation\";s:23:\"Partner list navigation\";s:10:\"items_list\";s:12:\"Partner list\";s:10:\"attributes\";s:18:\"Partner attributes\";s:14:\"name_admin_bar\";s:9:\"Partnerek\";s:14:\"item_published\";s:19:\"Partnerek published\";s:24:\"item_published_privately\";s:30:\"Partnerek published privately.\";s:22:\"item_reverted_to_draft\";s:28:\"Partnerek reverted to draft.\";s:14:\"item_scheduled\";s:19:\"Partnerek scheduled\";s:12:\"item_updated\";s:18:\"Partnerek updated.\";s:17:\"parent_item_colon\";s:17:\"Parent Partnerek:\";}s:15:\"custom_supports\";s:0:\"\";}s:12:\"publications\";a:30:{s:4:\"name\";s:12:\"publications\";s:5:\"label\";s:13:\"Publikációk\";s:14:\"singular_label\";s:12:\"Publikáció\";s:11:\"description\";s:0:\"\";s:6:\"public\";s:4:\"true\";s:18:\"publicly_queryable\";s:4:\"true\";s:7:\"show_ui\";s:4:\"true\";s:17:\"show_in_nav_menus\";s:4:\"true\";s:16:\"delete_with_user\";s:5:\"false\";s:12:\"show_in_rest\";s:4:\"true\";s:9:\"rest_base\";s:0:\"\";s:21:\"rest_controller_class\";s:0:\"\";s:11:\"has_archive\";s:4:\"true\";s:18:\"has_archive_string\";s:13:\"publikacioink\";s:19:\"exclude_from_search\";s:5:\"false\";s:15:\"capability_type\";s:4:\"post\";s:12:\"hierarchical\";s:4:\"true\";s:7:\"rewrite\";s:4:\"true\";s:12:\"rewrite_slug\";s:0:\"\";s:17:\"rewrite_withfront\";s:4:\"true\";s:9:\"query_var\";s:4:\"true\";s:14:\"query_var_slug\";s:0:\"\";s:13:\"menu_position\";s:0:\"\";s:12:\"show_in_menu\";s:4:\"true\";s:19:\"show_in_menu_string\";s:0:\"\";s:9:\"menu_icon\";s:0:\"\";s:8:\"supports\";a:3:{i:0;s:5:\"title\";i:1;s:6:\"editor\";i:2;s:9:\"thumbnail\";}s:10:\"taxonomies\";a:1:{i:0;s:8:\"category\";}s:6:\"labels\";a:30:{s:9:\"menu_name\";s:13:\"Publikációk\";s:9:\"all_items\";s:21:\"Összes Publikációk\";s:7:\"add_new\";s:16:\"Új hozzáadása\";s:12:\"add_new_item\";s:16:\"Új Publikáció\";s:9:\"edit_item\";s:26:\"Publikáció szerkesztése\";s:8:\"new_item\";s:16:\"Új Publikáció\";s:9:\"view_item\";s:12:\"Publikáció\";s:10:\"view_items\";s:26:\"Publikációk megtekintés\";s:12:\"search_items\";s:22:\"Publikációk keresés\";s:9:\"not_found\";s:28:\"Nem található Publikáció\";s:18:\"not_found_in_trash\";s:41:\"Nem található Publikáció a lomtárban\";s:6:\"parent\";s:21:\"Szülő Publikáció:\";s:14:\"featured_image\";s:30:\"Kiemelt kép a Publikációhoz\";s:18:\"set_featured_image\";s:42:\"Kiemelt kép beállítása Publikációhoz\";s:21:\"remove_featured_image\";s:41:\"Kiemelt kép eltávolítása Publikáció\";s:18:\"use_featured_image\";s:37:\"Kiemelt kép használata Publikáció\";s:8:\"archives\";s:21:\"Publikáció archives\";s:16:\"insert_into_item\";s:25:\"Publikációhoz rendelés\";s:21:\"uploaded_to_this_item\";s:27:\"Feltöltés Publikációhoz\";s:17:\"filter_items_list\";s:32:\"Publikációk szürési listája\";s:21:\"items_list_navigation\";s:31:\"Publikációk lista navigáció\";s:10:\"items_list\";s:19:\"Publikációk lista\";s:10:\"attributes\";s:27:\"Publikációk attribútumai\";s:14:\"name_admin_bar\";s:12:\"Publikáció\";s:14:\"item_published\";s:25:\"Publikáció közzétéve\";s:24:\"item_published_privately\";s:36:\"Publikáció közzétéve privátban\";s:22:\"item_reverted_to_draft\";s:31:\"Publikáció vázlatba helyezve\";s:14:\"item_scheduled\";s:23:\"Publikáció időzítve\";s:12:\"item_updated\";s:23:\"Publikáció frissítve\";s:17:\"parent_item_colon\";s:21:\"Szülő Publikáció:\";}s:15:\"custom_supports\";s:0:\"\";}s:12:\"int_contacts\";a:30:{s:4:\"name\";s:12:\"int_contacts\";s:5:\"label\";s:23:\"Nemzetközi kapcsolatok\";s:14:\"singular_label\";s:21:\"Nemzetközi kapcsolat\";s:11:\"description\";s:0:\"\";s:6:\"public\";s:4:\"true\";s:18:\"publicly_queryable\";s:4:\"true\";s:7:\"show_ui\";s:4:\"true\";s:17:\"show_in_nav_menus\";s:4:\"true\";s:16:\"delete_with_user\";s:5:\"false\";s:12:\"show_in_rest\";s:4:\"true\";s:9:\"rest_base\";s:0:\"\";s:21:\"rest_controller_class\";s:0:\"\";s:11:\"has_archive\";s:5:\"false\";s:18:\"has_archive_string\";s:0:\"\";s:19:\"exclude_from_search\";s:5:\"false\";s:15:\"capability_type\";s:4:\"post\";s:12:\"hierarchical\";s:4:\"true\";s:7:\"rewrite\";s:4:\"true\";s:12:\"rewrite_slug\";s:0:\"\";s:17:\"rewrite_withfront\";s:4:\"true\";s:9:\"query_var\";s:4:\"true\";s:14:\"query_var_slug\";s:0:\"\";s:13:\"menu_position\";s:0:\"\";s:12:\"show_in_menu\";s:4:\"true\";s:19:\"show_in_menu_string\";s:0:\"\";s:9:\"menu_icon\";s:0:\"\";s:8:\"supports\";a:2:{i:0;s:5:\"title\";i:1;s:6:\"editor\";}s:10:\"taxonomies\";a:0:{}s:6:\"labels\";a:30:{s:9:\"menu_name\";s:23:\"Nemzetközi kapcsolatok\";s:9:\"all_items\";s:31:\"Összes Nemzetközi kapcsolatok\";s:7:\"add_new\";s:16:\"Új hozzáadása\";s:12:\"add_new_item\";s:25:\"Új Nemzetközi kapcsolat\";s:9:\"edit_item\";s:35:\"Nemzetközi kapcsolat szerkesztése\";s:8:\"new_item\";s:25:\"Új Nemzetközi kapcsolat\";s:9:\"view_item\";s:35:\"Nemzetközi kapcsolat megtekintése\";s:10:\"view_items\";s:37:\"Nemzetközi kapcsolatok megtekintése\";s:12:\"search_items\";s:33:\"Nemzetközi kapcsolatok keresése\";s:9:\"not_found\";s:39:\"Nem található Nemzetközi kapcsolatok\";s:18:\"not_found_in_trash\";s:52:\"Nem található Nemzetközi kapcsolatok a lomtárban\";s:6:\"parent\";s:30:\"Szülő Nemzetközi kapcsolat:\";s:14:\"featured_image\";s:39:\"Kiemelt kép a Nemzetközi kapcsolathoz\";s:18:\"set_featured_image\";s:51:\"Kiemelt kép beállítása Nemzetközi kapcsolathoz\";s:21:\"remove_featured_image\";s:54:\"Kiemelt kép eltávolítása Nemzetközi kapcsolatból\";s:18:\"use_featured_image\";s:49:\"Kiemelt kép használata Nemzetközi kapcsolathou\";s:8:\"archives\";s:30:\"Nemzetközi kapcsolat archives\";s:16:\"insert_into_item\";s:34:\"Nemzetközi kapcsolatba illesztés\";s:21:\"uploaded_to_this_item\";s:36:\"Feltöltés Nemzetközi kapcsolathoz\";s:17:\"filter_items_list\";s:40:\"Szürő lista Nemzetközi kapcsolatokhoz\";s:21:\"items_list_navigation\";s:41:\"Nemzetközi kapcsolatok lista navigáció\";s:10:\"items_list\";s:29:\"Nemzetközi kapcsolatok lista\";s:10:\"attributes\";s:37:\"Nemzetközi kapcsolatok attribútumok\";s:14:\"name_admin_bar\";s:21:\"Nemzetközi kapcsolat\";s:14:\"item_published\";s:34:\"Nemzetközi kapcsolat közzétéve\";s:24:\"item_published_privately\";s:45:\"Nemzetközi kapcsolat közzétéve privátban\";s:22:\"item_reverted_to_draft\";s:37:\"Nemzetközi kapcsolat vázlatba téve\";s:14:\"item_scheduled\";s:32:\"Nemzetközi kapcsolat időzítve\";s:12:\"item_updated\";s:32:\"Nemzetközi kapcsolat frissítve\";s:17:\"parent_item_colon\";s:30:\"Szülő Nemzetközi kapcsolat:\";}s:15:\"custom_supports\";s:0:\"\";}s:6:\"events\";a:30:{s:4:\"name\";s:6:\"events\";s:5:\"label\";s:10:\"Események\";s:14:\"singular_label\";s:8:\"Esemény\";s:11:\"description\";s:0:\"\";s:6:\"public\";s:4:\"true\";s:18:\"publicly_queryable\";s:4:\"true\";s:7:\"show_ui\";s:4:\"true\";s:17:\"show_in_nav_menus\";s:4:\"true\";s:16:\"delete_with_user\";s:5:\"false\";s:12:\"show_in_rest\";s:4:\"true\";s:9:\"rest_base\";s:0:\"\";s:21:\"rest_controller_class\";s:0:\"\";s:11:\"has_archive\";s:4:\"true\";s:18:\"has_archive_string\";s:18:\"hirek-es-esemenyek\";s:19:\"exclude_from_search\";s:5:\"false\";s:15:\"capability_type\";s:4:\"post\";s:12:\"hierarchical\";s:4:\"true\";s:7:\"rewrite\";s:4:\"true\";s:12:\"rewrite_slug\";s:0:\"\";s:17:\"rewrite_withfront\";s:4:\"true\";s:9:\"query_var\";s:4:\"true\";s:14:\"query_var_slug\";s:0:\"\";s:13:\"menu_position\";s:0:\"\";s:12:\"show_in_menu\";s:4:\"true\";s:19:\"show_in_menu_string\";s:0:\"\";s:9:\"menu_icon\";s:0:\"\";s:8:\"supports\";a:3:{i:0;s:5:\"title\";i:1;s:6:\"editor\";i:2;s:9:\"thumbnail\";}s:10:\"taxonomies\";a:2:{i:0;s:8:\"category\";i:1;s:8:\"post_tag\";}s:6:\"labels\";a:30:{s:9:\"menu_name\";s:10:\"Események\";s:9:\"all_items\";s:18:\"Összes Események\";s:7:\"add_new\";s:16:\"Új hozzáadása\";s:12:\"add_new_item\";s:25:\"Új Esemény hozzáadása\";s:9:\"edit_item\";s:22:\"Esemény szerkesztése\";s:8:\"new_item\";s:12:\"Új Esemény\";s:9:\"view_item\";s:22:\"Esemény megtekintése\";s:10:\"view_items\";s:24:\"Események megtekintése\";s:12:\"search_items\";s:18:\"Esemény keresése\";s:9:\"not_found\";s:24:\"Nem található esemény\";s:18:\"not_found_in_trash\";s:37:\"Nem található esemény a lomtárban\";s:6:\"parent\";s:17:\"Szülő Esemény:\";s:14:\"featured_image\";s:27:\"Kiemelt kép az Eseményhez\";s:18:\"set_featured_image\";s:38:\"Kiemelt kép beállítása Eseményhez\";s:21:\"remove_featured_image\";s:41:\"Kiemelt kép eltávolítása Eseményből\";s:18:\"use_featured_image\";s:36:\"Kiemelt kép használata Eseményhez\";s:8:\"archives\";s:17:\"Esemény archives\";s:16:\"insert_into_item\";s:25:\"Elemben való elhelyezés\";s:21:\"uploaded_to_this_item\";s:23:\"Feltöltés Eseményhez\";s:17:\"filter_items_list\";s:27:\"Szürő lista Eseményekhez\";s:21:\"items_list_navigation\";s:28:\"Események lista navigáció\";s:10:\"items_list\";s:16:\"Események lista\";s:10:\"attributes\";s:24:\"Események attribútumai\";s:14:\"name_admin_bar\";s:8:\"Esemény\";s:14:\"item_published\";s:21:\"Esemény közzétéve\";s:24:\"item_published_privately\";s:32:\"Esemény közzétéve privátban\";s:22:\"item_reverted_to_draft\";s:27:\"Esemény vázlatba helyezve\";s:14:\"item_scheduled\";s:19:\"Esemény időzítve\";s:12:\"item_updated\";s:19:\"Esemény frissítve\";s:17:\"parent_item_colon\";s:17:\"Szülő Esemény:\";}s:15:\"custom_supports\";s:0:\"\";}}', 'yes'),
(1826, 'wpmudev_recommended_plugins_registered', 'a:1:{s:23:\"wp-smushit/wp-smush.php\";a:1:{s:13:\"registered_at\";i:1585057547;}}', 'no'),
(2324, 'cfdb7_view_ignore_notice', 'true', 'yes'),
(2365, 'auto_core_update_notified', 'a:4:{s:4:\"type\";s:7:\"success\";s:5:\"email\";s:18:\"doxa84@hotmail.com\";s:7:\"version\";s:5:\"5.3.4\";s:9:\"timestamp\";i:1599654577;}', 'no'),
(2386, 'disallowed_keys', '', 'no'),
(2387, 'comment_previously_approved', '1', 'yes'),
(2388, 'auto_plugin_theme_update_emails', 'a:0:{}', 'no'),
(2389, 'finished_updating_comment_type', '1', 'yes'),
(2392, 'can_compress_scripts', '1', 'no'),
(2395, 'yoast_migrations_free', 'a:1:{s:7:\"version\";s:4:\"14.9\";}', 'yes'),
(2411, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:2:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/hu_HU/wordpress-5.5.1.zip\";s:6:\"locale\";s:5:\"hu_HU\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/hu_HU/wordpress-5.5.1.zip\";s:10:\"no_content\";s:0:\"\";s:11:\"new_bundled\";s:0:\"\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"5.5.1\";s:7:\"version\";s:5:\"5.5.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";}i:1;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.5.1.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.5.1.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.5.1-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.5.1-new-bundled.zip\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"5.5.1\";s:7:\"version\";s:5:\"5.5.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1600773005;s:15:\"version_checked\";s:5:\"5.5.1\";s:12:\"translations\";a:0:{}}', 'no'),
(2422, '_site_transient_update_themes', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1600773004;s:7:\"checked\";a:4:{s:16:\"themeplate-child\";s:5:\"0.1.0\";s:10:\"themeplate\";s:5:\"0.1.0\";s:14:\"twentynineteen\";s:3:\"1.7\";s:12:\"twentytwenty\";s:3:\"1.5\";}s:8:\"response\";a:0:{}s:9:\"no_update\";a:2:{s:14:\"twentynineteen\";a:6:{s:5:\"theme\";s:14:\"twentynineteen\";s:11:\"new_version\";s:3:\"1.7\";s:3:\"url\";s:44:\"https://wordpress.org/themes/twentynineteen/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/theme/twentynineteen.1.7.zip\";s:8:\"requires\";s:5:\"4.9.6\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:12:\"twentytwenty\";a:6:{s:5:\"theme\";s:12:\"twentytwenty\";s:11:\"new_version\";s:3:\"1.5\";s:3:\"url\";s:42:\"https://wordpress.org/themes/twentytwenty/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/theme/twentytwenty.1.5.zip\";s:8:\"requires\";s:3:\"4.7\";s:12:\"requires_php\";s:5:\"5.2.4\";}}s:12:\"translations\";a:0:{}}', 'no'),
(2474, '_transient_health-check-site-status-result', '{\"good\":\"12\",\"recommended\":\"8\",\"critical\":\"0\"}', 'yes'),
(2641, 'category_children', 'a:0:{}', 'yes'),
(2758, '_site_transient_timeout_php_check_a4e7a3af7060c530d791075f6e3eb5fa', '1601015356', 'no'),
(2759, '_site_transient_php_check_a4e7a3af7060c530d791075f6e3eb5fa', 'a:5:{s:19:\"recommended_version\";s:3:\"7.4\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(2838, 'rewrite_rules', 'a:179:{s:19:\"sitemap_index\\.xml$\";s:19:\"index.php?sitemap=1\";s:31:\"([^/]+?)-sitemap([0-9]+)?\\.xml$\";s:51:\"index.php?sitemap=$matches[1]&sitemap_n=$matches[2]\";s:24:\"([a-z]+)?-?sitemap\\.xsl$\";s:39:\"index.php?yoast-sitemap-xsl=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:17:\"^wp-sitemap\\.xml$\";s:23:\"index.php?sitemap=index\";s:17:\"^wp-sitemap\\.xsl$\";s:36:\"index.php?sitemap-stylesheet=sitemap\";s:23:\"^wp-sitemap-index\\.xsl$\";s:34:\"index.php?sitemap-stylesheet=index\";s:48:\"^wp-sitemap-([a-z]+?)-([a-z\\d_-]+?)-(\\d+?)\\.xml$\";s:75:\"index.php?sitemap=$matches[1]&sitemap-subtype=$matches[2]&paged=$matches[3]\";s:34:\"^wp-sitemap-([a-z]+?)-(\\d+?)\\.xml$\";s:47:\"index.php?sitemap=$matches[1]&paged=$matches[2]\";s:11:\"our_team/?$\";s:28:\"index.php?post_type=our_team\";s:41:\"our_team/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=our_team&feed=$matches[1]\";s:36:\"our_team/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=our_team&feed=$matches[1]\";s:28:\"our_team/page/([0-9]{1,})/?$\";s:46:\"index.php?post_type=our_team&paged=$matches[1]\";s:16:\"publikacioink/?$\";s:32:\"index.php?post_type=publications\";s:46:\"publikacioink/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?post_type=publications&feed=$matches[1]\";s:41:\"publikacioink/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?post_type=publications&feed=$matches[1]\";s:33:\"publikacioink/page/([0-9]{1,})/?$\";s:50:\"index.php?post_type=publications&paged=$matches[1]\";s:21:\"hirek-es-esemenyek/?$\";s:26:\"index.php?post_type=events\";s:51:\"hirek-es-esemenyek/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?post_type=events&feed=$matches[1]\";s:46:\"hirek-es-esemenyek/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?post_type=events&feed=$matches[1]\";s:38:\"hirek-es-esemenyek/page/([0-9]{1,})/?$\";s:44:\"index.php?post_type=events&paged=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:34:\"our_team/.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:44:\"our_team/.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:64:\"our_team/.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"our_team/.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"our_team/.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:40:\"our_team/.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:23:\"our_team/(.+?)/embed/?$\";s:41:\"index.php?our_team=$matches[1]&embed=true\";s:27:\"our_team/(.+?)/trackback/?$\";s:35:\"index.php?our_team=$matches[1]&tb=1\";s:47:\"our_team/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?our_team=$matches[1]&feed=$matches[2]\";s:42:\"our_team/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?our_team=$matches[1]&feed=$matches[2]\";s:35:\"our_team/(.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?our_team=$matches[1]&paged=$matches[2]\";s:42:\"our_team/(.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?our_team=$matches[1]&cpage=$matches[2]\";s:31:\"our_team/(.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?our_team=$matches[1]&page=$matches[2]\";s:37:\"testimonial/.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:47:\"testimonial/.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:67:\"testimonial/.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"testimonial/.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"testimonial/.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:43:\"testimonial/.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:26:\"testimonial/(.+?)/embed/?$\";s:44:\"index.php?testimonial=$matches[1]&embed=true\";s:30:\"testimonial/(.+?)/trackback/?$\";s:38:\"index.php?testimonial=$matches[1]&tb=1\";s:38:\"testimonial/(.+?)/page/?([0-9]{1,})/?$\";s:51:\"index.php?testimonial=$matches[1]&paged=$matches[2]\";s:45:\"testimonial/(.+?)/comment-page-([0-9]{1,})/?$\";s:51:\"index.php?testimonial=$matches[1]&cpage=$matches[2]\";s:34:\"testimonial/(.+?)(?:/([0-9]+))?/?$\";s:50:\"index.php?testimonial=$matches[1]&page=$matches[2]\";s:33:\"partner/.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:43:\"partner/.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:63:\"partner/.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:58:\"partner/.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:58:\"partner/.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:39:\"partner/.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:22:\"partner/(.+?)/embed/?$\";s:40:\"index.php?partner=$matches[1]&embed=true\";s:26:\"partner/(.+?)/trackback/?$\";s:34:\"index.php?partner=$matches[1]&tb=1\";s:34:\"partner/(.+?)/page/?([0-9]{1,})/?$\";s:47:\"index.php?partner=$matches[1]&paged=$matches[2]\";s:41:\"partner/(.+?)/comment-page-([0-9]{1,})/?$\";s:47:\"index.php?partner=$matches[1]&cpage=$matches[2]\";s:30:\"partner/(.+?)(?:/([0-9]+))?/?$\";s:46:\"index.php?partner=$matches[1]&page=$matches[2]\";s:38:\"publications/.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:48:\"publications/.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:68:\"publications/.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:63:\"publications/.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:63:\"publications/.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:44:\"publications/.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:27:\"publications/(.+?)/embed/?$\";s:45:\"index.php?publications=$matches[1]&embed=true\";s:31:\"publications/(.+?)/trackback/?$\";s:39:\"index.php?publications=$matches[1]&tb=1\";s:51:\"publications/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:51:\"index.php?publications=$matches[1]&feed=$matches[2]\";s:46:\"publications/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:51:\"index.php?publications=$matches[1]&feed=$matches[2]\";s:39:\"publications/(.+?)/page/?([0-9]{1,})/?$\";s:52:\"index.php?publications=$matches[1]&paged=$matches[2]\";s:46:\"publications/(.+?)/comment-page-([0-9]{1,})/?$\";s:52:\"index.php?publications=$matches[1]&cpage=$matches[2]\";s:35:\"publications/(.+?)(?:/([0-9]+))?/?$\";s:51:\"index.php?publications=$matches[1]&page=$matches[2]\";s:38:\"int_contacts/.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:48:\"int_contacts/.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:68:\"int_contacts/.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:63:\"int_contacts/.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:63:\"int_contacts/.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:44:\"int_contacts/.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:27:\"int_contacts/(.+?)/embed/?$\";s:45:\"index.php?int_contacts=$matches[1]&embed=true\";s:31:\"int_contacts/(.+?)/trackback/?$\";s:39:\"index.php?int_contacts=$matches[1]&tb=1\";s:39:\"int_contacts/(.+?)/page/?([0-9]{1,})/?$\";s:52:\"index.php?int_contacts=$matches[1]&paged=$matches[2]\";s:46:\"int_contacts/(.+?)/comment-page-([0-9]{1,})/?$\";s:52:\"index.php?int_contacts=$matches[1]&cpage=$matches[2]\";s:35:\"int_contacts/(.+?)(?:/([0-9]+))?/?$\";s:51:\"index.php?int_contacts=$matches[1]&page=$matches[2]\";s:32:\"events/.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:42:\"events/.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:62:\"events/.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:57:\"events/.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:57:\"events/.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:38:\"events/.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:21:\"events/(.+?)/embed/?$\";s:39:\"index.php?events=$matches[1]&embed=true\";s:25:\"events/(.+?)/trackback/?$\";s:33:\"index.php?events=$matches[1]&tb=1\";s:45:\"events/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?events=$matches[1]&feed=$matches[2]\";s:40:\"events/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?events=$matches[1]&feed=$matches[2]\";s:33:\"events/(.+?)/page/?([0-9]{1,})/?$\";s:46:\"index.php?events=$matches[1]&paged=$matches[2]\";s:40:\"events/(.+?)/comment-page-([0-9]{1,})/?$\";s:46:\"index.php?events=$matches[1]&cpage=$matches[2]\";s:29:\"events/(.+?)(?:/([0-9]+))?/?$\";s:45:\"index.php?events=$matches[1]&page=$matches[2]\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:38:\"index.php?&page_id=2&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(2861, '_site_transient_timeout_browser_4243c5bad934d66f978f6ee6e569fdad', '1601298823', 'no'),
(2862, '_site_transient_browser_4243c5bad934d66f978f6ee6e569fdad', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"85.0.4183.102\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(2863, '_transient_timeout_wpseo_total_unindexed_posts', '1600780423', 'no'),
(2864, '_transient_wpseo_total_unindexed_posts', '0', 'no'),
(2865, '_transient_timeout_wpseo_total_unindexed_terms', '1600780423', 'no'),
(2866, '_transient_wpseo_total_unindexed_terms', '0', 'no'),
(2867, '_transient_timeout_wpseo_total_unindexed_post_type_archives', '1600780423', 'no'),
(2868, '_transient_wpseo_total_unindexed_post_type_archives', '0', 'no'),
(2869, '_transient_timeout_wpseo_unindexed_post_link_count', '1600780423', 'no'),
(2870, '_transient_wpseo_unindexed_post_link_count', '0', 'no'),
(2871, '_transient_timeout_wpseo_unindexed_term_link_count', '1600780423', 'no'),
(2872, '_transient_wpseo_unindexed_term_link_count', '0', 'no'),
(2876, '_transient_timeout_wpseo-statistics-totals', '1600780425', 'no'),
(2877, '_transient_wpseo-statistics-totals', 'a:1:{i:1;a:2:{s:6:\"scores\";a:0:{}s:8:\"division\";b:0;}}', 'no'),
(2879, '_site_transient_timeout_theme_roots', '1600774804', 'no'),
(2880, '_site_transient_theme_roots', 'a:4:{s:16:\"themeplate-child\";s:7:\"/themes\";s:10:\"themeplate\";s:7:\"/themes\";s:14:\"twentynineteen\";s:7:\"/themes\";s:12:\"twentytwenty\";s:7:\"/themes\";}', 'no'),
(2882, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1600773011;s:7:\"checked\";a:29:{s:30:\"advanced-custom-fields/acf.php\";s:5:\"5.9.1\";s:19:\"akismet/akismet.php\";s:5:\"4.1.6\";s:25:\"animate-it/edsanimate.php\";s:5:\"2.3.7\";s:27:\"wp-asset-clean-up/wpacu.php\";s:7:\"1.3.6.9\";s:43:\"asset-queue-manager/asset-queue-manager.php\";s:5:\"1.0.3\";s:21:\"backwpup/backwpup.php\";s:5:\"3.7.1\";s:33:\"classic-editor/classic-editor.php\";s:3:\"1.6\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:5:\"5.2.2\";s:42:\"contact-form-cfdb7/contact-form-cfdb-7.php\";s:8:\"1.2.4.11\";s:43:\"custom-post-type-ui/custom-post-type-ui.php\";s:5:\"1.8.0\";s:23:\"debug-bar/debug-bar.php\";s:5:\"1.0.1\";s:37:\"disable-comments/disable-comments.php\";s:6:\"1.11.0\";s:32:\"duplicate-page/duplicatepage.php\";s:3:\"4.3\";s:45:\"enable-media-replace/enable-media-replace.php\";s:5:\"3.4.2\";s:28:\"fast-velocity-minify/fvm.php\";s:5:\"2.8.9\";s:37:\"gdpr-cookie-compliance/moove-gdpr.php\";s:5:\"4.3.0\";s:21:\"hello-dolly/hello.php\";s:5:\"1.7.2\";s:37:\"rocket-lazy-load/rocket-lazy-load.php\";s:5:\"2.3.4\";s:23:\"loco-translate/loco.php\";s:5:\"2.4.3\";s:21:\"newsletter/plugin.php\";s:5:\"6.8.8\";s:31:\"query-monitor/query-monitor.php\";s:5:\"3.6.4\";s:47:\"show-current-template/show-current-template.php\";s:5:\"0.3.4\";s:35:\"simply-show-ids/simply-show-ids.php\";s:5:\"1.3.3\";s:23:\"wp-smushit/wp-smush.php\";s:5:\"3.7.0\";s:27:\"theme-check/theme-check.php\";s:10:\"20200731.1\";s:25:\"themeplate/themeplate.php\";s:6:\"3.14.2\";s:23:\"wp-cerber/wp-cerber.php\";s:5:\"8.6.7\";s:31:\"wp-migrate-db/wp-migrate-db.php\";s:6:\"1.0.15\";s:24:\"wordpress-seo/wp-seo.php\";s:4:\"14.9\";}s:8:\"response\";a:4:{s:27:\"wp-asset-clean-up/wpacu.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:31:\"w.org/plugins/wp-asset-clean-up\";s:4:\"slug\";s:17:\"wp-asset-clean-up\";s:6:\"plugin\";s:27:\"wp-asset-clean-up/wpacu.php\";s:11:\"new_version\";s:7:\"1.3.7.0\";s:3:\"url\";s:48:\"https://wordpress.org/plugins/wp-asset-clean-up/\";s:7:\"package\";s:68:\"https://downloads.wordpress.org/plugin/wp-asset-clean-up.1.3.7.0.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/wp-asset-clean-up/assets/icon-256x256.png?rev=1981952\";s:2:\"1x\";s:70:\"https://ps.w.org/wp-asset-clean-up/assets/icon-128x128.png?rev=1981952\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:72:\"https://ps.w.org/wp-asset-clean-up/assets/banner-772x250.png?rev=1986594\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.5.1\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:43:\"custom-post-type-ui/custom-post-type-ui.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:33:\"w.org/plugins/custom-post-type-ui\";s:4:\"slug\";s:19:\"custom-post-type-ui\";s:6:\"plugin\";s:43:\"custom-post-type-ui/custom-post-type-ui.php\";s:11:\"new_version\";s:5:\"1.8.1\";s:3:\"url\";s:50:\"https://wordpress.org/plugins/custom-post-type-ui/\";s:7:\"package\";s:68:\"https://downloads.wordpress.org/plugin/custom-post-type-ui.1.8.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:72:\"https://ps.w.org/custom-post-type-ui/assets/icon-256x256.png?rev=1069557\";s:2:\"1x\";s:72:\"https://ps.w.org/custom-post-type-ui/assets/icon-128x128.png?rev=1069557\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/custom-post-type-ui/assets/banner-1544x500.png?rev=1069557\";s:2:\"1x\";s:74:\"https://ps.w.org/custom-post-type-ui/assets/banner-772x250.png?rev=1069557\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.5.1\";s:12:\"requires_php\";s:3:\"5.6\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:37:\"gdpr-cookie-compliance/moove-gdpr.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:36:\"w.org/plugins/gdpr-cookie-compliance\";s:4:\"slug\";s:22:\"gdpr-cookie-compliance\";s:6:\"plugin\";s:37:\"gdpr-cookie-compliance/moove-gdpr.php\";s:11:\"new_version\";s:5:\"4.3.1\";s:3:\"url\";s:53:\"https://wordpress.org/plugins/gdpr-cookie-compliance/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/gdpr-cookie-compliance.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/gdpr-cookie-compliance/assets/icon-256x256.png?rev=2376316\";s:2:\"1x\";s:75:\"https://ps.w.org/gdpr-cookie-compliance/assets/icon-128x128.png?rev=2376316\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/gdpr-cookie-compliance/assets/banner-1544x500.png?rev=2376316\";s:2:\"1x\";s:77:\"https://ps.w.org/gdpr-cookie-compliance/assets/banner-772x250.png?rev=2376316\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.5.1\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:31:\"wp-migrate-db/wp-migrate-db.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:27:\"w.org/plugins/wp-migrate-db\";s:4:\"slug\";s:13:\"wp-migrate-db\";s:6:\"plugin\";s:31:\"wp-migrate-db/wp-migrate-db.php\";s:11:\"new_version\";s:6:\"1.0.16\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/wp-migrate-db/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/wp-migrate-db.1.0.16.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/wp-migrate-db/assets/icon-256x256.jpg?rev=1809889\";s:2:\"1x\";s:66:\"https://ps.w.org/wp-migrate-db/assets/icon-128x128.jpg?rev=1809889\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/wp-migrate-db/assets/banner-1544x500.jpg?rev=1809889\";s:2:\"1x\";s:68:\"https://ps.w.org/wp-migrate-db/assets/banner-772x250.jpg?rev=1809889\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.5.1\";s:12:\"requires_php\";s:3:\"5.4\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:24:{s:30:\"advanced-custom-fields/acf.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:36:\"w.org/plugins/advanced-custom-fields\";s:4:\"slug\";s:22:\"advanced-custom-fields\";s:6:\"plugin\";s:30:\"advanced-custom-fields/acf.php\";s:11:\"new_version\";s:5:\"5.9.1\";s:3:\"url\";s:53:\"https://wordpress.org/plugins/advanced-custom-fields/\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/plugin/advanced-custom-fields.5.9.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png?rev=1082746\";s:2:\"1x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-128x128.png?rev=1082746\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";s:2:\"1x\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";}s:11:\"banners_rtl\";a:0:{}}s:19:\"akismet/akismet.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.1.6\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}}s:25:\"animate-it/edsanimate.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:24:\"w.org/plugins/animate-it\";s:4:\"slug\";s:10:\"animate-it\";s:6:\"plugin\";s:25:\"animate-it/edsanimate.php\";s:11:\"new_version\";s:5:\"2.3.7\";s:3:\"url\";s:41:\"https://wordpress.org/plugins/animate-it/\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/plugin/animate-it.2.3.7.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:62:\"https://ps.w.org/animate-it/assets/icon-256x256.png?rev=989356\";s:2:\"1x\";s:62:\"https://ps.w.org/animate-it/assets/icon-128x128.png?rev=989356\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:65:\"https://ps.w.org/animate-it/assets/banner-1544x500.png?rev=988616\";s:2:\"1x\";s:64:\"https://ps.w.org/animate-it/assets/banner-772x250.png?rev=988616\";}s:11:\"banners_rtl\";a:0:{}}s:43:\"asset-queue-manager/asset-queue-manager.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:33:\"w.org/plugins/asset-queue-manager\";s:4:\"slug\";s:19:\"asset-queue-manager\";s:6:\"plugin\";s:43:\"asset-queue-manager/asset-queue-manager.php\";s:11:\"new_version\";s:5:\"1.0.3\";s:3:\"url\";s:50:\"https://wordpress.org/plugins/asset-queue-manager/\";s:7:\"package\";s:68:\"https://downloads.wordpress.org/plugin/asset-queue-manager.1.0.3.zip\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://s.w.org/plugins/geopattern-icon/asset-queue-manager.svg\";}s:7:\"banners\";a:0:{}s:11:\"banners_rtl\";a:0:{}}s:21:\"backwpup/backwpup.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:22:\"w.org/plugins/backwpup\";s:4:\"slug\";s:8:\"backwpup\";s:6:\"plugin\";s:21:\"backwpup/backwpup.php\";s:11:\"new_version\";s:5:\"3.7.1\";s:3:\"url\";s:39:\"https://wordpress.org/plugins/backwpup/\";s:7:\"package\";s:57:\"https://downloads.wordpress.org/plugin/backwpup.3.7.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:61:\"https://ps.w.org/backwpup/assets/icon-256x256.png?rev=1422084\";s:2:\"1x\";s:61:\"https://ps.w.org/backwpup/assets/icon-128x128.png?rev=1422084\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/backwpup/assets/banner-1544x500.png?rev=2340186\";s:2:\"1x\";s:63:\"https://ps.w.org/backwpup/assets/banner-772x250.png?rev=2340186\";}s:11:\"banners_rtl\";a:0:{}}s:33:\"classic-editor/classic-editor.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/classic-editor\";s:4:\"slug\";s:14:\"classic-editor\";s:6:\"plugin\";s:33:\"classic-editor/classic-editor.php\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/classic-editor/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/classic-editor.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/classic-editor/assets/icon-256x256.png?rev=1998671\";s:2:\"1x\";s:67:\"https://ps.w.org/classic-editor/assets/icon-128x128.png?rev=1998671\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/classic-editor/assets/banner-1544x500.png?rev=1998671\";s:2:\"1x\";s:69:\"https://ps.w.org/classic-editor/assets/banner-772x250.png?rev=1998676\";}s:11:\"banners_rtl\";a:0:{}}s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"5.2.2\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/contact-form-7.5.2.2.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:67:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=2279696\";s:2:\"1x\";s:59:\"https://ps.w.org/contact-form-7/assets/icon.svg?rev=2339255\";s:3:\"svg\";s:59:\"https://ps.w.org/contact-form-7/assets/icon.svg?rev=2339255\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}}s:42:\"contact-form-cfdb7/contact-form-cfdb-7.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:32:\"w.org/plugins/contact-form-cfdb7\";s:4:\"slug\";s:18:\"contact-form-cfdb7\";s:6:\"plugin\";s:42:\"contact-form-cfdb7/contact-form-cfdb-7.php\";s:11:\"new_version\";s:8:\"1.2.4.11\";s:3:\"url\";s:49:\"https://wordpress.org/plugins/contact-form-cfdb7/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/contact-form-cfdb7.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:71:\"https://ps.w.org/contact-form-cfdb7/assets/icon-256x256.png?rev=1619878\";s:2:\"1x\";s:71:\"https://ps.w.org/contact-form-cfdb7/assets/icon-128x128.png?rev=1619878\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:73:\"https://ps.w.org/contact-form-cfdb7/assets/banner-772x250.png?rev=1619902\";}s:11:\"banners_rtl\";a:0:{}}s:23:\"debug-bar/debug-bar.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:23:\"w.org/plugins/debug-bar\";s:4:\"slug\";s:9:\"debug-bar\";s:6:\"plugin\";s:23:\"debug-bar/debug-bar.php\";s:11:\"new_version\";s:5:\"1.0.1\";s:3:\"url\";s:40:\"https://wordpress.org/plugins/debug-bar/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/debug-bar.1.0.1.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:62:\"https://ps.w.org/debug-bar/assets/icon-256x256.png?rev=1908362\";s:2:\"1x\";s:54:\"https://ps.w.org/debug-bar/assets/icon.svg?rev=1908362\";s:3:\"svg\";s:54:\"https://ps.w.org/debug-bar/assets/icon.svg?rev=1908362\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:65:\"https://ps.w.org/debug-bar/assets/banner-1544x500.png?rev=1365496\";s:2:\"1x\";s:64:\"https://ps.w.org/debug-bar/assets/banner-772x250.png?rev=1365496\";}s:11:\"banners_rtl\";a:0:{}}s:37:\"disable-comments/disable-comments.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:30:\"w.org/plugins/disable-comments\";s:4:\"slug\";s:16:\"disable-comments\";s:6:\"plugin\";s:37:\"disable-comments/disable-comments.php\";s:11:\"new_version\";s:6:\"1.11.0\";s:3:\"url\";s:47:\"https://wordpress.org/plugins/disable-comments/\";s:7:\"package\";s:66:\"https://downloads.wordpress.org/plugin/disable-comments.1.11.0.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/disable-comments/assets/icon-256x256.png?rev=2365558\";s:2:\"1x\";s:69:\"https://ps.w.org/disable-comments/assets/icon-128x128.png?rev=2365562\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:72:\"https://ps.w.org/disable-comments/assets/banner-1544x500.png?rev=2365680\";s:2:\"1x\";s:71:\"https://ps.w.org/disable-comments/assets/banner-772x250.png?rev=2365680\";}s:11:\"banners_rtl\";a:0:{}}s:32:\"duplicate-page/duplicatepage.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/duplicate-page\";s:4:\"slug\";s:14:\"duplicate-page\";s:6:\"plugin\";s:32:\"duplicate-page/duplicatepage.php\";s:11:\"new_version\";s:3:\"4.3\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/duplicate-page/\";s:7:\"package\";s:57:\"https://downloads.wordpress.org/plugin/duplicate-page.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:67:\"https://ps.w.org/duplicate-page/assets/icon-128x128.jpg?rev=1412874\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/duplicate-page/assets/banner-772x250.jpg?rev=1410328\";}s:11:\"banners_rtl\";a:0:{}}s:45:\"enable-media-replace/enable-media-replace.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:34:\"w.org/plugins/enable-media-replace\";s:4:\"slug\";s:20:\"enable-media-replace\";s:6:\"plugin\";s:45:\"enable-media-replace/enable-media-replace.php\";s:11:\"new_version\";s:5:\"3.4.2\";s:3:\"url\";s:51:\"https://wordpress.org/plugins/enable-media-replace/\";s:7:\"package\";s:69:\"https://downloads.wordpress.org/plugin/enable-media-replace.3.4.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/enable-media-replace/assets/icon-256x256.png?rev=1940728\";s:2:\"1x\";s:73:\"https://ps.w.org/enable-media-replace/assets/icon-128x128.png?rev=1940728\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:76:\"https://ps.w.org/enable-media-replace/assets/banner-1544x500.png?rev=2322194\";s:2:\"1x\";s:75:\"https://ps.w.org/enable-media-replace/assets/banner-772x250.png?rev=2322194\";}s:11:\"banners_rtl\";a:0:{}}s:28:\"fast-velocity-minify/fvm.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:34:\"w.org/plugins/fast-velocity-minify\";s:4:\"slug\";s:20:\"fast-velocity-minify\";s:6:\"plugin\";s:28:\"fast-velocity-minify/fvm.php\";s:11:\"new_version\";s:5:\"2.8.9\";s:3:\"url\";s:51:\"https://wordpress.org/plugins/fast-velocity-minify/\";s:7:\"package\";s:69:\"https://downloads.wordpress.org/plugin/fast-velocity-minify.2.8.9.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:73:\"https://ps.w.org/fast-velocity-minify/assets/icon-128x128.jpg?rev=1440946\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:75:\"https://ps.w.org/fast-velocity-minify/assets/banner-772x250.jpg?rev=1440936\";}s:11:\"banners_rtl\";a:0:{}}s:21:\"hello-dolly/hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:21:\"hello-dolly/hello.php\";s:11:\"new_version\";s:5:\"1.7.2\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/hello-dolly.1.7.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=2052855\";s:2:\"1x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=2052855\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/hello-dolly/assets/banner-772x250.jpg?rev=2052855\";}s:11:\"banners_rtl\";a:0:{}}s:37:\"rocket-lazy-load/rocket-lazy-load.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:30:\"w.org/plugins/rocket-lazy-load\";s:4:\"slug\";s:16:\"rocket-lazy-load\";s:6:\"plugin\";s:37:\"rocket-lazy-load/rocket-lazy-load.php\";s:11:\"new_version\";s:5:\"2.3.4\";s:3:\"url\";s:47:\"https://wordpress.org/plugins/rocket-lazy-load/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/rocket-lazy-load.2.3.4.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/rocket-lazy-load/assets/icon-256x256.png?rev=1776193\";s:2:\"1x\";s:69:\"https://ps.w.org/rocket-lazy-load/assets/icon-128x128.png?rev=1776193\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:71:\"https://ps.w.org/rocket-lazy-load/assets/banner-772x250.png?rev=1776193\";}s:11:\"banners_rtl\";a:0:{}}s:23:\"loco-translate/loco.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/loco-translate\";s:4:\"slug\";s:14:\"loco-translate\";s:6:\"plugin\";s:23:\"loco-translate/loco.php\";s:11:\"new_version\";s:5:\"2.4.3\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/loco-translate/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/loco-translate.2.4.3.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/loco-translate/assets/icon-256x256.png?rev=1000676\";s:2:\"1x\";s:67:\"https://ps.w.org/loco-translate/assets/icon-128x128.png?rev=1000676\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:68:\"https://ps.w.org/loco-translate/assets/banner-772x250.jpg?rev=745046\";}s:11:\"banners_rtl\";a:0:{}}s:21:\"newsletter/plugin.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:24:\"w.org/plugins/newsletter\";s:4:\"slug\";s:10:\"newsletter\";s:6:\"plugin\";s:21:\"newsletter/plugin.php\";s:11:\"new_version\";s:5:\"6.8.8\";s:3:\"url\";s:41:\"https://wordpress.org/plugins/newsletter/\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/plugin/newsletter.6.8.8.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/newsletter/assets/icon-256x256.png?rev=1052028\";s:2:\"1x\";s:63:\"https://ps.w.org/newsletter/assets/icon-128x128.png?rev=1160467\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/newsletter/assets/banner-1544x500.png?rev=1052027\";s:2:\"1x\";s:65:\"https://ps.w.org/newsletter/assets/banner-772x250.png?rev=1052027\";}s:11:\"banners_rtl\";a:0:{}}s:31:\"query-monitor/query-monitor.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:27:\"w.org/plugins/query-monitor\";s:4:\"slug\";s:13:\"query-monitor\";s:6:\"plugin\";s:31:\"query-monitor/query-monitor.php\";s:11:\"new_version\";s:5:\"3.6.4\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/query-monitor/\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/plugin/query-monitor.3.6.4.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:66:\"https://ps.w.org/query-monitor/assets/icon-256x256.png?rev=2301273\";s:2:\"1x\";s:58:\"https://ps.w.org/query-monitor/assets/icon.svg?rev=2056073\";s:3:\"svg\";s:58:\"https://ps.w.org/query-monitor/assets/icon.svg?rev=2056073\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/query-monitor/assets/banner-1544x500.png?rev=1629576\";s:2:\"1x\";s:68:\"https://ps.w.org/query-monitor/assets/banner-772x250.png?rev=2301273\";}s:11:\"banners_rtl\";a:0:{}}s:47:\"show-current-template/show-current-template.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:35:\"w.org/plugins/show-current-template\";s:4:\"slug\";s:21:\"show-current-template\";s:6:\"plugin\";s:47:\"show-current-template/show-current-template.php\";s:11:\"new_version\";s:5:\"0.3.4\";s:3:\"url\";s:52:\"https://wordpress.org/plugins/show-current-template/\";s:7:\"package\";s:70:\"https://downloads.wordpress.org/plugin/show-current-template.0.3.4.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:73:\"https://ps.w.org/show-current-template/assets/icon-256x256.png?rev=976031\";s:2:\"1x\";s:65:\"https://ps.w.org/show-current-template/assets/icon.svg?rev=976031\";s:3:\"svg\";s:65:\"https://ps.w.org/show-current-template/assets/icon.svg?rev=976031\";}s:7:\"banners\";a:0:{}s:11:\"banners_rtl\";a:0:{}}s:35:\"simply-show-ids/simply-show-ids.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:29:\"w.org/plugins/simply-show-ids\";s:4:\"slug\";s:15:\"simply-show-ids\";s:6:\"plugin\";s:35:\"simply-show-ids/simply-show-ids.php\";s:11:\"new_version\";s:5:\"1.3.3\";s:3:\"url\";s:46:\"https://wordpress.org/plugins/simply-show-ids/\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/plugin/simply-show-ids.1.3.3.zip\";s:5:\"icons\";a:1:{s:7:\"default\";s:59:\"https://s.w.org/plugins/geopattern-icon/simply-show-ids.svg\";}s:7:\"banners\";a:0:{}s:11:\"banners_rtl\";a:0:{}}s:23:\"wp-smushit/wp-smush.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:24:\"w.org/plugins/wp-smushit\";s:4:\"slug\";s:10:\"wp-smushit\";s:6:\"plugin\";s:23:\"wp-smushit/wp-smush.php\";s:11:\"new_version\";s:5:\"3.7.0\";s:3:\"url\";s:41:\"https://wordpress.org/plugins/wp-smushit/\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/plugin/wp-smushit.3.7.0.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/wp-smushit/assets/icon-256x256.gif?rev=2263432\";s:2:\"1x\";s:63:\"https://ps.w.org/wp-smushit/assets/icon-128x128.gif?rev=2263431\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/wp-smushit/assets/banner-1544x500.png?rev=1863697\";s:2:\"1x\";s:65:\"https://ps.w.org/wp-smushit/assets/banner-772x250.png?rev=1863697\";}s:11:\"banners_rtl\";a:0:{}}s:27:\"theme-check/theme-check.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/theme-check\";s:4:\"slug\";s:11:\"theme-check\";s:6:\"plugin\";s:27:\"theme-check/theme-check.php\";s:11:\"new_version\";s:10:\"20200731.1\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/theme-check/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/theme-check.20200731.1.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:63:\"https://ps.w.org/theme-check/assets/icon-128x128.png?rev=972579\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/theme-check/assets/banner-1544x500.png?rev=904294\";s:2:\"1x\";s:65:\"https://ps.w.org/theme-check/assets/banner-772x250.png?rev=904294\";}s:11:\"banners_rtl\";a:0:{}}s:23:\"wp-cerber/wp-cerber.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:23:\"w.org/plugins/wp-cerber\";s:4:\"slug\";s:9:\"wp-cerber\";s:6:\"plugin\";s:23:\"wp-cerber/wp-cerber.php\";s:11:\"new_version\";s:5:\"8.6.7\";s:3:\"url\";s:40:\"https://wordpress.org/plugins/wp-cerber/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/wp-cerber.8.6.7.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:62:\"https://ps.w.org/wp-cerber/assets/icon-256x256.png?rev=2344645\";s:2:\"1x\";s:62:\"https://ps.w.org/wp-cerber/assets/icon-128x128.png?rev=2359065\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:65:\"https://ps.w.org/wp-cerber/assets/banner-1544x500.png?rev=2344628\";s:2:\"1x\";s:64:\"https://ps.w.org/wp-cerber/assets/banner-772x250.png?rev=2344628\";}s:11:\"banners_rtl\";a:0:{}}s:24:\"wordpress-seo/wp-seo.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:27:\"w.org/plugins/wordpress-seo\";s:4:\"slug\";s:13:\"wordpress-seo\";s:6:\"plugin\";s:24:\"wordpress-seo/wp-seo.php\";s:11:\"new_version\";s:4:\"14.9\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/wordpress-seo/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/wordpress-seo.14.9.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:66:\"https://ps.w.org/wordpress-seo/assets/icon-256x256.png?rev=2363699\";s:2:\"1x\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=2363699\";s:3:\"svg\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=2363699\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500.png?rev=1843435\";s:2:\"1x\";s:68:\"https://ps.w.org/wordpress-seo/assets/banner-772x250.png?rev=1843435\";}s:11:\"banners_rtl\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500-rtl.png?rev=1843435\";s:2:\"1x\";s:72:\"https://ps.w.org/wordpress-seo/assets/banner-772x250-rtl.png?rev=1843435\";}}}}', 'no');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(4, 5, '_form', '<div class=\"row contact-form\">\n <div class=\"col-lg-6 name\"> \n  [text* your-name placeholder \"Your Name\"]\n</div>\n<div class=\"col-lg-6 email\">\n  [email* your-email placeholder \"Your Email\"]\n</div>\n<div class=\"col-lg-12 message\">\n  [textarea your-message placeholder \"Your Message\"]\n</div>\n<div class=\"col-lg-12 submit\">\n  [submit \"Send\"]\n</div>\n</div>'),
(5, 5, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:13:\"Érdeklődés\";s:6:\"sender\";s:12:\"[your-email]\";s:9:\"recipient\";s:15:\"admin@email.com\";s:4:\"body\";s:23:\"Üzenet:\n[your-message]\";s:18:\"additional_headers\";s:0:\"\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:1;s:13:\"exclude_blank\";b:0;}'),
(6, 5, '_mail_2', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:18:\"Sikeres Beküldés\";s:6:\"sender\";s:15:\"admin@email.com\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:18:\"Sikeres Beküldés\";s:18:\"additional_headers\";s:0:\"\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(7, 5, '_messages', 'a:22:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";s:12:\"invalid_date\";s:35:\"Nem megfelelő a dátum formátuma.\";s:14:\"date_too_early\";s:63:\"A dátum korábbi, mint az engedélyezett legkorábbi időpont.\";s:13:\"date_too_late\";s:43:\"A dátum az engedélyezett időpont utáni.\";s:13:\"upload_failed\";s:57:\"Ismeretlen eredetű hiba történt a fájlfeltöltéskor.\";s:24:\"upload_file_type_invalid\";s:56:\"Nem engedélyezett az ilyen típusú fájl feltöltése.\";s:21:\"upload_file_too_large\";s:26:\"Túl nagyméretű a fájl.\";s:23:\"upload_failed_php_error\";s:42:\"Hiba történt a fájlfeltöltés közben.\";s:14:\"invalid_number\";s:33:\"A szám formátuma érvénytelen.\";s:16:\"number_too_small\";s:53:\"A szám kisebb az engedélyezett legkisebb számnál.\";s:16:\"number_too_large\";s:55:\"A szám nagyobb az engedélyezett legnagyobb számnál.\";s:23:\"quiz_answer_not_correct\";s:39:\"A quiz-kérdésre adott válasz hibás.\";s:13:\"invalid_email\";s:37:\"A megadott e-mail cím érvénytelen.\";s:11:\"invalid_url\";s:25:\"Az URL Cím érvénytelen\";s:11:\"invalid_tel\";s:29:\"A telefonszám érvénytelen.\";}'),
(8, 5, '_additional_settings', ''),
(9, 5, '_locale', 'hu_HU'),
(37, 2, '_edit_lock', '1585059287:1'),
(38, 2, '_edit_last', '1'),
(39, 2, 'themeplate_field_range', '50'),
(41, 12, '_edit_lock', '1583846100:1'),
(42, 11, '_edit_lock', '1583846115:1'),
(43, 1, '_edit_lock', '1583846127:1'),
(44, 13, '_wp_attached_file', '2020/03/placeholder-img-3.jpg'),
(45, 13, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:600;s:4:\"file\";s:29:\"2020/03/placeholder-img-3.jpg\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:29:\"placeholder-img-3-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"placeholder-img-3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:29:\"placeholder-img-3-768x576.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
(46, 13, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:22.951845933345414;s:5:\"bytes\";i:27864;s:11:\"size_before\";i:121402;s:10:\"size_after\";i:93538;s:4:\"time\";d:0.06;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:3:{s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";d:24.2;s:5:\"bytes\";i:3486;s:11:\"size_before\";i:14405;s:10:\"size_after\";i:10919;s:4:\"time\";d:0.01;}s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:19.53;s:5:\"bytes\";i:554;s:11:\"size_before\";i:2837;s:10:\"size_after\";i:2283;s:4:\"time\";d:0.02;}s:12:\"medium_large\";O:8:\"stdClass\":5:{s:7:\"percent\";d:22.87;s:5:\"bytes\";i:23824;s:11:\"size_before\";i:104160;s:10:\"size_after\";i:80336;s:4:\"time\";d:0.03;}}}'),
(47, 12, '_edit_last', '1'),
(48, 12, '_thumbnail_id', '13'),
(50, 14, 'themeplate_field_range', '50'),
(51, 12, '_yoast_wpseo_content_score', '90'),
(52, 12, 'themeplate_field_range', '50'),
(53, 11, '_edit_last', '1'),
(54, 11, '_thumbnail_id', '13'),
(56, 15, 'themeplate_field_range', '50'),
(57, 11, '_yoast_wpseo_content_score', '90'),
(58, 11, 'themeplate_field_range', '50'),
(59, 1, '_edit_last', '1'),
(60, 1, '_thumbnail_id', '13'),
(62, 16, 'themeplate_field_range', '50'),
(63, 1, '_yoast_wpseo_content_score', '90'),
(64, 1, 'themeplate_field_range', '50'),
(65, 17, '_edit_last', '1'),
(66, 19, 'themeplate_field_range', '50'),
(67, 17, 'themeplate_field_range', '50'),
(68, 17, '_edit_lock', '1584007452:1'),
(69, 20, '_edit_last', '1'),
(70, 20, '_edit_lock', '1584006650:1'),
(71, 22, '_wp_attached_file', '2020/03/unknown-person.png'),
(72, 22, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:450;s:6:\"height\";i:450;s:4:\"file\";s:26:\"2020/03/unknown-person.png\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"unknown-person-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"unknown-person-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
(73, 22, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:25.739626227431106;s:5:\"bytes\";i:8126;s:11:\"size_before\";i:31570;s:10:\"size_after\";i:23444;s:4:\"time\";d:0.25;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:2:{s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";d:24.19;s:5:\"bytes\";i:5495;s:11:\"size_before\";i:22715;s:10:\"size_after\";i:17220;s:4:\"time\";d:0.16;}s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:29.71;s:5:\"bytes\";i:2631;s:11:\"size_before\";i:8855;s:10:\"size_after\";i:6224;s:4:\"time\";d:0.09;}}}'),
(74, 20, '_thumbnail_id', '22'),
(75, 20, 'titulus', 'Manager'),
(76, 20, '_titulus', 'field_5e6a023c16dca'),
(77, 20, 'themeplate_field_range', '50'),
(78, 20, '_yoast_wpseo_content_score', '60'),
(79, 23, '_edit_last', '1'),
(80, 23, '_edit_lock', '1584006072:1'),
(81, 23, '_thumbnail_id', '22'),
(82, 23, 'titulus', 'Manager'),
(83, 23, '_titulus', 'field_5e6a023c16dca'),
(84, 23, 'themeplate_field_range', '50'),
(85, 23, '_yoast_wpseo_content_score', '60'),
(86, 24, '_edit_last', '1'),
(87, 24, '_edit_lock', '1584006076:1'),
(88, 24, '_thumbnail_id', '22'),
(89, 24, 'titulus', 'Manager'),
(90, 24, '_titulus', 'field_5e6a023c16dca'),
(91, 24, 'themeplate_field_range', '50'),
(92, 24, '_yoast_wpseo_content_score', '60'),
(93, 25, '_edit_last', '1'),
(94, 25, '_edit_lock', '1584006084:1'),
(95, 25, '_thumbnail_id', '22'),
(96, 25, 'titulus', 'Manager'),
(97, 25, '_titulus', 'field_5e6a023c16dca'),
(98, 25, 'themeplate_field_range', '50'),
(99, 25, '_yoast_wpseo_content_score', '60'),
(100, 26, 'themeplate_field_range', '50'),
(101, 27, '_edit_last', '1'),
(102, 29, 'themeplate_field_range', '50'),
(103, 27, 'themeplate_field_range', '50'),
(104, 27, '_edit_lock', '1584007428:1'),
(105, 30, '_edit_last', '1'),
(106, 30, '_edit_lock', '1584007647:1'),
(107, 30, 'testimonial_titulus', 'CEO'),
(108, 30, '_testimonial_titulus', 'field_5e6a0977edbdf'),
(109, 30, 'themeplate_field_range', '50'),
(110, 30, '_yoast_wpseo_content_score', '60'),
(111, 32, '_edit_last', '1'),
(112, 32, '_edit_lock', '1584007663:1'),
(113, 32, 'testimonial_titulus', 'CEO'),
(114, 32, '_testimonial_titulus', 'field_5e6a0977edbdf'),
(115, 32, 'themeplate_field_range', '50'),
(116, 32, '_yoast_wpseo_content_score', '60'),
(118, 33, '_edit_last', '1'),
(119, 33, '_edit_lock', '1584007668:1'),
(120, 33, 'testimonial_titulus', 'CEO'),
(121, 33, '_testimonial_titulus', 'field_5e6a0977edbdf'),
(122, 33, 'themeplate_field_range', '50'),
(123, 33, '_yoast_wpseo_content_score', '60'),
(125, 34, '_edit_last', '1'),
(126, 34, '_edit_lock', '1584007673:1'),
(127, 34, 'testimonial_titulus', 'CEO'),
(128, 34, '_testimonial_titulus', 'field_5e6a0977edbdf'),
(129, 34, 'themeplate_field_range', '50'),
(130, 34, '_yoast_wpseo_content_score', '60'),
(141, 37, '_wp_attached_file', '2020/03/partner-logo.png'),
(142, 37, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:624;s:4:\"file\";s:24:\"2020/03/partner-logo.png\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"partner-logo-300x195.png\";s:5:\"width\";i:300;s:6:\"height\";i:195;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"partner-logo-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:24:\"partner-logo-768x499.png\";s:5:\"width\";i:768;s:6:\"height\";i:499;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
(143, 37, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:20.910518778583636;s:5:\"bytes\";i:9053;s:11:\"size_before\";i:43294;s:10:\"size_after\";i:34241;s:4:\"time\";d:0.38;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:3:{s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";d:19.5;s:5:\"bytes\";i:1393;s:11:\"size_before\";i:7144;s:10:\"size_after\";i:5751;s:4:\"time\";d:0.07;}s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:16.11;s:5:\"bytes\";i:768;s:11:\"size_before\";i:4766;s:10:\"size_after\";i:3998;s:4:\"time\";d:0.09;}s:12:\"medium_large\";O:8:\"stdClass\":5:{s:7:\"percent\";d:21.96;s:5:\"bytes\";i:6892;s:11:\"size_before\";i:31384;s:10:\"size_after\";i:24492;s:4:\"time\";d:0.22;}}}'),
(144, 35, '_edit_last', '1'),
(145, 35, '_edit_lock', '1600171016:1'),
(147, 35, 'themeplate_field_range', '50'),
(148, 35, '_yoast_wpseo_content_score', '30'),
(149, 38, '_edit_last', '1'),
(150, 38, '_edit_lock', '1600169663:1'),
(151, 38, '_thumbnail_id', '101'),
(152, 38, 'themeplate_field_range', '50'),
(153, 38, '_yoast_wpseo_content_score', '30'),
(156, 39, '_edit_last', '1'),
(157, 39, '_edit_lock', '1600169667:1'),
(158, 39, '_thumbnail_id', '102'),
(159, 39, 'themeplate_field_range', '50'),
(160, 39, '_yoast_wpseo_content_score', '30'),
(163, 40, '_edit_last', '1'),
(164, 40, '_edit_lock', '1600169655:1'),
(165, 40, '_thumbnail_id', '103'),
(166, 40, 'themeplate_field_range', '50'),
(167, 40, '_yoast_wpseo_content_score', '30'),
(168, 42, '_edit_last', '1'),
(169, 42, '_wp_page_template', 'archive-blog.php'),
(170, 44, 'themeplate_field_range', '50'),
(171, 42, 'themeplate_field_range', '50'),
(172, 42, '_edit_lock', '1600412857:1'),
(196, 51, '_edit_lock', '1585227863:1'),
(197, 51, '_edit_last', '1'),
(198, 51, '_thumbnail_id', '13'),
(199, 51, '_yoast_wpseo_content_score', '90'),
(200, 51, 'themeplate_field_range', '50'),
(203, 52, '_edit_lock', '1585227868:1'),
(204, 52, '_edit_last', '1'),
(205, 52, '_thumbnail_id', '13'),
(206, 52, '_yoast_wpseo_content_score', '90'),
(207, 52, 'themeplate_field_range', '50'),
(210, 53, '_edit_lock', '1585227881:1'),
(211, 53, '_edit_last', '1'),
(212, 53, '_thumbnail_id', '13'),
(213, 53, '_yoast_wpseo_content_score', '90'),
(214, 53, 'themeplate_field_range', '50'),
(232, 64, '_edit_last', '1'),
(233, 64, '_wp_page_template', 'archive-our_team.php'),
(234, 66, 'themeplate_field_range', '50'),
(235, 64, 'themeplate_field_range', '50'),
(236, 64, '_edit_lock', '1585560970:1'),
(249, 71, '_wp_attached_file', '2020/03/dx-engine-fav.png'),
(250, 71, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:178;s:6:\"height\";i:178;s:4:\"file\";s:25:\"2020/03/dx-engine-fav.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"dx-engine-fav-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
(251, 71, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";i:0;s:5:\"bytes\";i:0;s:11:\"size_before\";i:21129;s:10:\"size_after\";i:21129;s:4:\"time\";d:0.07;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:1:{s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";i:0;s:5:\"bytes\";i:0;s:11:\"size_before\";i:21129;s:10:\"size_after\";i:21129;s:4:\"time\";d:0.07;}}}'),
(252, 72, '_wp_attached_file', '2020/03/cropped-dx-engine-fav.png'),
(253, 72, '_wp_attachment_context', 'site-icon'),
(254, 72, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:512;s:6:\"height\";i:512;s:4:\"file\";s:33:\"2020/03/cropped-dx-engine-fav.png\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"cropped-dx-engine-fav-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"cropped-dx-engine-fav-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-270\";a:4:{s:4:\"file\";s:33:\"cropped-dx-engine-fav-270x270.png\";s:5:\"width\";i:270;s:6:\"height\";i:270;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-192\";a:4:{s:4:\"file\";s:33:\"cropped-dx-engine-fav-192x192.png\";s:5:\"width\";i:192;s:6:\"height\";i:192;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-180\";a:4:{s:4:\"file\";s:33:\"cropped-dx-engine-fav-180x180.png\";s:5:\"width\";i:180;s:6:\"height\";i:180;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"site_icon-32\";a:4:{s:4:\"file\";s:31:\"cropped-dx-engine-fav-32x32.png\";s:5:\"width\";i:32;s:6:\"height\";i:32;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
(255, 72, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:0.060350333687053344;s:5:\"bytes\";i:120;s:11:\"size_before\";i:198839;s:10:\"size_after\";i:198719;s:4:\"time\";d:0.44;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:6:{s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";i:0;s:5:\"bytes\";i:0;s:11:\"size_before\";i:62929;s:10:\"size_after\";i:62929;s:4:\"time\";d:0.12;}s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";i:0;s:5:\"bytes\";i:0;s:11:\"size_before\";i:21778;s:10:\"size_after\";i:21778;s:4:\"time\";d:0.13;}s:13:\"site_icon-270\";O:8:\"stdClass\":5:{s:7:\"percent\";i:0;s:5:\"bytes\";i:0;s:11:\"size_before\";i:53126;s:10:\"size_after\";i:53126;s:4:\"time\";d:0.08;}s:13:\"site_icon-192\";O:8:\"stdClass\":5:{s:7:\"percent\";i:0;s:5:\"bytes\";i:0;s:11:\"size_before\";i:30083;s:10:\"size_after\";i:30083;s:4:\"time\";d:0.04;}s:13:\"site_icon-180\";O:8:\"stdClass\":5:{s:7:\"percent\";i:0;s:5:\"bytes\";i:0;s:11:\"size_before\";i:28956;s:10:\"size_after\";i:28956;s:4:\"time\";d:0.05;}s:12:\"site_icon-32\";O:8:\"stdClass\":5:{s:7:\"percent\";d:6.1;s:5:\"bytes\";i:120;s:11:\"size_before\";i:1967;s:10:\"size_after\";i:1847;s:4:\"time\";d:0.02;}}}'),
(256, 73, '_wp_attached_file', '2020/03/dx-engine-fav-1.png'),
(257, 73, '_wp_attachment_metadata', 'a:4:{s:5:\"width\";i:142;s:6:\"height\";i:128;s:4:\"file\";s:27:\"2020/03/dx-engine-fav-1.png\";s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
(258, 73, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";i:0;s:5:\"bytes\";i:0;s:11:\"size_before\";i:17500;s:10:\"size_after\";i:17500;s:4:\"time\";d:0.05;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:1:{s:4:\"full\";O:8:\"stdClass\":5:{s:7:\"percent\";i:0;s:5:\"bytes\";i:0;s:11:\"size_before\";i:17500;s:10:\"size_after\";i:17500;s:4:\"time\";d:0.05;}}}'),
(259, 74, '_wp_attached_file', '2020/03/cropped-dx-engine-fav-1.png'),
(260, 74, '_wp_attachment_context', 'site-icon'),
(261, 74, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:512;s:6:\"height\";i:512;s:4:\"file\";s:35:\"2020/03/cropped-dx-engine-fav-1.png\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:35:\"cropped-dx-engine-fav-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:35:\"cropped-dx-engine-fav-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-270\";a:4:{s:4:\"file\";s:35:\"cropped-dx-engine-fav-1-270x270.png\";s:5:\"width\";i:270;s:6:\"height\";i:270;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-192\";a:4:{s:4:\"file\";s:35:\"cropped-dx-engine-fav-1-192x192.png\";s:5:\"width\";i:192;s:6:\"height\";i:192;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-180\";a:4:{s:4:\"file\";s:35:\"cropped-dx-engine-fav-1-180x180.png\";s:5:\"width\";i:180;s:6:\"height\";i:180;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"site_icon-32\";a:4:{s:4:\"file\";s:33:\"cropped-dx-engine-fav-1-32x32.png\";s:5:\"width\";i:32;s:6:\"height\";i:32;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
(262, 75, '_edit_lock', '1585653787:1'),
(266, 78, '_wp_attached_file', '2020/09/mokk-adatkutatoalintezet-logo-small.png'),
(267, 78, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:512;s:6:\"height\";i:512;s:4:\"file\";s:47:\"2020/09/mokk-adatkutatoalintezet-logo-small.png\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:47:\"mokk-adatkutatoalintezet-logo-small-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:47:\"mokk-adatkutatoalintezet-logo-small-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
(268, 79, '_wp_attached_file', '2020/09/cropped-mokk-adatkutatoalintezet-logo-small.png'),
(269, 79, '_wp_attachment_context', 'site-icon'),
(270, 79, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:512;s:6:\"height\";i:512;s:4:\"file\";s:55:\"2020/09/cropped-mokk-adatkutatoalintezet-logo-small.png\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:55:\"cropped-mokk-adatkutatoalintezet-logo-small-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:55:\"cropped-mokk-adatkutatoalintezet-logo-small-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-270\";a:4:{s:4:\"file\";s:55:\"cropped-mokk-adatkutatoalintezet-logo-small-270x270.png\";s:5:\"width\";i:270;s:6:\"height\";i:270;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-192\";a:4:{s:4:\"file\";s:55:\"cropped-mokk-adatkutatoalintezet-logo-small-192x192.png\";s:5:\"width\";i:192;s:6:\"height\";i:192;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-180\";a:4:{s:4:\"file\";s:55:\"cropped-mokk-adatkutatoalintezet-logo-small-180x180.png\";s:5:\"width\";i:180;s:6:\"height\";i:180;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"site_icon-32\";a:4:{s:4:\"file\";s:53:\"cropped-mokk-adatkutatoalintezet-logo-small-32x32.png\";s:5:\"width\";i:32;s:6:\"height\";i:32;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
(271, 78, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:31.913666045999317;s:5:\"bytes\";i:36507;s:11:\"size_before\";i:114393;s:10:\"size_after\";i:77886;s:4:\"time\";d:0.31;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:2:{s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";d:31.63;s:5:\"bytes\";i:27287;s:11:\"size_before\";i:86282;s:10:\"size_after\";i:58995;s:4:\"time\";d:0.24;}s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:32.8;s:5:\"bytes\";i:9220;s:11:\"size_before\";i:28111;s:10:\"size_after\";i:18891;s:4:\"time\";d:0.07;}}}'),
(272, 79, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:32.387186490421364;s:5:\"bytes\";i:84310;s:11:\"size_before\";i:260319;s:10:\"size_after\";i:176009;s:4:\"time\";d:1.0699999999999998;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:6:{s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";d:31.63;s:5:\"bytes\";i:27287;s:11:\"size_before\";i:86282;s:10:\"size_after\";i:58995;s:4:\"time\";d:0.31;}s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:32.8;s:5:\"bytes\";i:9220;s:11:\"size_before\";i:28111;s:10:\"size_after\";i:18891;s:4:\"time\";d:0.04;}s:13:\"site_icon-270\";O:8:\"stdClass\":5:{s:7:\"percent\";d:32.77;s:5:\"bytes\";i:23307;s:11:\"size_before\";i:71129;s:10:\"size_after\";i:47822;s:4:\"time\";d:0.33;}s:13:\"site_icon-192\";O:8:\"stdClass\":5:{s:7:\"percent\";d:31.88;s:5:\"bytes\";i:11080;s:11:\"size_before\";i:34759;s:10:\"size_after\";i:23679;s:4:\"time\";d:0.19;}s:13:\"site_icon-180\";O:8:\"stdClass\":5:{s:7:\"percent\";d:33.77;s:5:\"bytes\";i:12556;s:11:\"size_before\";i:37183;s:10:\"size_after\";i:24627;s:4:\"time\";d:0.19;}s:12:\"site_icon-32\";O:8:\"stdClass\":5:{s:7:\"percent\";d:30.12;s:5:\"bytes\";i:860;s:11:\"size_before\";i:2855;s:10:\"size_after\";i:1995;s:4:\"time\";d:0.01;}}}'),
(273, 82, '_edit_last', '1'),
(274, 82, '_edit_lock', '1600151033:1'),
(275, 85, '_edit_last', '1'),
(276, 85, '_edit_lock', '1600095461:1'),
(277, 85, 'publikacio_szerzoje', 'Dr. Parti Tamás'),
(278, 85, '_publikacio_szerzoje', 'field_5f5f3a01d7486'),
(279, 85, 'publikalas_datuma', '2019. (66. évf.) 1. sz. 5-22. old.'),
(280, 85, '_publikalas_datuma', 'field_5f5f3a17d7487'),
(281, 85, '_yoast_wpseo_content_score', '60'),
(282, 85, 'publikacio_megjelenesi_helye', 'Közjegyzők Könyve'),
(283, 85, '_publikacio_megjelenesi_helye', 'field_5f5f3b73b9938'),
(284, 87, '_wp_attached_file', '2020/09/publikacio-cikk-1.jpg'),
(285, 87, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1069;s:6:\"height\";i:501;s:4:\"file\";s:29:\"2020/09/publikacio-cikk-1.jpg\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:29:\"publikacio-cikk-1-300x141.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:141;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:30:\"publikacio-cikk-1-1024x480.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:480;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"publikacio-cikk-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:29:\"publikacio-cikk-1-768x360.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:360;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
(286, 85, '_thumbnail_id', '87'),
(287, 87, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:12.683741648106906;s:5:\"bytes\";i:101371;s:11:\"size_before\";i:799220;s:10:\"size_after\";i:697849;s:4:\"time\";d:0.38;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:4:{s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";d:13.41;s:5:\"bytes\";i:6474;s:11:\"size_before\";i:48294;s:10:\"size_after\";i:41820;s:4:\"time\";d:0.03;}s:5:\"large\";O:8:\"stdClass\":5:{s:7:\"percent\";d:12.33;s:5:\"bytes\";i:55444;s:11:\"size_before\";i:449798;s:10:\"size_after\";i:394354;s:4:\"time\";d:0.23;}s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:13.22;s:5:\"bytes\";i:3498;s:11:\"size_before\";i:26466;s:10:\"size_after\";i:22968;s:4:\"time\";d:0.01;}s:12:\"medium_large\";O:8:\"stdClass\":5:{s:7:\"percent\";d:13.09;s:5:\"bytes\";i:35955;s:11:\"size_before\";i:274662;s:10:\"size_after\";i:238707;s:4:\"time\";d:0.11;}}}'),
(288, 88, '_edit_last', '1'),
(289, 88, '_edit_lock', '1600095857:1'),
(290, 88, 'publikacio_szerzoje', 'Dr. Parti Tamás'),
(291, 88, '_publikacio_szerzoje', 'field_5f5f3a01d7486'),
(292, 88, 'publikalas_datuma', '2019. (27. évf.) 9. sz. 1-7. old.'),
(293, 88, '_publikalas_datuma', 'field_5f5f3a17d7487'),
(294, 88, '_yoast_wpseo_content_score', '60'),
(295, 88, 'publikacio_megjelenesi_helye', 'Gazdaság és jog'),
(296, 88, '_publikacio_megjelenesi_helye', 'field_5f5f3b73b9938'),
(297, 88, '_thumbnail_id', '89'),
(303, 89, '_wp_attached_file', '2020/09/publikacio-cikk-2.jpg'),
(304, 89, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1304;s:4:\"file\";s:29:\"2020/09/publikacio-cikk-2.jpg\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:29:\"publikacio-cikk-2-300x204.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:204;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:30:\"publikacio-cikk-2-1024x695.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:695;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"publikacio-cikk-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:29:\"publikacio-cikk-2-768x522.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:522;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:31:\"publikacio-cikk-2-1536x1043.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1043;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:3:\"max\";a:4:{s:4:\"file\";s:31:\"publikacio-cikk-2-1590x1080.jpg\";s:5:\"width\";i:1590;s:6:\"height\";i:1080;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
(305, 89, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:7.915822755838492;s:5:\"bytes\";i:238641;s:11:\"size_before\";i:3014734;s:10:\"size_after\";i:2776093;s:4:\"time\";d:1.09;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:6:{s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";d:12.73;s:5:\"bytes\";i:8255;s:11:\"size_before\";i:64863;s:10:\"size_after\";i:56608;s:4:\"time\";d:0.02;}s:5:\"large\";O:8:\"stdClass\":5:{s:7:\"percent\";d:7.65;s:5:\"bytes\";i:38878;s:11:\"size_before\";i:508476;s:10:\"size_after\";i:469598;s:4:\"time\";d:0.32;}s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:13.63;s:5:\"bytes\";i:3673;s:11:\"size_before\";i:26954;s:10:\"size_after\";i:23281;s:4:\"time\";d:0.09;}s:12:\"medium_large\";O:8:\"stdClass\":5:{s:7:\"percent\";d:8.97;s:5:\"bytes\";i:27893;s:11:\"size_before\";i:311116;s:10:\"size_after\";i:283223;s:4:\"time\";d:0.16;}s:9:\"1536x1536\";O:8:\"stdClass\":5:{s:7:\"percent\";d:7.65;s:5:\"bytes\";i:77995;s:11:\"size_before\";i:1019375;s:10:\"size_after\";i:941380;s:4:\"time\";d:0.29;}s:3:\"max\";O:8:\"stdClass\":5:{s:7:\"percent\";d:7.56;s:5:\"bytes\";i:81947;s:11:\"size_before\";i:1083950;s:10:\"size_after\";i:1002003;s:4:\"time\";d:0.21;}}}'),
(306, 90, '_edit_last', '1'),
(307, 90, '_edit_lock', '1600177095:1'),
(308, 90, 'publikacio_szerzoje', 'Dr. Parti Tamás'),
(309, 90, '_publikacio_szerzoje', 'field_5f5f3a01d7486'),
(310, 90, 'publikalas_datuma', '2016. (8. évf.) 3. sz. 2-5. old.'),
(311, 90, '_publikalas_datuma', 'field_5f5f3a17d7487'),
(312, 90, '_yoast_wpseo_content_score', '60'),
(313, 90, 'publikacio_megjelenesi_helye', 'Notarius Hungaricus'),
(314, 90, '_publikacio_megjelenesi_helye', 'field_5f5f3b73b9938'),
(315, 90, '_thumbnail_id', '91'),
(321, 91, '_wp_attached_file', '2020/09/publikacio-cikk-3.jpg'),
(322, 91, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1756;s:6:\"height\";i:769;s:4:\"file\";s:29:\"2020/09/publikacio-cikk-3.jpg\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:29:\"publikacio-cikk-3-300x131.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:131;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:30:\"publikacio-cikk-3-1024x448.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:448;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"publikacio-cikk-3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:29:\"publikacio-cikk-3-768x336.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:336;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:30:\"publikacio-cikk-3-1536x673.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:673;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:24:\"©Alex - stock.adobe.com\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
(323, 91, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:10.843151091441843;s:5:\"bytes\";i:139215;s:11:\"size_before\";i:1283898;s:10:\"size_after\";i:1144683;s:4:\"time\";d:0.76;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:5:{s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";d:9.37;s:5:\"bytes\";i:3162;s:11:\"size_before\";i:33755;s:10:\"size_after\";i:30593;s:4:\"time\";d:0.02;}s:5:\"large\";O:8:\"stdClass\":5:{s:7:\"percent\";d:10.6;s:5:\"bytes\";i:35529;s:11:\"size_before\";i:335032;s:10:\"size_after\";i:299503;s:4:\"time\";d:0.11;}s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:8.99;s:5:\"bytes\";i:1794;s:11:\"size_before\";i:19949;s:10:\"size_after\";i:18155;s:4:\"time\";d:0.02;}s:12:\"medium_large\";O:8:\"stdClass\":5:{s:7:\"percent\";d:10.89;s:5:\"bytes\";i:21885;s:11:\"size_before\";i:201034;s:10:\"size_after\";i:179149;s:4:\"time\";d:0.09;}s:9:\"1536x1536\";O:8:\"stdClass\":5:{s:7:\"percent\";d:11.07;s:5:\"bytes\";i:76845;s:11:\"size_before\";i:694128;s:10:\"size_after\";i:617283;s:4:\"time\";d:0.52;}}}'),
(324, 92, '_edit_last', '1'),
(325, 92, '_edit_lock', '1600151553:1'),
(326, 92, '_yoast_wpseo_content_score', '60'),
(327, 93, '_edit_last', '1'),
(328, 93, '_edit_lock', '1600151760:1'),
(329, 93, '_yoast_wpseo_content_score', '60'),
(330, 94, '_edit_last', '1'),
(331, 94, '_edit_lock', '1600151756:1'),
(332, 94, '_yoast_wpseo_content_score', '60'),
(333, 95, '_edit_last', '1'),
(334, 95, '_edit_lock', '1600151790:1'),
(335, 95, '_yoast_wpseo_content_score', '60'),
(336, 53, '_wp_trash_meta_status', 'publish'),
(337, 53, '_wp_trash_meta_time', '1600160595'),
(338, 53, '_wp_desired_post_slug', 'blog-post-6'),
(339, 52, '_wp_trash_meta_status', 'publish'),
(340, 52, '_wp_trash_meta_time', '1600160595'),
(341, 52, '_wp_desired_post_slug', 'blog-post-5'),
(342, 51, '_wp_trash_meta_status', 'publish'),
(343, 51, '_wp_trash_meta_time', '1600160595'),
(344, 51, '_wp_desired_post_slug', 'blog-post-4'),
(345, 11, '_wp_trash_meta_status', 'publish'),
(346, 11, '_wp_trash_meta_time', '1600160595'),
(347, 11, '_wp_desired_post_slug', 'blog-post-3'),
(348, 12, '_wp_trash_meta_status', 'publish'),
(349, 12, '_wp_trash_meta_time', '1600160595'),
(350, 12, '_wp_desired_post_slug', 'blog-post-2'),
(351, 1, '_wp_trash_meta_status', 'publish'),
(352, 1, '_wp_trash_meta_time', '1600160595'),
(353, 1, '_wp_desired_post_slug', 'hello-vilag'),
(354, 1, '_wp_trash_meta_comments_status', 'a:1:{i:1;s:1:\"1\";}'),
(355, 96, '_edit_last', '1'),
(356, 96, '_edit_lock', '1600161109:1'),
(357, 97, '_wp_attached_file', '2020/09/esemeny-1.jpg'),
(358, 97, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1080;s:4:\"file\";s:21:\"2020/09/esemeny-1.jpg\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"esemeny-1-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:22:\"esemeny-1-1024x576.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"esemeny-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:21:\"esemeny-1-768x432.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:22:\"esemeny-1-1536x864.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:864;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
(359, 96, '_thumbnail_id', '97'),
(361, 96, '_yoast_wpseo_content_score', '60'),
(362, 96, '_yoast_wpseo_primary_category', '3'),
(363, 97, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:11.390320442294545;s:5:\"bytes\";i:263585;s:11:\"size_before\";i:2314114;s:10:\"size_after\";i:2050529;s:4:\"time\";d:0.47;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:5:{s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";d:13.24;s:5:\"bytes\";i:8152;s:11:\"size_before\";i:61561;s:10:\"size_after\";i:53409;s:4:\"time\";d:0.02;}s:5:\"large\";O:8:\"stdClass\":5:{s:7:\"percent\";d:11.86;s:5:\"bytes\";i:73047;s:11:\"size_before\";i:616148;s:10:\"size_after\";i:543101;s:4:\"time\";d:0.13;}s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:13.09;s:5:\"bytes\";i:3702;s:11:\"size_before\";i:28284;s:10:\"size_after\";i:24582;s:4:\"time\";d:0.01;}s:12:\"medium_large\";O:8:\"stdClass\":5:{s:7:\"percent\";d:12.67;s:5:\"bytes\";i:46560;s:11:\"size_before\";i:367425;s:10:\"size_after\";i:320865;s:4:\"time\";d:0.09;}s:9:\"1536x1536\";O:8:\"stdClass\":5:{s:7:\"percent\";d:10.65;s:5:\"bytes\";i:132124;s:11:\"size_before\";i:1240696;s:10:\"size_after\";i:1108572;s:4:\"time\";d:0.22;}}}'),
(364, 96, '_wp_trash_meta_status', 'publish'),
(365, 96, '_wp_trash_meta_time', '1600161255'),
(366, 96, '_wp_desired_post_slug', 'a-futurologiai-forum-2020-szeptember-14-en-online-kerul-megrendezesre'),
(367, 99, '_edit_last', '1'),
(368, 99, '_edit_lock', '1600161503:1'),
(369, 99, '_thumbnail_id', '97'),
(370, 99, '_yoast_wpseo_primary_category', '5'),
(371, 99, '_yoast_wpseo_content_score', '60'),
(375, 101, '_wp_attached_file', '2020/03/mokk_logo.png'),
(376, 101, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:191;s:6:\"height\";i:192;s:4:\"file\";s:21:\"2020/03/mokk_logo.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"mokk_logo-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
(377, 101, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:25.11981708657609;s:5:\"bytes\";i:5713;s:11:\"size_before\";i:22743;s:10:\"size_after\";i:17030;s:4:\"time\";d:0.71;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:1:{s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:25.12;s:5:\"bytes\";i:5713;s:11:\"size_before\";i:22743;s:10:\"size_after\";i:17030;s:4:\"time\";d:0.71;}}}'),
(378, 102, '_wp_attached_file', '2020/03/muegyetem.png'),
(379, 102, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:368;s:6:\"height\";i:98;s:4:\"file\";s:21:\"2020/03/muegyetem.png\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"muegyetem-300x80.png\";s:5:\"width\";i:300;s:6:\"height\";i:80;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"muegyetem-150x98.png\";s:5:\"width\";i:150;s:6:\"height\";i:98;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
(380, 102, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:6.102187710527248;s:5:\"bytes\";i:1721;s:11:\"size_before\";i:28203;s:10:\"size_after\";i:26482;s:4:\"time\";d:0.09;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:2:{s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";d:7.62;s:5:\"bytes\";i:1332;s:11:\"size_before\";i:17469;s:10:\"size_after\";i:16137;s:4:\"time\";d:0.06;}s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:3.62;s:5:\"bytes\";i:389;s:11:\"size_before\";i:10734;s:10:\"size_after\";i:10345;s:4:\"time\";d:0.03;}}}'),
(381, 103, '_wp_attached_file', '2020/03/kurt-akademia.png'),
(382, 103, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:416;s:6:\"height\";i:133;s:4:\"file\";s:25:\"2020/03/kurt-akademia.png\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"kurt-akademia-300x96.png\";s:5:\"width\";i:300;s:6:\"height\";i:96;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"kurt-akademia-150x133.png\";s:5:\"width\";i:150;s:6:\"height\";i:133;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
(383, 103, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:25.7635146170178;s:5:\"bytes\";i:6310;s:11:\"size_before\";i:24492;s:10:\"size_after\";i:18182;s:4:\"time\";d:0.16999999999999998;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:2:{s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";d:22.14;s:5:\"bytes\";i:4059;s:11:\"size_before\";i:18333;s:10:\"size_after\";i:14274;s:4:\"time\";d:0.06;}s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:36.55;s:5:\"bytes\";i:2251;s:11:\"size_before\";i:6159;s:10:\"size_after\";i:3908;s:4:\"time\";d:0.11;}}}'),
(384, 104, '_wp_attached_file', '2020/03/KRE_logo_A4_magyar.png'),
(385, 104, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:414;s:6:\"height\";i:181;s:4:\"file\";s:30:\"2020/03/KRE_logo_A4_magyar.png\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"KRE_logo_A4_magyar-300x131.png\";s:5:\"width\";i:300;s:6:\"height\";i:131;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"KRE_logo_A4_magyar-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
(386, 104, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:29.445799263615935;s:5:\"bytes\";i:12316;s:11:\"size_before\";i:41826;s:10:\"size_after\";i:29510;s:4:\"time\";d:0.39999999999999997;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:2:{s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";d:29.1;s:5:\"bytes\";i:7882;s:11:\"size_before\";i:27087;s:10:\"size_after\";i:19205;s:4:\"time\";d:0.11;}s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:30.08;s:5:\"bytes\";i:4434;s:11:\"size_before\";i:14739;s:10:\"size_after\";i:10305;s:4:\"time\";d:0.29;}}}'),
(387, 35, '_thumbnail_id', '104'),
(388, 105, '_menu_item_type', 'custom'),
(389, 105, '_menu_item_menu_item_parent', '0'),
(390, 105, '_menu_item_object_id', '105'),
(391, 105, '_menu_item_object', 'custom'),
(392, 105, '_menu_item_target', ''),
(393, 105, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(394, 105, '_menu_item_xfn', ''),
(395, 105, '_menu_item_url', '#'),
(397, 106, '_menu_item_type', 'custom'),
(398, 106, '_menu_item_menu_item_parent', '0'),
(399, 106, '_menu_item_object_id', '106'),
(400, 106, '_menu_item_object', 'custom'),
(401, 106, '_menu_item_target', ''),
(402, 106, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(403, 106, '_menu_item_xfn', ''),
(404, 106, '_menu_item_url', '#'),
(406, 107, '_menu_item_type', 'custom'),
(407, 107, '_menu_item_menu_item_parent', '0'),
(408, 107, '_menu_item_object_id', '107'),
(409, 107, '_menu_item_object', 'custom'),
(410, 107, '_menu_item_target', ''),
(411, 107, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(412, 107, '_menu_item_xfn', ''),
(413, 107, '_menu_item_url', '#'),
(424, 109, '_menu_item_type', 'custom'),
(425, 109, '_menu_item_menu_item_parent', '0'),
(426, 109, '_menu_item_object_id', '109'),
(427, 109, '_menu_item_object', 'custom'),
(428, 109, '_menu_item_target', ''),
(429, 109, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(430, 109, '_menu_item_xfn', ''),
(431, 109, '_menu_item_url', '#international-contacts'),
(433, 110, '_menu_item_type', 'post_type'),
(434, 110, '_menu_item_menu_item_parent', '0'),
(435, 110, '_menu_item_object_id', '2'),
(436, 110, '_menu_item_object', 'page'),
(437, 110, '_menu_item_target', ''),
(438, 110, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(439, 110, '_menu_item_xfn', ''),
(440, 110, '_menu_item_url', ''),
(442, 9, '_wp_trash_meta_status', 'publish'),
(443, 9, '_wp_trash_meta_time', '1600173961'),
(444, 9, '_wp_desired_post_slug', 'newsletter'),
(445, 64, '_wp_trash_meta_status', 'publish'),
(446, 64, '_wp_trash_meta_time', '1600173963'),
(447, 64, '_wp_desired_post_slug', 'cpt'),
(448, 112, '_edit_last', '1'),
(449, 112, '_edit_lock', '1600410856:1'),
(450, 112, '_wp_page_template', 'page-welcome.php'),
(451, 112, '_yoast_wpseo_content_score', '30'),
(452, 114, '_wp_attached_file', '2020/09/parti-tamas-img.jpg'),
(453, 114, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:993;s:6:\"height\";i:1083;s:4:\"file\";s:27:\"2020/09/parti-tamas-img.jpg\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"parti-tamas-img-275x300.jpg\";s:5:\"width\";i:275;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:28:\"parti-tamas-img-939x1024.jpg\";s:5:\"width\";i:939;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"parti-tamas-img-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:27:\"parti-tamas-img-768x838.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:838;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:3:\"max\";a:4:{s:4:\"file\";s:28:\"parti-tamas-img-990x1080.jpg\";s:5:\"width\";i:990;s:6:\"height\";i:1080;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
(454, 112, '_thumbnail_id', '114'),
(455, 114, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:7.77628095221433;s:5:\"bytes\";i:133343;s:11:\"size_before\";i:1714740;s:10:\"size_after\";i:1581397;s:4:\"time\";d:0.5700000000000001;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:5:{s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";d:7.54;s:5:\"bytes\";i:4485;s:11:\"size_before\";i:59505;s:10:\"size_after\";i:55020;s:4:\"time\";d:0.02;}s:5:\"large\";O:8:\"stdClass\":5:{s:7:\"percent\";d:7.8;s:5:\"bytes\";i:45876;s:11:\"size_before\";i:588036;s:10:\"size_after\";i:542160;s:4:\"time\";d:0.28;}s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:9.38;s:5:\"bytes\";i:1847;s:11:\"size_before\";i:19697;s:10:\"size_after\";i:17850;s:4:\"time\";d:0.01;}s:12:\"medium_large\";O:8:\"stdClass\":5:{s:7:\"percent\";d:7.6;s:5:\"bytes\";i:30957;s:11:\"size_before\";i:407161;s:10:\"size_after\";i:376204;s:4:\"time\";d:0.16;}s:3:\"max\";O:8:\"stdClass\":5:{s:7:\"percent\";d:7.84;s:5:\"bytes\";i:50178;s:11:\"size_before\";i:640341;s:10:\"size_after\";i:590163;s:4:\"time\";d:0.1;}}}'),
(456, 115, '_edit_last', '1'),
(457, 115, '_wp_page_template', 'default'),
(458, 115, '_edit_lock', '1600178931:1'),
(459, 118, '_edit_last', '1'),
(460, 118, '_wp_page_template', 'default'),
(461, 118, '_edit_lock', '1600178798:1'),
(462, 123, '_menu_item_type', 'post_type'),
(463, 123, '_menu_item_menu_item_parent', '0'),
(464, 123, '_menu_item_object_id', '118'),
(465, 123, '_menu_item_object', 'page'),
(466, 123, '_menu_item_target', ''),
(467, 123, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(468, 123, '_menu_item_xfn', ''),
(469, 123, '_menu_item_url', ''),
(471, 115, '_wp_trash_meta_status', 'publish'),
(472, 115, '_wp_trash_meta_time', '1600411363'),
(473, 115, '_wp_desired_post_slug', 'publikacioink'),
(474, 127, '_menu_item_type', 'post_type'),
(475, 127, '_menu_item_menu_item_parent', '0'),
(476, 127, '_menu_item_object_id', '42'),
(477, 127, '_menu_item_object', 'page'),
(478, 127, '_menu_item_target', ''),
(479, 127, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(480, 127, '_menu_item_xfn', ''),
(481, 127, '_menu_item_url', '');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT 0,
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2017-10-31 08:28:29', '2017-10-31 08:28:29', 'Üdvözlet! Ez az első saját WordPress-bejegyzés. Módosítható, vagy törölhető, aztán megkezdhető a honlap tartalommal történő feltöltése!', 'BLOG POST 1', '', 'trash', 'open', 'open', '', 'hello-vilag__trashed', '', '', '2020-09-15 11:03:15', '2020-09-15 09:03:15', '', 0, 'http://localhost/dxengine/?p=1', 0, 'post', '', 1),
(2, 1, '2017-10-31 08:28:29', '2017-10-31 08:28:29', '', 'Főoldal', '', 'publish', 'closed', 'open', '', 'ez-egy-minta-oldal', '', '', '2020-03-12 09:57:34', '2020-03-12 09:57:34', '', 0, 'http://localhost/dxengine/?page_id=2', 0, 'page', '', 0),
(5, 1, '2018-06-02 09:12:39', '2018-06-02 09:12:39', '<div class=\"row contact-form\">\r\n <div class=\"col-lg-6 name\"> \r\n  [text* your-name placeholder \"Your Name\"]\r\n</div>\r\n<div class=\"col-lg-6 email\">\r\n  [email* your-email placeholder \"Your Email\"]\r\n</div>\r\n<div class=\"col-lg-12 message\">\r\n  [textarea your-message placeholder \"Your Message\"]\r\n</div>\r\n<div class=\"col-lg-12 submit\">\r\n  [submit \"Send\"]\r\n</div>\r\n</div>\n1\nÉrdeklődés\n[your-email]\nadmin@email.com\nÜzenet:\r\n[your-message]\n\n\n1\n\n1\nSikeres Beküldés\nadmin@email.com\n[your-email]\nSikeres Beküldés\n\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nNem megfelelő a dátum formátuma.\nA dátum korábbi, mint az engedélyezett legkorábbi időpont.\nA dátum az engedélyezett időpont utáni.\nIsmeretlen eredetű hiba történt a fájlfeltöltéskor.\nNem engedélyezett az ilyen típusú fájl feltöltése.\nTúl nagyméretű a fájl.\nHiba történt a fájlfeltöltés közben.\nA szám formátuma érvénytelen.\nA szám kisebb az engedélyezett legkisebb számnál.\nA szám nagyobb az engedélyezett legnagyobb számnál.\nA quiz-kérdésre adott válasz hibás.\nA megadott e-mail cím érvénytelen.\nAz URL Cím érvénytelen\nA telefonszám érvénytelen.', 'Contact form 1', '', 'publish', 'closed', 'closed', '', 'contact-form-1', '', '', '2020-03-12 12:29:17', '2020-03-12 12:29:17', '', 0, 'http://localhost/dxengine/?post_type=wpcf7_contact_form&#038;p=5', 0, 'wpcf7_contact_form', '', 0),
(9, 1, '2020-03-10 09:54:33', '2020-03-10 09:54:33', '[newsletter]', 'Newsletter', '', 'trash', 'closed', 'closed', '', 'newsletter__trashed', '', '', '2020-09-15 14:46:01', '2020-09-15 12:46:01', '', 0, 'http://localhost/dxengine/newsletter/', 0, 'page', '', 0),
(10, 1, '2020-03-10 10:33:22', '2020-03-10 10:33:22', 'Ez egy mintaoldal, egy WordPress-alapú honlap egy oldalának létrehozásához. Az oldal különbözik a bejegyzéstől, mert az oldal állandóan látszik a honlap menüjében, a navigáció során (a legtöbb sablonban). Az emberek többsége használja a Névjegy oldalt, amely arra szolgál, hogy a lehetséges látogatói számára elmondjanak magukról, vállalkozásukról, a honlapról néhány jellemző mondatot. Egyik lehetséges megoldás erre pl.:\n<blockquote>Helló Emberek!Ezt a fordítást a WordPress Magyarország Fordítói csapata végezte. Sok száz munkaórát fektettünk abba, hogy ezt a remek tartalomkezelő rendszert anyanyelvünkön, magyarul használhassa mindenki.</blockquote>\n.... vagy egy másik példa:\n<blockquote>A<strong> WordPress Magyarország</strong> tulajdonképpen a kezdetektől létezik anélkül, hogy kihirdette volna létét. <strong>A WordPress Magyarország egy Közösség!</strong> - a nyílt forráskódú, zseniális WordPress tartalomkezelő rendszer alkalmazására, megismerésére, tanulására szövetkeztünk.</blockquote>\nÚj WordPress-felhasználóként a <a href=\"http://localhost/dxengine/wp-admin/\">Vezérlőpult</a>ba lépve ez az oldal törölhető, módosítható, és új oldalak hozhatóak létre saját tartalommal. Jó szórakozást kíván a WordPress Nemzeközi Közössége!', 'Ez egy minta oldal', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2020-03-10 10:33:22', '2020-03-10 10:33:22', '', 2, 'https://localhost/wordpress-bootstrap-4-boilerplate/2-revision-v1/', 0, 'revision', '', 0),
(11, 1, '2020-03-10 13:15:13', '2020-03-10 13:15:13', 'Üdvözlet! Ez az első saját WordPress-bejegyzés. Módosítható, vagy törölhető, aztán megkezdhető a honlap tartalommal történő feltöltése!', 'BLOG POST 3', '', 'trash', 'open', 'open', '', 'blog-post-3__trashed', '', '', '2020-09-15 11:03:15', '2020-09-15 09:03:15', '', 0, 'http://localhost/dxengine/?p=11', 0, 'post', '', 0),
(12, 1, '2020-03-10 13:14:59', '2020-03-10 13:14:59', 'Üdvözlet! Ez az első saját WordPress-bejegyzés. Módosítható, vagy törölhető, aztán megkezdhető a honlap tartalommal történő feltöltése!', 'BLOG POST 2', '', 'trash', 'open', 'open', '', 'blog-post-2__trashed', '', '', '2020-09-15 11:03:15', '2020-09-15 09:03:15', '', 0, 'http://localhost/dxengine/?p=12', 0, 'post', '', 0),
(13, 1, '2020-03-10 13:14:40', '2020-03-10 13:14:40', '', 'placeholder-img-3', '', 'inherit', 'open', 'closed', '', 'placeholder-img-3', '', '', '2020-03-10 13:14:40', '2020-03-10 13:14:40', '', 12, 'http://localhost/dxengine/wp-content/uploads/2020/03/placeholder-img-3.jpg', 0, 'attachment', 'image/jpeg', 0),
(14, 1, '2020-03-10 13:14:59', '2020-03-10 13:14:59', 'Üdvözlet! Ez az első saját WordPress-bejegyzés. Módosítható, vagy törölhető, aztán megkezdhető a honlap tartalommal történő feltöltése!', 'BLOG POST 2', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2020-03-10 13:14:59', '2020-03-10 13:14:59', '', 12, 'http://localhost/dxengine/12-revision-v1/', 0, 'revision', '', 0),
(15, 1, '2020-03-10 13:15:13', '2020-03-10 13:15:13', 'Üdvözlet! Ez az első saját WordPress-bejegyzés. Módosítható, vagy törölhető, aztán megkezdhető a honlap tartalommal történő feltöltése!', 'BLOG POST 3', '', 'inherit', 'closed', 'closed', '', '11-revision-v1', '', '', '2020-03-10 13:15:13', '2020-03-10 13:15:13', '', 11, 'http://localhost/dxengine/11-revision-v1/', 0, 'revision', '', 0),
(16, 1, '2020-03-10 13:15:25', '2020-03-10 13:15:25', 'Üdvözlet! Ez az első saját WordPress-bejegyzés. Módosítható, vagy törölhető, aztán megkezdhető a honlap tartalommal történő feltöltése!', 'BLOG POST 1', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2020-03-10 13:15:25', '2020-03-10 13:15:25', '', 1, 'http://localhost/dxengine/1-revision-v1/', 0, 'revision', '', 0),
(17, 1, '2020-03-12 09:35:02', '2020-03-12 09:35:02', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:8:\"our_team\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Our Team', 'our-team', 'publish', 'closed', 'closed', '', 'group_5e6a022d542c0', '', '', '2020-03-12 10:06:24', '2020-03-12 10:06:24', '', 0, 'http://localhost/dxengine/?post_type=acf-field-group&#038;p=17', 0, 'acf-field-group', '', 0),
(19, 1, '2020-03-12 09:35:02', '2020-03-12 09:35:02', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Titulus', 'member_titulus', 'publish', 'closed', 'closed', '', 'field_5e6a023c16dca', '', '', '2020-03-12 10:06:24', '2020-03-12 10:06:24', '', 17, 'http://localhost/dxengine/?post_type=acf-field&#038;p=19', 0, 'acf-field', '', 0),
(20, 1, '2020-03-12 09:39:05', '2020-03-12 09:39:05', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, omnis doloremque non cum id reprehenderit, quisquam totam aspernatur tempora minima unde aliquid ea culpa sunt. Reiciendis quia dolorum ducimus unde.', 'Team Member 1', '', 'publish', 'closed', 'closed', '', 'team-member-1', '', '', '2020-03-12 09:39:05', '2020-03-12 09:39:05', '', 0, 'http://localhost/dxengine/?post_type=our_team&#038;p=20', 0, 'our_team', '', 0),
(22, 1, '2020-03-12 09:38:58', '2020-03-12 09:38:58', '', 'unknown-person', '', 'inherit', 'open', 'closed', '', 'unknown-person', '', '', '2020-03-12 09:38:58', '2020-03-12 09:38:58', '', 20, 'http://localhost/dxengine/wp-content/uploads/2020/03/unknown-person.png', 0, 'attachment', 'image/png', 0),
(23, 1, '2020-03-12 09:39:12', '2020-03-12 09:39:12', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, omnis doloremque non cum id reprehenderit, quisquam totam aspernatur tempora minima unde aliquid ea culpa sunt. Reiciendis quia dolorum ducimus unde.', 'Team Member 2', '', 'publish', 'closed', 'closed', '', 'team-member-2', '', '', '2020-03-12 09:41:12', '2020-03-12 09:41:12', '', 0, 'http://localhost/dxengine/?post_type=our_team&#038;p=23', 0, 'our_team', '', 0),
(24, 1, '2020-03-12 09:39:14', '2020-03-12 09:39:14', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, omnis doloremque non cum id reprehenderit, quisquam totam aspernatur tempora minima unde aliquid ea culpa sunt. Reiciendis quia dolorum ducimus unde.', 'Team Member 3', '', 'publish', 'closed', 'closed', '', 'team-member-3', '', '', '2020-03-12 09:41:16', '2020-03-12 09:41:16', '', 0, 'http://localhost/dxengine/?post_type=our_team&#038;p=24', 0, 'our_team', '', 0),
(25, 1, '2020-03-12 09:39:17', '2020-03-12 09:39:17', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, omnis doloremque non cum id reprehenderit, quisquam totam aspernatur tempora minima unde aliquid ea culpa sunt. Reiciendis quia dolorum ducimus unde.', 'Team Member 4', '', 'publish', 'closed', 'closed', '', 'team-member-4', '', '', '2020-03-12 09:41:24', '2020-03-12 09:41:24', '', 0, 'http://localhost/dxengine/?post_type=our_team&#038;p=25', 0, 'our_team', '', 0),
(26, 1, '2020-03-12 09:57:34', '2020-03-12 09:57:34', '', 'Főoldal', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2020-03-12 09:57:34', '2020-03-12 09:57:34', '', 2, 'http://localhost/dxengine/2-revision-v1/', 0, 'revision', '', 0),
(27, 1, '2020-03-12 10:06:08', '2020-03-12 10:06:08', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:11:\"testimonial\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Testimonial', 'testimonial', 'publish', 'closed', 'closed', '', 'group_5e6a096c84b2f', '', '', '2020-03-12 10:06:08', '2020-03-12 10:06:08', '', 0, 'http://localhost/dxengine/?post_type=acf-field-group&#038;p=27', 0, 'acf-field-group', '', 0),
(29, 1, '2020-03-12 10:06:08', '2020-03-12 10:06:08', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Titulus', 'testimonial_titulus', 'publish', 'closed', 'closed', '', 'field_5e6a0977edbdf', '', '', '2020-03-12 10:06:08', '2020-03-12 10:06:08', '', 27, 'http://localhost/dxengine/?post_type=acf-field&p=29', 0, 'acf-field', '', 0),
(30, 1, '2020-03-12 10:07:25', '2020-03-12 10:07:25', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, expedita, saepe, vero rerum deleniti beatae veniam harum neque nemo praesentium cum alias asperiores commodi.', 'Testimonial 1', '', 'publish', 'closed', 'closed', '', 'testimonial-1', '', '', '2020-03-12 10:07:25', '2020-03-12 10:07:25', '', 0, 'http://localhost/dxengine/?post_type=testimonial&#038;p=30', 0, 'testimonial', '', 0),
(32, 1, '2020-03-12 10:07:32', '2020-03-12 10:07:32', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, expedita, saepe, vero rerum deleniti beatae veniam harum neque nemo praesentium cum alias asperiores commodi.', 'Testimonial 2', '', 'publish', 'closed', 'closed', '', 'testimonial-2', '', '', '2020-03-12 10:07:43', '2020-03-12 10:07:43', '', 0, 'http://localhost/dxengine/?post_type=testimonial&#038;p=32', 0, 'testimonial', '', 0),
(33, 1, '2020-03-12 10:07:35', '2020-03-12 10:07:35', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, expedita, saepe, vero rerum deleniti beatae veniam harum neque nemo praesentium cum alias asperiores commodi.', 'Testimonial 3', '', 'publish', 'closed', 'closed', '', 'testimonial-3', '', '', '2020-03-12 10:07:48', '2020-03-12 10:07:48', '', 0, 'http://localhost/dxengine/?post_type=testimonial&#038;p=33', 0, 'testimonial', '', 0),
(34, 1, '2020-03-12 10:07:37', '2020-03-12 10:07:37', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, expedita, saepe, vero rerum deleniti beatae veniam harum neque nemo praesentium cum alias asperiores commodi.', 'Testimonial 4', '', 'publish', 'closed', 'closed', '', 'testimonial-4', '', '', '2020-03-12 10:07:53', '2020-03-12 10:07:53', '', 0, 'http://localhost/dxengine/?post_type=testimonial&#038;p=34', 0, 'testimonial', '', 0),
(35, 1, '2020-03-12 13:23:33', '2020-03-12 13:23:33', '', 'KÁROLI GÁSPÁR REFORMÁTUS EGYETEM', '', 'publish', 'closed', 'closed', '', 'partner-1', '', '', '2020-09-15 13:40:06', '2020-09-15 11:40:06', '', 0, 'http://localhost/dxengine/?post_type=partner&#038;p=35', 0, 'partner', '', 0),
(37, 1, '2020-03-12 13:23:21', '2020-03-12 13:23:21', '', 'partner-logo', '', 'inherit', 'open', 'closed', '', 'partner-logo', '', '', '2020-03-12 13:23:21', '2020-03-12 13:23:21', '', 35, 'http://localhost/dxengine/wp-content/uploads/2020/03/partner-logo.png', 0, 'attachment', 'image/png', 0),
(38, 1, '2020-03-11 13:23:38', '2020-03-11 12:23:38', '', 'MOKK', '', 'publish', 'closed', 'closed', '', 'partner-2', '', '', '2020-09-15 13:34:23', '2020-09-15 11:34:23', '', 0, 'http://localhost/dxengine/?post_type=partner&#038;p=38', 0, 'partner', '', 0),
(39, 1, '2020-03-10 13:23:41', '2020-03-10 12:23:41', '', 'Műegyetem', '', 'publish', 'closed', 'closed', '', 'partner-3', '', '', '2020-09-15 13:34:27', '2020-09-15 11:34:27', '', 0, 'http://localhost/dxengine/?post_type=partner&#038;p=39', 0, 'partner', '', 0),
(40, 1, '2020-03-09 13:23:44', '2020-03-09 12:23:44', '', 'Kürt Akadémia', '', 'publish', 'closed', 'closed', '', 'partner-4', '', '', '2020-09-15 13:34:15', '2020-09-15 11:34:15', '', 0, 'http://localhost/dxengine/?post_type=partner&#038;p=40', 0, 'partner', '', 0),
(42, 1, '2020-03-24 14:14:36', '2020-03-24 13:14:36', '', 'Publikációink', '', 'publish', 'closed', 'closed', '', 'publikacioink', '', '', '2020-09-18 08:49:24', '2020-09-18 06:49:24', '', 0, 'http://localhost/dxengine/?page_id=42', 0, 'page', '', 0),
(44, 1, '2020-03-24 14:14:36', '2020-03-24 14:14:36', '', 'Blog', '', 'inherit', 'closed', 'closed', '', '42-revision-v1', '', '', '2020-03-24 14:14:36', '2020-03-24 14:14:36', '', 42, 'http://localhost/dxengine/42-revision-v1/', 0, 'revision', '', 0),
(45, 1, '2020-03-24 14:15:11', '2020-03-24 14:15:11', '{\n    \"page_for_posts\": {\n        \"value\": \"42\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-03-24 14:15:11\"\n    }\n}', '', '', 'publish', 'closed', 'closed', '', 'b6c38f01-2be6-4993-8cd2-4dd5c3b4182a', '', '', '2020-03-24 14:15:11', '2020-03-24 14:15:11', '', 0, 'http://localhost/dxengine/b6c38f01-2be6-4993-8cd2-4dd5c3b4182a/', 0, 'customize_changeset', '', 0),
(51, 1, '2020-03-26 13:04:03', '2020-03-26 13:04:03', 'Üdvözlet! Ez az első saját WordPress-bejegyzés. Módosítható, vagy törölhető, aztán megkezdhető a honlap tartalommal történő feltöltése!', 'BLOG POST 4', '', 'trash', 'open', 'open', '', 'blog-post-4__trashed', '', '', '2020-09-15 11:03:15', '2020-09-15 09:03:15', '', 0, 'http://localhost/dxengine/?p=51', 0, 'post', '', 0),
(52, 1, '2020-03-26 13:04:06', '2020-03-26 13:04:06', 'Üdvözlet! Ez az első saját WordPress-bejegyzés. Módosítható, vagy törölhető, aztán megkezdhető a honlap tartalommal történő feltöltése!', 'BLOG POST 5', '', 'trash', 'open', 'open', '', 'blog-post-5__trashed', '', '', '2020-09-15 11:03:15', '2020-09-15 09:03:15', '', 0, 'http://localhost/dxengine/?p=52', 0, 'post', '', 0),
(53, 1, '2020-03-26 13:04:11', '2020-03-26 13:04:11', 'Üdvözlet! Ez az első saját WordPress-bejegyzés. Módosítható, vagy törölhető, aztán megkezdhető a honlap tartalommal történő feltöltése!', 'BLOG POST 6', '', 'trash', 'open', 'open', '', 'blog-post-6__trashed', '', '', '2020-09-15 11:03:15', '2020-09-15 09:03:15', '', 0, 'http://localhost/dxengine/?p=53', 0, 'post', '', 0),
(54, 1, '2020-03-26 13:04:19', '2020-03-26 13:04:19', 'Üdvözlet! Ez az első saját WordPress-bejegyzés. Módosítható, vagy törölhető, aztán megkezdhető a honlap tartalommal történő feltöltése!', 'BLOG POST 4', '', 'inherit', 'closed', 'closed', '', '51-revision-v1', '', '', '2020-03-26 13:04:19', '2020-03-26 13:04:19', '', 51, 'http://localhost/dxengine/51-revision-v1/', 0, 'revision', '', 0),
(55, 1, '2020-03-26 13:04:28', '2020-03-26 13:04:28', 'Üdvözlet! Ez az első saját WordPress-bejegyzés. Módosítható, vagy törölhető, aztán megkezdhető a honlap tartalommal történő feltöltése!', 'BLOG POST 5', '', 'inherit', 'closed', 'closed', '', '52-revision-v1', '', '', '2020-03-26 13:04:28', '2020-03-26 13:04:28', '', 52, 'http://localhost/dxengine/52-revision-v1/', 0, 'revision', '', 0),
(56, 1, '2020-03-26 13:04:33', '2020-03-26 13:04:33', 'Üdvözlet! Ez az első saját WordPress-bejegyzés. Módosítható, vagy törölhető, aztán megkezdhető a honlap tartalommal történő feltöltése!', 'BLOG POST 6', '', 'inherit', 'closed', 'closed', '', '53-revision-v1', '', '', '2020-03-26 13:04:33', '2020-03-26 13:04:33', '', 53, 'http://localhost/dxengine/53-revision-v1/', 0, 'revision', '', 0),
(64, 1, '2020-03-30 09:22:05', '2020-03-30 09:22:05', '', 'CPT', '', 'trash', 'closed', 'closed', '', 'cpt__trashed', '', '', '2020-09-15 14:46:03', '2020-09-15 12:46:03', '', 0, 'http://localhost/dxengine/?page_id=64', 0, 'page', '', 0),
(66, 1, '2020-03-30 09:22:05', '2020-03-30 09:22:05', '', 'Portfolio', '', 'inherit', 'closed', 'closed', '', '64-revision-v1', '', '', '2020-03-30 09:22:05', '2020-03-30 09:22:05', '', 64, 'http://localhost/dxengine/64-revision-v1/', 0, 'revision', '', 0),
(68, 1, '2020-03-30 09:35:25', '2020-03-30 09:35:25', '', 'CPT', '', 'inherit', 'closed', 'closed', '', '64-revision-v1', '', '', '2020-03-30 09:35:25', '2020-03-30 09:35:25', '', 64, 'http://localhost/dxengine/64-revision-v1/', 0, 'revision', '', 0),
(71, 1, '2020-03-31 11:22:00', '2020-03-31 11:22:00', '', 'dx-engine-fav', '', 'inherit', 'open', 'closed', '', 'dx-engine-fav', '', '', '2020-03-31 11:22:00', '2020-03-31 11:22:00', '', 0, 'http://localhost/dxengine/wp-content/uploads/2020/03/dx-engine-fav.png', 0, 'attachment', 'image/png', 0),
(72, 1, '2020-03-31 11:22:05', '2020-03-31 11:22:05', 'http://localhost/dxengine/wp-content/uploads/2020/03/cropped-dx-engine-fav.png', 'cropped-dx-engine-fav.png', '', 'inherit', 'open', 'closed', '', 'cropped-dx-engine-fav-png', '', '', '2020-03-31 11:22:05', '2020-03-31 11:22:05', '', 0, 'http://localhost/dxengine/wp-content/uploads/2020/03/cropped-dx-engine-fav.png', 0, 'attachment', 'image/png', 0),
(73, 1, '2020-03-31 11:22:59', '2020-03-31 11:22:59', '', 'dx-engine-fav', '', 'inherit', 'open', 'closed', '', 'dx-engine-fav-2', '', '', '2020-03-31 11:22:59', '2020-03-31 11:22:59', '', 0, 'http://localhost/dxengine/wp-content/uploads/2020/03/dx-engine-fav-1.png', 0, 'attachment', 'image/png', 0),
(74, 1, '2020-03-31 11:23:04', '2020-03-31 11:23:04', 'http://localhost/dxengine/wp-content/uploads/2020/03/cropped-dx-engine-fav-1.png', 'cropped-dx-engine-fav-1.png', '', 'inherit', 'open', 'closed', '', 'cropped-dx-engine-fav-1-png', '', '', '2020-03-31 11:23:04', '2020-03-31 11:23:04', '', 0, 'http://localhost/dxengine/wp-content/uploads/2020/03/cropped-dx-engine-fav-1.png', 0, 'attachment', 'image/png', 0),
(75, 1, '2020-03-31 11:23:18', '2020-03-31 11:23:18', '{\n    \"site_icon\": {\n        \"value\": 74,\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-03-31 11:23:07\"\n    },\n    \"blogdescription\": {\n        \"value\": \"powered by dxlabz\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-03-31 11:23:18\"\n    }\n}', '', '', 'publish', 'closed', 'closed', '', '00fe786f-e330-4df2-9d6b-363f500833e5', '', '', '2020-03-31 11:23:18', '2020-03-31 11:23:18', '', 0, 'http://localhost/dxengine/?p=75', 0, 'customize_changeset', '', 0),
(78, 1, '2020-09-11 12:03:39', '2020-09-11 10:03:39', '', 'mokk-adatkutatoalintezet-logo-small', '', 'inherit', 'open', 'closed', '', 'mokk-adatkutatoalintezet-logo-small', '', '', '2020-09-11 12:03:39', '2020-09-11 10:03:39', '', 0, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/mokk-adatkutatoalintezet-logo-small.png', 0, 'attachment', 'image/png', 0),
(79, 1, '2020-09-11 12:03:42', '2020-09-11 10:03:42', 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/cropped-mokk-adatkutatoalintezet-logo-small.png', 'cropped-mokk-adatkutatoalintezet-logo-small.png', '', 'inherit', 'open', 'closed', '', 'cropped-mokk-adatkutatoalintezet-logo-small-png', '', '', '2020-09-11 12:03:42', '2020-09-11 10:03:42', '', 0, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/cropped-mokk-adatkutatoalintezet-logo-small.png', 0, 'attachment', 'image/png', 0),
(80, 1, '2020-09-11 12:03:42', '2020-09-11 10:03:42', '{{unknown}}', '', '', 'publish', 'closed', 'closed', '', '5470514ddfbc3e69add646876360f311', '', '', '2020-09-11 12:03:42', '2020-09-11 10:03:42', '', 0, 'http://localhost/mokk-aali.hu/5470514ddfbc3e69add646876360f311/', 0, 'oembed_cache', '', 0),
(81, 1, '2020-09-11 12:04:04', '2020-09-11 10:04:04', '{\n    \"site_icon\": {\n        \"value\": 79,\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-09-11 10:04:04\"\n    }\n}', '', '', 'publish', 'closed', 'closed', '', '99cda8b6-ad56-4adf-ab97-d27ec25cb6fd', '', '', '2020-09-11 12:04:04', '2020-09-11 10:04:04', '', 0, 'http://localhost/mokk-aali.hu/99cda8b6-ad56-4adf-ab97-d27ec25cb6fd/', 0, 'customize_changeset', '', 0),
(82, 1, '2020-09-14 11:38:59', '2020-09-14 09:38:59', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:12:\"publications\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Publikáció', 'publikacio', 'publish', 'closed', 'closed', '', 'group_5f5f39f53f6ed', '', '', '2020-09-14 11:44:40', '2020-09-14 09:44:40', '', 0, 'http://localhost/mokk-aali.hu/?post_type=acf-field-group&#038;p=82', 0, 'acf-field-group', '', 0),
(83, 1, '2020-09-14 11:38:59', '2020-09-14 09:38:59', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Publikáció szerzője', 'publikacio_szerzoje', 'publish', 'closed', 'closed', '', 'field_5f5f3a01d7486', '', '', '2020-09-14 11:44:40', '2020-09-14 09:44:40', '', 82, 'http://localhost/mokk-aali.hu/?post_type=acf-field&#038;p=83', 1, 'acf-field', '', 0),
(84, 1, '2020-09-14 11:38:59', '2020-09-14 09:38:59', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Publikálás dátuma', 'publikalas_datuma', 'publish', 'closed', 'closed', '', 'field_5f5f3a17d7487', '', '', '2020-09-14 11:44:40', '2020-09-14 09:44:40', '', 82, 'http://localhost/mokk-aali.hu/?post_type=acf-field&#038;p=84', 2, 'acf-field', '', 0),
(85, 1, '2020-09-14 11:43:36', '2020-09-14 09:43:36', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'MI és Mi - joggyakorlat és jogalkalmazás - hálózati szabályozás - Gondolatok a technológiai alkalmazkodásról', '', 'publish', 'closed', 'closed', '', 'mi-es-mi-joggyakorlat-es-jogalkalmazas-halozati-szabalyozas-gondolatok-a-technologiai-alkalmazkodasrol', '', '', '2020-09-14 15:12:22', '2020-09-14 13:12:22', '', 0, 'http://localhost/mokk-aali.hu/?post_type=publications&#038;p=85', 0, 'publications', '', 0),
(86, 1, '2020-09-14 11:44:40', '2020-09-14 09:44:40', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Publikáció megjelenési helye', 'publikacio_megjelenesi_helye', 'publish', 'closed', 'closed', '', 'field_5f5f3b73b9938', '', '', '2020-09-14 11:44:40', '2020-09-14 09:44:40', '', 82, 'http://localhost/mokk-aali.hu/?post_type=acf-field&p=86', 0, 'acf-field', '', 0),
(87, 1, '2020-09-14 14:54:54', '2020-09-14 12:54:54', '', 'publikacio-cikk-1', '', 'inherit', 'open', 'closed', '', 'publikacio-cikk-1', '', '', '2020-09-14 14:54:54', '2020-09-14 12:54:54', '', 85, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/publikacio-cikk-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(88, 1, '2020-09-13 17:03:14', '2020-09-13 15:03:14', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Gondolatok a joggyakorlat és a jogalkalmazás technológiai fejlődéshez való alkalmazkodásáról', '', 'publish', 'closed', 'closed', '', 'gondolatok-a-joggyakorlat-es-a-jogalkalmazas-technologiai-fejlodeshez-valo-alkalmazkodasarol', '', '', '2020-09-14 17:04:25', '2020-09-14 15:04:25', '', 0, 'http://localhost/mokk-aali.hu/?post_type=publications&#038;p=88', 0, 'publications', '', 0),
(89, 1, '2020-09-14 17:02:51', '2020-09-14 15:02:51', '', 'publikacio-cikk-2', '', 'inherit', 'open', 'closed', '', 'publikacio-cikk-2', '', '', '2020-09-14 17:02:51', '2020-09-14 15:02:51', '', 88, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/publikacio-cikk-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(90, 1, '2020-09-12 17:07:09', '2020-09-12 15:07:09', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Budapesti Futurológiai Fórum 2016', '', 'publish', 'closed', 'closed', '', 'budapesti-futurologiai-forum-2016', '', '', '2020-09-14 17:07:51', '2020-09-14 15:07:51', '', 0, 'http://localhost/mokk-aali.hu/?post_type=publications&#038;p=90', 0, 'publications', '', 0),
(91, 1, '2020-09-14 17:07:40', '2020-09-14 15:07:40', '', 'publikacio-cikk-3', '', 'inherit', 'open', 'closed', '', 'publikacio-cikk-3', '', '', '2020-09-14 17:07:40', '2020-09-14 15:07:40', '', 90, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/publikacio-cikk-3.jpg', 0, 'attachment', 'image/jpeg', 0),
(92, 1, '2020-09-15 08:34:31', '2020-09-15 06:34:31', 'Az Európai Közjegyzői Hálózat az európai közjegyzőség tudományos együttműködési, valamint képzésért felelős szervezete. Tagsága megegyezik a CNUE tagságával (22 teljes jogú és 1 megfigyelői státuszú tag), a tagok képviseletét a tagközjegyzőségek által kijelölt kapcsolattartók (interlocutor/interlocuteur) látják el. A Hálózat belső és külső tevékenysége elkülönül egymástól. Belső tevékenysége a közjegyzők, valamint a kamarák részére történő tájékoztatás nyújtás, valamint az európai szintű közjegyzői képzéssel kapcsolatos feladatok ellátása. A szervezet külső tevékenysége a nagyközönség számára létrehozott többnyelvű információs honlapok létrehozatalában testesül meg, melyek a következő linkre kattintva érhetők el: ARERT/ENRWA (Végrendeleti Nyilvántartások Európai Hálózatának Egyesülete - Association du Réseau Européen des Registres Testamentaires/European Network of Registers of Wills Association)', 'Európai Közjegyzői Hálózat (ENN-European Notarial Network- RNE-Réseau Notarial Européen)', '', 'publish', 'closed', 'closed', '', 'europai-kozjegyzoi-halozat-enn-european-notarial-network-rne-reseau-notarial-europeen', '', '', '2020-09-15 08:34:31', '2020-09-15 06:34:31', '', 0, 'http://localhost/mokk-aali.hu/?post_type=int_contacts&#038;p=92', 0, 'int_contacts', '', 0),
(93, 1, '2020-09-14 08:35:00', '2020-09-14 06:35:00', 'Az Európai Közjegyzői Hálózat az európai közjegyzőség tudományos együttműködési, valamint képzésért felelős szervezete. Tagsága megegyezik a CNUE tagságával (22 teljes jogú és 1 megfigyelői státuszú tag), a tagok képviseletét a tagközjegyzőségek által kijelölt kapcsolattartók (interlocutor/interlocuteur) látják el. A Hálózat belső és külső tevékenysége elkülönül egymástól. Belső tevékenysége a közjegyzők, valamint a kamarák részére történő tájékoztatás nyújtás, valamint az európai szintű közjegyzői képzéssel kapcsolatos feladatok ellátása. A szervezet külső tevékenysége a nagyközönség számára létrehozott többnyelvű információs honlapok létrehozatalában testesül meg, melyek a következő linkre kattintva érhetők el: ARERT/ENRWA (Végrendeleti Nyilvántartások Európai Hálózatának Egyesülete - Association du Réseau Européen des Registres Testamentaires/European Network of Registers of Wills Association)', 'Francia-Magyar Bizottság (Comité franco-hongrois)', '', 'publish', 'closed', 'closed', '', 'francia-magyar-bizottsag-comite-franco-hongrois', '', '', '2020-09-15 08:36:00', '2020-09-15 06:36:00', '', 0, 'http://localhost/mokk-aali.hu/?post_type=int_contacts&#038;p=93', 0, 'int_contacts', '', 0),
(94, 1, '2020-09-13 08:35:21', '2020-09-13 06:35:21', 'Az Európai Közjegyzői Hálózat az európai közjegyzőség tudományos együttműködési, valamint képzésért felelős szervezete. Tagsága megegyezik a CNUE tagságával (22 teljes jogú és 1 megfigyelői státuszú tag), a tagok képviseletét a tagközjegyzőségek által kijelölt kapcsolattartók (interlocutor/interlocuteur) látják el. A Hálózat belső és külső tevékenysége elkülönül egymástól. Belső tevékenysége a közjegyzők, valamint a kamarák részére történő tájékoztatás nyújtás, valamint az európai szintű közjegyzői képzéssel kapcsolatos feladatok ellátása. A szervezet külső tevékenysége a nagyközönség számára létrehozott többnyelvű információs honlapok létrehozatalában testesül meg, melyek a következő linkre kattintva érhetők el: ARERT/ENRWA (Végrendeleti Nyilvántartások Európai Hálózatának Egyesülete - Association du Réseau Européen des Registres Testamentaires/European Network of Registers of Wills Association)', 'Közép-Európai Közjegyzői Együttműködés (Hexagonale)', '', 'publish', 'closed', 'closed', '', 'kozep-europai-kozjegyzoi-egyuttmukodes-hexagonale', '', '', '2020-09-15 08:35:56', '2020-09-15 06:35:56', '', 0, 'http://localhost/mokk-aali.hu/?post_type=int_contacts&#038;p=94', 0, 'int_contacts', '', 0),
(95, 1, '2020-09-12 08:35:37', '2020-09-12 06:35:37', 'Az Európai Közjegyzői Hálózat az európai közjegyzőség tudományos együttműködési, valamint képzésért felelős szervezete. Tagsága megegyezik a CNUE tagságával (22 teljes jogú és 1 megfigyelői státuszú tag), a tagok képviseletét a tagközjegyzőségek által kijelölt kapcsolattartók (interlocutor/interlocuteur) látják el. A Hálózat belső és külső tevékenysége elkülönül egymástól. Belső tevékenysége a közjegyzők, valamint a kamarák részére történő tájékoztatás nyújtás, valamint az európai szintű közjegyzői képzéssel kapcsolatos feladatok ellátása. A szervezet külső tevékenysége a nagyközönség számára létrehozott többnyelvű információs honlapok létrehozatalában testesül meg, melyek a következő linkre kattintva érhetők el: ARERT/ENRWA (Végrendeleti Nyilvántartások Európai Hálózatának Egyesülete - Association du Réseau Européen des Registres Testamentaires/European Network of Registers of Wills Association)', 'Európai Közjegyzői Hálózat (ENN-European Notarial Network-RNE-Réseau Notarial Européen', '', 'publish', 'closed', 'closed', '', 'europai-kozjegyzoi-halozat-enn-european-notarial-network-rne-reseau-notarial-europeen-2', '', '', '2020-09-15 08:36:30', '2020-09-15 06:36:30', '', 0, 'http://localhost/mokk-aali.hu/?post_type=int_contacts&#038;p=95', 0, 'int_contacts', '', 0),
(96, 1, '2020-09-15 11:04:49', '2020-09-15 09:04:49', '<strong>A Fórum célja, hogy felkutassa és nyomon kövesse azokat a jelenségeket, technológiákat és ezek lehetséges következményeit, amelyek hatással lehetnek vagy már hatással vannak a közjegyzői szolgáltatások területére. </strong>\r\n\r\nOlyan jelenségekkel foglalkozunk amelyekhez a közjegyzőknek alkalmazkodniuk kell és amelyek az alkalmazkodás következtében többé kevésbé megváltoztathatják a teljes közjegyzői környezetet a hivatásgyakorlástól kezdve a szabályozásig.', 'A Futurológiai Fórum 2020 szeptember 14-én online kerül megrendezésre', '', 'trash', 'open', 'open', '', 'a-futurologiai-forum-2020-szeptember-14-en-online-kerul-megrendezesre__trashed', '', '', '2020-09-15 11:14:15', '2020-09-15 09:14:15', '', 0, 'http://localhost/mokk-aali.hu/?p=96', 0, 'post', '', 0),
(97, 1, '2020-09-15 11:04:40', '2020-09-15 09:04:40', '', 'esemeny-1', '', 'inherit', 'open', 'closed', '', 'esemeny-1', '', '', '2020-09-15 11:04:40', '2020-09-15 09:04:40', '', 96, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/esemeny-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(98, 1, '2020-09-15 11:04:49', '2020-09-15 09:04:49', '<strong>A Fórum célja, hogy felkutassa és nyomon kövesse azokat a jelenségeket, technológiákat és ezek lehetséges következményeit, amelyek hatással lehetnek vagy már hatással vannak a közjegyzői szolgáltatások területére. </strong>\r\n\r\nOlyan jelenségekkel foglalkozunk amelyekhez a közjegyzőknek alkalmazkodniuk kell és amelyek az alkalmazkodás következtében többé kevésbé megváltoztathatják a teljes közjegyzői környezetet a hivatásgyakorlástól kezdve a szabályozásig.', 'A Futurológiai Fórum 2020 szeptember 14-én online kerül megrendezésre', '', 'inherit', 'closed', 'closed', '', '96-revision-v1', '', '', '2020-09-15 11:04:49', '2020-09-15 09:04:49', '', 96, 'http://localhost/mokk-aali.hu/96-revision-v1/', 0, 'revision', '', 0),
(99, 1, '2020-09-15 11:20:40', '2020-09-15 09:20:40', '<strong>A Fórum célja, hogy felkutassa és nyomon kövesse azokat a jelenségeket, technológiákat és ezek lehetséges következményeit, amelyek hatással lehetnek vagy már hatással vannak a közjegyzői szolgáltatások területére. </strong>\r\n\r\nOlyan jelenségekkel foglalkozunk amelyekhez a közjegyzőknek alkalmazkodniuk kell és amelyek az alkalmazkodás következtében többé kevésbé megváltoztathatják a teljes közjegyzői környezetet a hivatásgyakorlástól kezdve a szabályozásig.', 'A Futurológiai Fórum 2020 szeptember 14-én online kerül megrendezésre', '', 'publish', 'closed', 'closed', '', 'a-futurologiai-forum-2020-szeptember-14-en-online-kerul-megrendezesre', '', '', '2020-09-15 11:20:40', '2020-09-15 09:20:40', '', 0, 'http://localhost/mokk-aali.hu/?post_type=events&#038;p=99', 0, 'events', '', 0),
(101, 1, '2020-09-15 13:32:02', '2020-09-15 11:32:02', '', 'mokk_logo', '', 'inherit', 'open', 'closed', '', 'mokk_logo', '', '', '2020-09-15 13:32:02', '2020-09-15 11:32:02', '', 38, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/mokk_logo.png', 0, 'attachment', 'image/png', 0),
(102, 1, '2020-09-15 13:32:34', '2020-09-15 11:32:34', '', 'muegyetem', '', 'inherit', 'open', 'closed', '', 'muegyetem', '', '', '2020-09-15 13:32:34', '2020-09-15 11:32:34', '', 39, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/muegyetem.png', 0, 'attachment', 'image/png', 0),
(103, 1, '2020-09-15 13:33:41', '2020-09-15 11:33:41', '', 'kurt-akademia', '', 'inherit', 'open', 'closed', '', 'kurt-akademia', '', '', '2020-09-15 13:33:41', '2020-09-15 11:33:41', '', 40, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/kurt-akademia.png', 0, 'attachment', 'image/png', 0),
(104, 1, '2020-09-15 13:40:00', '2020-09-15 11:40:00', '', 'KRE_logo_A4_magyar', '', 'inherit', 'open', 'closed', '', 'kre_logo_a4_magyar', '', '', '2020-09-15 13:40:00', '2020-09-15 11:40:00', '', 35, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/KRE_logo_A4_magyar.png', 0, 'attachment', 'image/png', 0),
(105, 1, '2020-09-15 14:01:14', '2020-09-15 12:01:14', '', 'Kapcsolat', '', 'publish', 'closed', 'closed', '', 'kapcsolat', '', '', '2020-09-18 12:36:34', '2020-09-18 10:36:34', '', 0, 'http://localhost/mokk-aali.hu/?p=105', 7, 'nav_menu_item', '', 0),
(106, 1, '2020-09-15 14:01:14', '2020-09-15 12:01:14', '', 'Rólunk', '', 'publish', 'closed', 'closed', '', 'rolunk', '', '', '2020-09-18 12:36:34', '2020-09-18 10:36:34', '', 0, 'http://localhost/mokk-aali.hu/?p=106', 2, 'nav_menu_item', '', 0),
(107, 1, '2020-09-15 14:01:14', '2020-09-15 12:01:14', '', 'Munkatársaink', '', 'publish', 'closed', 'closed', '', 'munkatarsaink', '', '', '2020-09-18 12:36:34', '2020-09-18 10:36:34', '', 0, 'http://localhost/mokk-aali.hu/?p=107', 3, 'nav_menu_item', '', 0),
(109, 1, '2020-09-15 14:01:14', '2020-09-15 12:01:14', '', 'Nemzetközi Kapcsolatok', '', 'publish', 'closed', 'closed', '', 'nemzetkozi-kapcsolatok', '', '', '2020-09-18 12:36:34', '2020-09-18 10:36:34', '', 0, 'http://localhost/mokk-aali.hu/?p=109', 6, 'nav_menu_item', '', 0),
(110, 1, '2020-09-15 14:03:54', '2020-09-15 12:03:54', ' ', '', '', 'publish', 'closed', 'closed', '', '110', '', '', '2020-09-18 12:36:34', '2020-09-18 10:36:34', '', 0, 'http://localhost/mokk-aali.hu/?p=110', 1, 'nav_menu_item', '', 0),
(111, 1, '2020-09-15 14:46:01', '2020-09-15 12:46:01', '[newsletter]', 'Newsletter', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2020-09-15 14:46:01', '2020-09-15 12:46:01', '', 9, 'http://localhost/mokk-aali.hu/9-revision-v1/', 0, 'revision', '', 0),
(112, 1, '2020-09-15 14:54:01', '2020-09-15 12:54:01', '<h3>Üdvözlöm a Magyar Országos Közjegyzői Kamara (MOKK) Intézete Adatkutató Alintézetének weboldalán</h3>\r\nAz Alintézet létrehozásával a MOKK jelentős lépést tett a technológiai alkalmazkodás irányában azzal, hogy a közjegyzői hivatásrend belső folyamatainak mélyebb és kiterjedtebb megismerésére, és a hivatásrend fejlesztésére az eddig rendelkezésre állt eszközök mellett igénybe veszi a legmodernebb technológia által kínált lehetőségeket is.\r\n\r\nA világunkról szerzett tudást általában megfigyelésekre (mérésekre) alapozzuk. Ezek a mérések kezdetben ritkák és elnagyoltak voltak, majd egyre gyakoribbak lettek és mára már a folyamatainkhoz kapcsolt technológiai megoldások révén szinte elktépzelhetetlenül sűrűvé váltak. Ugyanakkor lehetővévált a mérések által gyűjtött hatalmas mennyiségű, nagyrészt rendezetlen adat tárolása. Míg korábban a ritkább megfigyelési pontok mellett könnyebb volt szem előtt tartani a vizsgált jelenség egészét, mostanra a sokkal részletesebb megfigyelés mellett elveszhet az “összkép”. Emellett a humán rendszerek, benne a jog és a közjegyzői eljárások növekvő összetettségük miatt ugynevezett Komplex rendszerekké fejlődtek, amelyek gyors tempóban fejlődnek tovább. A komplex rendszerek vizsgálatához pedig erre alkalmas szemléletet és új vizsgálati módszereket kell elsajátítanunk. Különben “Elveszítjük a fonalat”.\r\n\r\nA hagyományos szemlélet és a hagyományos módszerek mellett, emberi léptékkel mérve ma már képtelenség egyszerre figyelni az Egészre és a Részletekre. Különösen nehéz mindezt az időfejlődésre tekintettel a múltban és a jelenben egyszerre vizsgálni, nem is szólva a jövőre vonatkozó következtetésekről.\r\n\r\nA széles értelemben vett digitalizáció immár az életünk részévé vált. A különböző közjegyzői eljárásokegymás után lépnek át a kibertérbe, utat engedve az elektronikus ügyintézésnek, így ezek a folyamatokegyre nagyobb részletességgel tükröződnek egy rendezetlen digitális adathalmazban. Ez a “tükör” azonban már nem csupán a felszínt képes megmutatni hanem magát a tükröződő jelenséget is a maga egyre részletesebben kibontakozó valóságában. Ez pedig azt jelenti, hogy az eljárási folyamataink ma már sokkal nagyobb pontossággal mérhetők mint korábban bármikor voltak a történelmünk során, így összességükben nagyobb részletességgel válnak vizsgálhatóvá.\r\n\r\nManapság, a XXI. század 20. évben, a rendelkezésünkre álló technológia, a gépi tanulás különböző fajtái, a folyamatosan bővülő BigData és az ezekre épülő alkalmazások (MI) lehetővé teszik azt, hogymi, közjegyzők, a jogtudomány képviselőiként úgy vizsgáljuk és ismerjük meg a közjegyzői hivatást, a folyamataink összeségét, ahogyan pl. az orvostudomány képviselői képesek a legnagyobb részletességgel megvizsgálni és megismerni az emberi test egészét.\r\n\r\nMinden fejlesztés alapja a múlt és a jelen lehető legrészletesebb megismerése. A Magyar Országos Közjegyzői Kamara az Adatkutató Alintézet létrehozásával a rá vonatkozó törvényi keretek között a legmodernebb technológiát alkalmazva törekszik a hivatásrendi folyamatok minél részletesebb elemzésére és megismerésére a közjegyzői eljárások fejlesztése érdekében.\r\n\r\nA modern technológia felhasználásával rejtett, másként hozzá nem férhető mintázatok tárhatók fel, mint amilyenek pl. az egymástól távol eső eljárási területek folyamatainak összefüggései, amely felismerések hitem szerint a jövőben hozzá fognak járulni a hivatásrend megalapozott fejlesztéséhez.\r\n\r\n<strong>Dr. Parti Tamás</strong>\r\na MOKK Adatkutató Alintézetének vezetője', 'Köszöntő', '', 'publish', 'closed', 'closed', '', 'koszonto', '', '', '2020-09-18 08:34:16', '2020-09-18 06:34:16', '', 0, 'http://localhost/mokk-aali.hu/?page_id=112', 0, 'page', '', 0),
(113, 1, '2020-09-15 14:54:01', '2020-09-15 12:54:01', '<h3>Üdvözlöm a Magyar Országos Közjegyzői Kamara (MOKK) Intézete Adatkutató Alintézetének weboldalán</h3>\r\nAz Alintézet létrehozásával a MOKK jelentős lépést tett a technológiai alkalmazkodás irányában azzal, hogy a közjegyzői hivatásrend belső folyamatainak mélyebb és kiterjedtebb megismerésére, és a hivatásrend fejlesztésére az eddig rendelkezésre állt eszközök mellett igénybe veszi a legmodernebb technológia által kínált lehetőségeket is.\r\n\r\nA világunkról szerzett tudást általában megfigyelésekre (mérésekre) alapozzuk. Ezek a mérések kezdetben ritkák és elnagyoltak voltak, majd egyre gyakoribbak lettek és mára már a folyamatainkhoz kapcsolt technológiai megoldások révén szinte elktépzelhetetlenül sűrűvé váltak. Ugyanakkor lehetővévált a mérések által gyűjtött hatalmas mennyiségű, nagyrészt rendezetlen adat tárolása. Míg korábban a ritkább megfigyelési pontok mellett könnyebb volt szem előtt tartani a vizsgált jelenség egészét, mostanra a sokkal részletesebb megfigyelés mellett elveszhet az “összkép”. Emellett a humán rendszerek, benne a jog és a közjegyzői eljárások növekvő összetettségük miatt ugynevezett Komplex rendszerekké fejlődtek, amelyek gyors tempóban fejlődnek tovább. A komplex rendszerek vizsgálatához pedig erre alkalmas szemléletet és új vizsgálati módszereket kell elsajátítanunk. Különben “Elveszítjük a fonalat”.\r\n\r\nA hagyományos szemlélet és a hagyományos módszerek mellett, emberi léptékkel mérve ma már képtelenség egyszerre figyelni az Egészre és a Részletekre. Különösen nehéz mindezt az időfejlődésre tekintettel a múltban és a jelenben egyszerre vizsgálni, nem is szólva a jövőre vonatkozó következtetésekről.\r\n\r\nA széles értelemben vett digitalizáció immár az életünk részévé vált. A különböző közjegyzői eljárásokegymás után lépnek át a kibertérbe, utat engedve az elektronikus ügyintézésnek, így ezek a folyamatokegyre nagyobb részletességgel tükröződnek egy rendezetlen digitális adathalmazban. Ez a “tükör” azonban már nem csupán a felszínt képes megmutatni hanem magát a tükröződő jelenséget is a maga egyre részletesebben kibontakozó valóságában. Ez pedig azt jelenti, hogy az eljárási folyamataink ma már sokkal nagyobb pontossággal mérhetők mint korábban bármikor voltak a történelmünk során, így összességükben nagyobb részletességgel válnak vizsgálhatóvá.\r\n\r\nManapság, a XXI. század 20. évben, a rendelkezésünkre álló technológia, a gépi tanulás különböző fajtái, a folyamatosan bővülő BigData és az ezekre épülő alkalmazások (MI) lehetővé teszik azt, hogymi, közjegyzők, a jogtudomány képviselőiként úgy vizsgáljuk és ismerjük meg a közjegyzői hivatást, a folyamataink összeségét, ahogyan pl. az orvostudomány képviselői képesek a legnagyobb részletességgel megvizsgálni és megismerni az emberi test egészét.\r\n\r\nMinden fejlesztés alapja a múlt és a jelen lehető legrészletesebb megismerése. A Magyar Országos Közjegyzői Kamara az Adatkutató Alintézet létrehozásával a rá vonatkozó törvényi keretek között a legmodernebb technológiát alkalmazva törekszik a hivatásrendi folyamatok minél részletesebb elemzésére és megismerésére a közjegyzői eljárások fejlesztése érdekében.\r\n\r\nA modern technológia felhasználásával rejtett, másként hozzá nem férhető mintázatok tárhatók fel, mint amilyenek pl. az egymástól távol eső eljárási területek folyamatainak összefüggései, amely felismerések hitem szerint a jövőben hozzá fognak járulni a hivatásrend megalapozott fejlesztéséhez.\r\n\r\n<strong>Dr. Parti Tamás</strong>\r\na MOKK Adatkutató Alintézetének vezetője', 'Köszöntő', '', 'inherit', 'closed', 'closed', '', '112-revision-v1', '', '', '2020-09-15 14:54:01', '2020-09-15 12:54:01', '', 112, 'http://localhost/mokk-aali.hu/112-revision-v1/', 0, 'revision', '', 0),
(114, 1, '2020-09-15 15:03:54', '2020-09-15 13:03:54', '', 'parti-tamas-img', '', 'inherit', 'open', 'closed', '', 'parti-tamas-img', '', '', '2020-09-15 15:03:54', '2020-09-15 13:03:54', '', 112, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/parti-tamas-img.jpg', 0, 'attachment', 'image/jpeg', 0),
(115, 1, '2020-09-15 15:43:04', '2020-09-15 13:43:04', '', 'Publikációink', '', 'trash', 'closed', 'closed', '', 'publikacioink__trashed', '', '', '2020-09-18 08:42:43', '2020-09-18 06:42:43', '', 0, 'http://localhost/mokk-aali.hu/?page_id=115', 0, 'page', '', 0),
(116, 1, '2020-09-15 15:43:04', '2020-09-15 13:43:04', '', 'Publikációk', '', 'inherit', 'closed', 'closed', '', '115-revision-v1', '', '', '2020-09-15 15:43:04', '2020-09-15 13:43:04', '', 115, 'http://localhost/mokk-aali.hu/115-revision-v1/', 0, 'revision', '', 0),
(117, 1, '2020-09-15 16:08:51', '2020-09-15 14:08:51', '', 'Publikációink', '', 'inherit', 'closed', 'closed', '', '115-revision-v1', '', '', '2020-09-15 16:08:51', '2020-09-15 14:08:51', '', 115, 'http://localhost/mokk-aali.hu/115-revision-v1/', 0, 'revision', '', 0),
(118, 1, '2020-09-15 16:08:59', '2020-09-15 14:08:59', '', 'Hírek és Események', '', 'publish', 'closed', 'closed', '', 'hirek-es-esemenyek', '', '', '2020-09-15 16:08:59', '2020-09-15 14:08:59', '', 0, 'http://localhost/mokk-aali.hu/?page_id=118', 0, 'page', '', 0),
(119, 1, '2020-09-15 16:08:59', '2020-09-15 14:08:59', '', 'Hírek és Események', '', 'inherit', 'closed', 'closed', '', '118-revision-v1', '', '', '2020-09-15 16:08:59', '2020-09-15 14:08:59', '', 118, 'http://localhost/mokk-aali.hu/118-revision-v1/', 0, 'revision', '', 0),
(120, 1, '2020-09-15 16:09:29', '2020-09-15 14:09:29', '{{unknown}}', '', '', 'publish', 'closed', 'closed', '', '7f0de3f479c81f5c6510af51edf5bbb9', '', '', '2020-09-15 16:09:29', '2020-09-15 14:09:29', '', 0, 'http://localhost/mokk-aali.hu/7f0de3f479c81f5c6510af51edf5bbb9/', 0, 'oembed_cache', '', 0),
(121, 1, '2020-09-15 16:09:29', '2020-09-15 14:09:29', '{{unknown}}', '', '', 'publish', 'closed', 'closed', '', '951794e709cb8a6c7feb965371d1d8bf', '', '', '2020-09-15 16:09:29', '2020-09-15 14:09:29', '', 0, 'http://localhost/mokk-aali.hu/951794e709cb8a6c7feb965371d1d8bf/', 0, 'oembed_cache', '', 0),
(122, 1, '2020-09-18 08:31:30', '0000-00-00 00:00:00', '', 'Automatikus vázlat', '', 'auto-draft', 'open', 'open', '', '', '', '', '2020-09-18 08:31:30', '0000-00-00 00:00:00', '', 0, 'http://localhost/mokk-aali.hu/?p=122', 0, 'post', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(123, 1, '2020-09-18 08:32:13', '2020-09-18 06:32:13', ' ', '', '', 'publish', 'closed', 'closed', '', 'esemenyek-2', '', '', '2020-09-18 12:36:34', '2020-09-18 10:36:34', '', 0, 'http://localhost/mokk-aali.hu/?p=123', 5, 'nav_menu_item', '', 0),
(124, 1, '2020-09-18 08:41:58', '2020-09-18 06:41:58', '', 'Publikációink', '', 'inherit', 'closed', 'closed', '', '42-revision-v1', '', '', '2020-09-18 08:41:58', '2020-09-18 06:41:58', '', 42, 'http://localhost/mokk-aali.hu/42-revision-v1/', 0, 'revision', '', 0),
(125, 1, '2020-09-18 08:48:05', '2020-09-18 06:48:05', '{\n    \"page_for_posts\": {\n        \"value\": \"0\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-09-18 06:48:05\"\n    }\n}', '', '', 'publish', 'closed', 'closed', '', 'ad76cc06-1286-4016-9947-871f118bc262', '', '', '2020-09-18 08:48:05', '2020-09-18 06:48:05', '', 0, 'http://localhost/mokk-aali.hu/ad76cc06-1286-4016-9947-871f118bc262/', 0, 'customize_changeset', '', 0),
(126, 1, '2020-09-18 08:50:24', '2020-09-18 06:50:24', '{\n    \"page_for_posts\": {\n        \"value\": \"42\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-09-18 06:50:24\"\n    }\n}', '', '', 'publish', 'closed', 'closed', '', 'c9cccb87-0ed4-432a-a511-62e692e0433c', '', '', '2020-09-18 08:50:24', '2020-09-18 06:50:24', '', 0, 'http://localhost/mokk-aali.hu/c9cccb87-0ed4-432a-a511-62e692e0433c/', 0, 'customize_changeset', '', 0),
(127, 1, '2020-09-18 08:59:00', '2020-09-18 06:59:00', ' ', '', '', 'publish', 'closed', 'closed', '', '127', '', '', '2020-09-18 12:36:34', '2020-09-18 10:36:34', '', 0, 'http://localhost/mokk-aali.hu/?p=127', 4, 'nav_menu_item', '', 0);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_smush_dir_images`
--

CREATE TABLE `wp_smush_dir_images` (
  `id` mediumint(9) NOT NULL,
  `path` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `path_hash` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `resize` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lossy` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `error` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_size` int(10) UNSIGNED DEFAULT NULL,
  `orig_size` int(10) UNSIGNED DEFAULT NULL,
  `file_time` int(10) UNSIGNED DEFAULT NULL,
  `last_scan` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `meta` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Egyéb', 'egyeb', 0),
(2, 'Main menu', 'main-menu', 0),
(4, 'Kiemelt', 'kiemelt', 0),
(5, 'Tudomány', 'tudomany', 0),
(6, 'Technológia', 'technologia', 0);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_order` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(11, 1, 0),
(12, 1, 0),
(51, 1, 0),
(52, 1, 0),
(53, 1, 0),
(96, 1, 0),
(96, 4, 0),
(99, 4, 0),
(99, 5, 0),
(99, 6, 0),
(105, 2, 0),
(106, 2, 0),
(107, 2, 0),
(109, 2, 0),
(110, 2, 0),
(123, 2, 0),
(127, 2, 0);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 0),
(2, 2, 'nav_menu', '', 0, 7),
(4, 4, 'post_tag', '', 0, 1),
(5, 5, 'category', '', 0, 1),
(6, 6, 'category', '', 0, 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'fresh'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'true'),
(10, 1, 'locale', ''),
(11, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(12, 1, 'wp_user_level', '10'),
(13, 1, 'dismissed_wp_pointers', 'wp496_privacy'),
(14, 1, 'show_welcome_panel', '0'),
(16, 1, 'wp_dashboard_quick_press_last_post_id', '122'),
(17, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:2:\"::\";}'),
(19, 1, '_yoast_wpseo_profile_updated', '1548620464'),
(20, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(21, 1, 'metaboxhidden_nav-menus', 'a:5:{i:0;s:23:\"add-post-type-portfolio\";i:1;s:12:\"add-post_tag\";i:2;s:15:\"add-post_format\";i:3;s:17:\"add-portfolio-cat\";i:4;s:17:\"add-portfolio-tag\";}'),
(22, 1, 'wbcr_clearfy_hidden_notices', 'a:2:{s:65:\"35cc128cded936867f1217fc156cadf6_60cff2f1de6f2d36f6a38932b8d702f6\";s:137:\"Az új ThemePlate verzió már elérhető.  A legfrissebb 3.2.1 változat részletei itt tekinthetőek meg, vagy automatikus frissítés.\";s:65:\"73c6ed5c28b46cd800ea1cb82de8949b_cb1ec3fc7d15a1e7b961766658c9f956\";s:216:\"The following recommended plugin is currently inactive: W3 Total Cache.\nThere are updates available for the following plugins: Enable Media Replace and W3 Total Cache.\nBegin updating plugins | Begin activating plugin\";}'),
(23, 1, 'session_tokens', 'a:1:{s:64:\"6ecdf5acdf5024341928d5a46c5243c40f9f4075c2ba95d98d78c2fcedc69740\";a:4:{s:10:\"expiration\";i:1600866821;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36\";s:5:\"login\";i:1600694021;}}'),
(24, 1, 'wp_yoast_notifications', 'a:2:{i:0;a:2:{s:7:\"message\";s:639:\"<p>As you can see, there is a translation of this plugin in Hungarian. This translation is currently 89% complete. We need your help to make it complete and to fix any errors. Please register at <a href=\"https://translate.wordpress.org/projects/wp-plugins/wordpress-seo/\">Translating WordPress</a> to help complete the translation to Hungarian!</p><p><a href=\"https://translate.wordpress.org/projects/wp-plugins/wordpress-seo/\">Register now &raquo;</a></p><a class=\"button\" href=\"/mokk-aali.hu/wp-admin/admin.php?page=wpseo_titles&#038;settings-updated=true&#038;remove_i18n_promo=1\">Please don&#039;t show me this notification anymore</a>\";s:7:\"options\";a:10:{s:4:\"type\";s:7:\"warning\";s:2:\"id\";s:31:\"i18nModuleTranslationAssistance\";s:4:\"user\";O:7:\"WP_User\":8:{s:4:\"data\";O:8:\"stdClass\":10:{s:2:\"ID\";s:1:\"1\";s:10:\"user_login\";s:5:\"admin\";s:9:\"user_pass\";s:34:\"$P$BdjPfXY1XfOcmfwmZksyICO9L.9iEo0\";s:13:\"user_nicename\";s:5:\"admin\";s:10:\"user_email\";s:18:\"doxa84@hotmail.com\";s:8:\"user_url\";s:0:\"\";s:15:\"user_registered\";s:19:\"2017-10-31 08:28:28\";s:19:\"user_activation_key\";s:0:\"\";s:11:\"user_status\";s:1:\"0\";s:12:\"display_name\";s:5:\"admin\";}s:2:\"ID\";i:1;s:4:\"caps\";a:1:{s:13:\"administrator\";b:1;}s:7:\"cap_key\";s:15:\"wp_capabilities\";s:5:\"roles\";a:1:{i:0;s:13:\"administrator\";}s:7:\"allcaps\";a:65:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:20:\"wpseo_manage_options\";b:1;s:12:\"cfdb7_access\";b:1;s:10:\"loco_admin\";b:1;s:13:\"administrator\";b:1;}s:6:\"filter\";N;s:16:\"\0WP_User\0site_id\";i:1;}s:5:\"nonce\";N;s:8:\"priority\";d:0.5;s:9:\"data_json\";a:0:{}s:13:\"dismissal_key\";N;s:12:\"capabilities\";a:1:{i:0;s:20:\"wpseo_manage_options\";}s:16:\"capability_check\";s:3:\"all\";s:14:\"yoast_branding\";b:0;}}i:1;a:2:{s:7:\"message\";s:372:\"<strong>New in Yoast SEO 14.9: </strong>We now have Hebrew keyphrase recognition and some great performance improvements! <a href=\"https://yoa.st/yoast14-9?php_version=7.3&#038;platform=wordpress&#038;platform_version=5.5.1&#038;software=free&#038;software_version=14.9&#038;days_active=30plus&#038;user_language=hu_HU\" target=\"_blank\">Read all about version 14.9 here</a>\";s:7:\"options\";a:10:{s:4:\"type\";s:7:\"updated\";s:2:\"id\";s:20:\"wpseo-plugin-updated\";s:4:\"user\";O:7:\"WP_User\":8:{s:4:\"data\";O:8:\"stdClass\":10:{s:2:\"ID\";s:1:\"1\";s:10:\"user_login\";s:5:\"admin\";s:9:\"user_pass\";s:34:\"$P$BdjPfXY1XfOcmfwmZksyICO9L.9iEo0\";s:13:\"user_nicename\";s:5:\"admin\";s:10:\"user_email\";s:18:\"doxa84@hotmail.com\";s:8:\"user_url\";s:0:\"\";s:15:\"user_registered\";s:19:\"2017-10-31 08:28:28\";s:19:\"user_activation_key\";s:0:\"\";s:11:\"user_status\";s:1:\"0\";s:12:\"display_name\";s:5:\"admin\";}s:2:\"ID\";i:1;s:4:\"caps\";a:1:{s:13:\"administrator\";b:1;}s:7:\"cap_key\";s:15:\"wp_capabilities\";s:5:\"roles\";a:1:{i:0;s:13:\"administrator\";}s:7:\"allcaps\";a:65:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:20:\"wpseo_manage_options\";b:1;s:12:\"cfdb7_access\";b:1;s:10:\"loco_admin\";b:1;s:13:\"administrator\";b:1;}s:6:\"filter\";N;s:16:\"\0WP_User\0site_id\";i:1;}s:5:\"nonce\";N;s:8:\"priority\";d:0.5;s:9:\"data_json\";a:1:{s:13:\"dismiss_value\";s:4:\"14.9\";}s:13:\"dismissal_key\";s:20:\"wpseo-plugin-updated\";s:12:\"capabilities\";a:1:{i:0;s:20:\"wpseo_manage_options\";}s:16:\"capability_check\";s:3:\"all\";s:14:\"yoast_branding\";b:0;}}}'),
(25, 1, 'nav_menu_recently_edited', '2'),
(26, 1, 'closedpostboxes_dashboard', 'a:0:{}'),
(27, 1, 'metaboxhidden_dashboard', 'a:5:{i:0;s:19:\"dashboard_right_now\";i:1;s:18:\"dashboard_activity\";i:2;s:24:\"wpseo-dashboard-overview\";i:3;s:21:\"dashboard_quick_press\";i:4;s:17:\"dashboard_primary\";}'),
(28, 1, 'wp_user-settings', 'libraryContent=browse'),
(29, 1, 'wp_user-settings-time', '1583846095'),
(30, 1, 'closedpostboxes_our_team', 'a:2:{i:0;s:10:\"wpseo_meta\";i:1;s:27:\"themeplate_themeplate_field\";}'),
(31, 1, 'metaboxhidden_our_team', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(32, 1, 'meta-box-order_our_team', 'a:5:{s:15:\"acf_after_title\";s:0:\"\";s:11:\"after_title\";s:0:\"\";s:4:\"side\";s:22:\"submitdiv,postimagediv\";s:6:\"normal\";s:42:\"acf-group_5e6a022d542c0,wpseo_meta,slugdiv\";s:8:\"advanced\";s:27:\"themeplate_themeplate_field\";}'),
(33, 1, 'screen_layout_our_team', '2'),
(34, 1, 'closedpostboxes_testimonial', 'a:2:{i:0;s:10:\"wpseo_meta\";i:1;s:27:\"themeplate_themeplate_field\";}'),
(35, 1, 'metaboxhidden_testimonial', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(36, 1, 'meta-box-order_testimonial', 'a:5:{s:15:\"acf_after_title\";s:0:\"\";s:11:\"after_title\";s:0:\"\";s:4:\"side\";s:9:\"submitdiv\";s:6:\"normal\";s:42:\"acf-group_5e6a096c84b2f,wpseo_meta,slugdiv\";s:8:\"advanced\";s:27:\"themeplate_themeplate_field\";}'),
(37, 1, 'screen_layout_testimonial', '2'),
(38, 1, 'closedpostboxes_partner', 'a:1:{i:0;s:10:\"wpseo_meta\";}'),
(39, 1, 'metaboxhidden_partner', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(40, 1, 'meta-box-order_partner', 'a:4:{s:15:\"acf_after_title\";s:0:\"\";s:4:\"side\";s:9:\"submitdiv\";s:6:\"normal\";s:18:\"wpseo_meta,slugdiv\";s:8:\"advanced\";s:0:\"\";}'),
(41, 1, 'screen_layout_partner', '2'),
(42, 1, 'closedpostboxes_publications', 'a:1:{i:0;s:10:\"wpseo_meta\";}'),
(43, 1, 'metaboxhidden_publications', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(44, 1, 'meta-box-order_publications', 'a:4:{s:15:\"acf_after_title\";s:0:\"\";s:4:\"side\";s:34:\"submitdiv,categorydiv,postimagediv\";s:6:\"normal\";s:42:\"acf-group_5f5f39f53f6ed,wpseo_meta,slugdiv\";s:8:\"advanced\";s:0:\"\";}'),
(45, 1, 'screen_layout_publications', '2'),
(46, 1, 'closedpostboxes_int_contacts', 'a:1:{i:0;s:10:\"wpseo_meta\";}'),
(47, 1, 'metaboxhidden_int_contacts', 'a:1:{i:0;s:7:\"slugdiv\";}');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT 0,
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$BdjPfXY1XfOcmfwmZksyICO9L.9iEo0', 'admin', 'doxa84@hotmail.com', '', '2017-10-31 08:28:28', '', 0, 'admin');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_yoast_indexable`
--

CREATE TABLE `wp_yoast_indexable` (
  `id` int(11) UNSIGNED NOT NULL,
  `permalink` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permalink_hash` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `object_id` int(11) UNSIGNED DEFAULT NULL,
  `object_type` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `object_sub_type` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `author_id` int(11) UNSIGNED DEFAULT NULL,
  `post_parent` int(11) UNSIGNED DEFAULT NULL,
  `title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `breadcrumb_title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_public` tinyint(1) DEFAULT NULL,
  `is_protected` tinyint(1) DEFAULT 0,
  `has_public_posts` tinyint(1) DEFAULT NULL,
  `number_of_pages` int(11) UNSIGNED DEFAULT NULL,
  `canonical` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `primary_focus_keyword` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `primary_focus_keyword_score` int(3) DEFAULT NULL,
  `readability_score` int(3) DEFAULT NULL,
  `is_cornerstone` tinyint(1) DEFAULT 0,
  `is_robots_noindex` tinyint(1) DEFAULT 0,
  `is_robots_nofollow` tinyint(1) DEFAULT 0,
  `is_robots_noarchive` tinyint(1) DEFAULT 0,
  `is_robots_noimageindex` tinyint(1) DEFAULT 0,
  `is_robots_nosnippet` tinyint(1) DEFAULT 0,
  `twitter_title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_image` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_image_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_image_source` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `open_graph_title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `open_graph_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `open_graph_image` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `open_graph_image_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `open_graph_image_source` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `open_graph_image_meta` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_count` int(11) DEFAULT NULL,
  `incoming_link_count` int(11) DEFAULT NULL,
  `prominent_words_version` int(11) UNSIGNED DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `blog_id` bigint(20) NOT NULL DEFAULT 1,
  `language` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `schema_page_type` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `schema_article_type` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `has_ancestors` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `wp_yoast_indexable`
--

INSERT INTO `wp_yoast_indexable` (`id`, `permalink`, `permalink_hash`, `object_id`, `object_type`, `object_sub_type`, `author_id`, `post_parent`, `title`, `description`, `breadcrumb_title`, `post_status`, `is_public`, `is_protected`, `has_public_posts`, `number_of_pages`, `canonical`, `primary_focus_keyword`, `primary_focus_keyword_score`, `readability_score`, `is_cornerstone`, `is_robots_noindex`, `is_robots_nofollow`, `is_robots_noarchive`, `is_robots_noimageindex`, `is_robots_nosnippet`, `twitter_title`, `twitter_image`, `twitter_description`, `twitter_image_id`, `twitter_image_source`, `open_graph_title`, `open_graph_description`, `open_graph_image`, `open_graph_image_id`, `open_graph_image_source`, `open_graph_image_meta`, `link_count`, `incoming_link_count`, `prominent_words_version`, `created_at`, `updated_at`, `blog_id`, `language`, `region`, `schema_page_type`, `schema_article_type`, `has_ancestors`) VALUES
(1, NULL, NULL, NULL, 'system-page', '404', NULL, NULL, 'Page not found %%sep%% %%sitename%%', NULL, 'Error 404: Page not found', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-09 12:37:28', '2020-09-09 10:37:28', 1, NULL, NULL, NULL, NULL, 0),
(2, 'http://localhost/mokk-aali.hu/', '30:e3cab587f06a568804c9372cee79ab87', NULL, 'home-page', NULL, NULL, NULL, '%%sitename%% %%page%% %%sep%% %%sitedesc%%', '', 'Home', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '2020-09-09 12:54:11', '2020-09-09 10:54:11', 1, NULL, NULL, NULL, NULL, 0),
(3, 'http://localhost/mokk-aali.hu/author/admin/', '43:5607d89b11aad8b52364d64572338bde', 1, 'user', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 'https://secure.gravatar.com/avatar/5cfb74386c805526089e57863c520eb7?s=500&d=mm&r=g', NULL, NULL, 'gravatar-image', NULL, NULL, 'https://secure.gravatar.com/avatar/5cfb74386c805526089e57863c520eb7?s=500&d=mm&r=g', NULL, 'gravatar-image', NULL, NULL, NULL, NULL, '2020-09-09 12:54:23', '2020-09-18 08:36:34', 1, NULL, NULL, NULL, NULL, 0),
(5, 'http://localhost/mokk-aali.hu/blog-post-6__trashed/', '51:69dc45cb21eb3ecabed993077d7d8112', 53, 'post', 'post', 1, 0, NULL, NULL, 'BLOG POST 6', 'trash', 0, 0, NULL, NULL, NULL, NULL, NULL, 90, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/placeholder-img-3.jpg', NULL, '13', 'featured-image', NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/placeholder-img-3.jpg', '13', 'featured-image', '{\"width\":800,\"height\":600,\"url\":\"http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/placeholder-img-3.jpg\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\mokk-aali.hu/wp-content/uploads/2020/03/placeholder-img-3.jpg\",\"size\":\"full\",\"id\":13,\"alt\":\"\",\"pixels\":480000,\"type\":\"image/jpeg\"}', 0, NULL, NULL, '2020-09-09 12:55:24', '2020-09-15 07:03:15', 1, NULL, NULL, NULL, NULL, 0),
(6, 'http://localhost/mokk-aali.hu/blog-post-5__trashed/', '51:cfa738747b0c2d59900b8232449337fd', 52, 'post', 'post', 1, 0, NULL, NULL, 'BLOG POST 5', 'trash', 0, 0, NULL, NULL, NULL, NULL, NULL, 90, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/placeholder-img-3.jpg', NULL, '13', 'featured-image', NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/placeholder-img-3.jpg', '13', 'featured-image', '{\"width\":800,\"height\":600,\"url\":\"http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/placeholder-img-3.jpg\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\mokk-aali.hu/wp-content/uploads/2020/03/placeholder-img-3.jpg\",\"size\":\"full\",\"id\":13,\"alt\":\"\",\"pixels\":480000,\"type\":\"image/jpeg\"}', 0, NULL, NULL, '2020-09-09 12:55:24', '2020-09-15 07:03:15', 1, NULL, NULL, NULL, NULL, 0),
(7, 'http://localhost/mokk-aali.hu/blog-post-4__trashed/', '51:34d4e05d0b8bdd79b341df6767cd7a7a', 51, 'post', 'post', 1, 0, NULL, NULL, 'BLOG POST 4', 'trash', 0, 0, NULL, NULL, NULL, NULL, NULL, 90, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/placeholder-img-3.jpg', NULL, '13', 'featured-image', NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/placeholder-img-3.jpg', '13', 'featured-image', '{\"width\":800,\"height\":600,\"url\":\"http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/placeholder-img-3.jpg\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\mokk-aali.hu/wp-content/uploads/2020/03/placeholder-img-3.jpg\",\"size\":\"full\",\"id\":13,\"alt\":\"\",\"pixels\":480000,\"type\":\"image/jpeg\"}', 0, NULL, NULL, '2020-09-09 12:55:24', '2020-09-15 07:03:15', 1, NULL, NULL, NULL, NULL, 0),
(8, 'https://localhost/mokk-aali.hu/', '31:a67121e6bb07cd2637d18fd8cb54e248', 2, 'post', 'page', 1, 0, NULL, NULL, 'Főoldal', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-09 13:39:07', '2020-09-09 11:39:07', 1, NULL, NULL, NULL, NULL, 0),
(10, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/mokk-adatkutatoalintezet-logo-small.png', '96:540d16df7f0060685eaf28b73f1aad72', 78, 'post', 'attachment', 1, 0, NULL, NULL, 'mokk-adatkutatoalintezet-logo-small', 'inherit', 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/mokk-adatkutatoalintezet-logo-small.png', NULL, '78', 'attachment-image', NULL, NULL, NULL, '78', 'attachment-image', NULL, 0, NULL, NULL, '2020-09-11 10:03:39', '2020-09-11 08:03:39', 1, NULL, NULL, NULL, NULL, 0),
(11, 'http://localhost/mokk-aali.hu/5470514ddfbc3e69add646876360f311/', '63:3bbe88023f62e03e5938efe01551dec3', 80, 'post', 'oembed_cache', 1, 0, NULL, NULL, '', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-11 10:03:42', '2020-09-11 08:03:42', 1, NULL, NULL, NULL, NULL, 0),
(12, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/cropped-mokk-adatkutatoalintezet-logo-small.png', '104:d85dcee91b2d413294ddf287c51863b5', 79, 'post', 'attachment', 1, 0, NULL, NULL, 'cropped-mokk-adatkutatoalintezet-logo-small.png', 'inherit', 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/cropped-mokk-adatkutatoalintezet-logo-small.png', NULL, '79', 'attachment-image', NULL, NULL, NULL, '79', 'attachment-image', NULL, 0, NULL, NULL, '2020-09-11 10:03:42', '2020-09-11 08:03:42', 1, NULL, NULL, NULL, NULL, 0),
(13, 'http://localhost/mokk-aali.hu/99cda8b6-ad56-4adf-ab97-d27ec25cb6fd/', '67:6e238bcb1f7bc82f59c56fbae4ef617a', 81, 'post', 'customize_changeset', 1, 0, NULL, NULL, '', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-11 10:04:04', '2020-09-11 08:04:04', 1, NULL, NULL, NULL, NULL, 0),
(14, 'http://localhost/mokk-aali.hu/blog-post-3__trashed/', '51:6dabf359b00753cd2865f9aab0253aac', 11, 'post', 'post', 1, 0, NULL, NULL, 'BLOG POST 3', 'trash', 0, 0, NULL, NULL, NULL, NULL, NULL, 90, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/placeholder-img-3.jpg', NULL, '13', 'featured-image', NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/placeholder-img-3.jpg', '13', 'featured-image', '{\"width\":800,\"height\":600,\"url\":\"http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/placeholder-img-3.jpg\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\mokk-aali.hu/wp-content/uploads/2020/03/placeholder-img-3.jpg\",\"size\":\"full\",\"id\":13,\"alt\":\"\",\"pixels\":480000,\"type\":\"image/jpeg\"}', 0, NULL, NULL, '2020-09-14 09:31:51', '2020-09-15 07:03:15', 1, NULL, NULL, NULL, NULL, 0),
(15, 'http://localhost/mokk-aali.hu/blog-post-2__trashed/', '51:0074db72f4befecd23276bf5aa1a8719', 12, 'post', 'post', 1, 0, NULL, NULL, 'BLOG POST 2', 'trash', 0, 0, NULL, NULL, NULL, NULL, NULL, 90, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/placeholder-img-3.jpg', NULL, '13', 'featured-image', NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/placeholder-img-3.jpg', '13', 'featured-image', '{\"width\":800,\"height\":600,\"url\":\"http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/placeholder-img-3.jpg\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\mokk-aali.hu/wp-content/uploads/2020/03/placeholder-img-3.jpg\",\"size\":\"full\",\"id\":13,\"alt\":\"\",\"pixels\":480000,\"type\":\"image/jpeg\"}', 0, NULL, NULL, '2020-09-14 09:31:51', '2020-09-15 07:03:15', 1, NULL, NULL, NULL, NULL, 0),
(16, 'http://localhost/mokk-aali.hu/hello-vilag__trashed/', '51:1120af07f713a943649b746ae57d83ac', 1, 'post', 'post', 1, 0, NULL, NULL, 'BLOG POST 1', 'trash', 0, 0, NULL, NULL, NULL, NULL, NULL, 90, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/placeholder-img-3.jpg', NULL, '13', 'featured-image', NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/placeholder-img-3.jpg', '13', 'featured-image', '{\"width\":800,\"height\":600,\"url\":\"http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/placeholder-img-3.jpg\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\mokk-aali.hu/wp-content/uploads/2020/03/placeholder-img-3.jpg\",\"size\":\"full\",\"id\":13,\"alt\":\"\",\"pixels\":480000,\"type\":\"image/jpeg\"}', 0, NULL, NULL, '2020-09-14 09:31:51', '2020-09-15 07:03:15', 1, NULL, NULL, NULL, NULL, 0),
(17, 'http://localhost/mokk-aali.hu/?post_type=acf-field-group&p=17', '61:ef99dd53cc300d06bb68cc300c63f9e5', 17, 'post', 'acf-field-group', 1, 0, NULL, NULL, 'Our Team', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-14 09:37:53', '2020-09-14 07:37:53', 1, NULL, NULL, NULL, NULL, 0),
(18, 'http://localhost/mokk-aali.hu/?post_type=acf-field-group&p=27', '61:821f4239a38e74e04cff5ca1fe505a15', 27, 'post', 'acf-field-group', 1, 0, NULL, NULL, 'Testimonial', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-14 09:37:53', '2020-09-14 07:37:53', 1, NULL, NULL, NULL, NULL, 0),
(19, 'http://localhost/mokk-aali.hu/?post_type=acf-field-group&p=82', '61:8496caccbb2041e80c33b918d0d15648', 82, 'post', 'acf-field-group', 1, 0, NULL, NULL, 'Publikáció', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-14 09:37:56', '2020-09-14 07:44:40', 1, NULL, NULL, NULL, NULL, 0),
(20, 'http://localhost/mokk-aali.hu/?post_type=acf-field&p=83', '55:765cf7d01848e9ce3080abb92aad28a2', 83, 'post', 'acf-field', 1, 82, NULL, NULL, 'Publikáció szerzője', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-14 09:38:59', '2020-09-14 07:44:40', 1, NULL, NULL, NULL, NULL, 1),
(21, 'http://localhost/mokk-aali.hu/?post_type=acf-field&p=84', '55:c879628757d6142edb23b46d1fa22b4b', 84, 'post', 'acf-field', 1, 82, NULL, NULL, 'Publikálás dátuma', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-14 09:38:59', '2020-09-14 07:44:40', 1, NULL, NULL, NULL, NULL, 1),
(22, 'http://localhost/mokk-aali.hu/publications/mi-es-mi-joggyakorlat-es-jogalkalmazas-halozati-szabalyozas-gondolatok-a-technologiai-alkalmazkodasrol/', '146:0c78918499406ec101fa90d265df67f0', 85, 'post', 'publications', 1, 0, NULL, NULL, 'MI és Mi &#8211; joggyakorlat és jogalkalmazás &#8211; hálózati szabályozás &#8211; Gondolatok a technológiai alkalmazkodásról', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 60, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/publikacio-cikk-1.jpg', NULL, '87', 'featured-image', NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/publikacio-cikk-1.jpg', '87', 'featured-image', '{\"width\":1069,\"height\":501,\"url\":\"http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/publikacio-cikk-1.jpg\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\mokk-aali.hu/wp-content/uploads/2020/09/publikacio-cikk-1.jpg\",\"size\":\"full\",\"id\":87,\"alt\":\"\",\"pixels\":535569,\"type\":\"image/jpeg\"}', 0, NULL, NULL, '2020-09-14 09:39:10', '2020-09-14 11:12:22', 1, NULL, NULL, NULL, NULL, 0),
(23, 'http://localhost/mokk-aali.hu/?post_type=acf-field&p=86', '55:e3f4e3134a289dbc88b1e8f70d40a17e', 86, 'post', 'acf-field', 1, 82, NULL, NULL, 'Publikáció megjelenési helye', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-14 09:44:40', '2020-09-14 07:44:40', 1, NULL, NULL, NULL, NULL, 1),
(24, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/publikacio-cikk-1.jpg', '78:e980001b864b6257868d56a143c91a55', 87, 'post', 'attachment', 1, 85, NULL, NULL, 'publikacio-cikk-1', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/publikacio-cikk-1.jpg', NULL, '87', 'attachment-image', NULL, NULL, NULL, '87', 'attachment-image', NULL, 0, NULL, NULL, '2020-09-14 12:54:54', '2020-09-14 10:54:54', 1, NULL, NULL, NULL, NULL, 1),
(25, 'http://localhost/mokk-aali.hu/publications/gondolatok-a-joggyakorlat-es-a-jogalkalmazas-technologiai-fejlodeshez-valo-alkalmazkodasarol/', '136:6d26b3f2bc204cf72dc15242a235970e', 88, 'post', 'publications', 1, 0, NULL, NULL, 'Gondolatok a joggyakorlat és a jogalkalmazás technológiai fejlődéshez való alkalmazkodásáról', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 60, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/publikacio-cikk-2.jpg', NULL, '89', 'featured-image', NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/publikacio-cikk-2.jpg', '89', 'featured-image', '{\"width\":1920,\"height\":1304,\"url\":\"http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/publikacio-cikk-2.jpg\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\mokk-aali.hu/wp-content/uploads/2020/09/publikacio-cikk-2.jpg\",\"size\":\"full\",\"id\":89,\"alt\":\"\",\"pixels\":2503680,\"type\":\"image/jpeg\"}', 0, NULL, NULL, '2020-09-14 15:00:07', '2020-09-14 13:04:25', 1, NULL, NULL, NULL, NULL, 0),
(26, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/publikacio-cikk-2.jpg', '78:67630922408ae454a5556fc62544a50f', 89, 'post', 'attachment', 1, 88, NULL, NULL, 'publikacio-cikk-2', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/publikacio-cikk-2.jpg', NULL, '89', 'attachment-image', NULL, NULL, NULL, '89', 'attachment-image', NULL, 0, NULL, NULL, '2020-09-14 15:02:51', '2020-09-14 15:03:14', 1, NULL, NULL, NULL, NULL, 1),
(27, 'http://localhost/mokk-aali.hu/publications/budapesti-futurologiai-forum-2016/', '77:54e245dce5c7fb251658bf124fb20f65', 90, 'post', 'publications', 1, 0, NULL, NULL, 'Budapesti Futurológiai Fórum 2016', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 60, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/publikacio-cikk-3.jpg', NULL, '91', 'featured-image', NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/publikacio-cikk-3.jpg', '91', 'featured-image', '{\"width\":1756,\"height\":769,\"url\":\"http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/publikacio-cikk-3.jpg\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\mokk-aali.hu/wp-content/uploads/2020/09/publikacio-cikk-3.jpg\",\"size\":\"full\",\"id\":91,\"alt\":\"\",\"pixels\":1350364,\"type\":\"image/jpeg\"}', 0, NULL, NULL, '2020-09-14 15:06:45', '2020-09-14 13:07:51', 1, NULL, NULL, NULL, NULL, 0),
(28, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/publikacio-cikk-3.jpg', '78:e2bbbcd9dd1917ec0d08dfc7824b3afb', 91, 'post', 'attachment', 1, 90, NULL, NULL, 'publikacio-cikk-3', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/publikacio-cikk-3.jpg', NULL, '91', 'attachment-image', NULL, NULL, NULL, '91', 'attachment-image', NULL, 0, NULL, NULL, '2020-09-14 15:07:40', '2020-09-14 15:07:51', 1, NULL, NULL, NULL, NULL, 1),
(29, 'http://localhost/mokk-aali.hu/int_contacts/europai-kozjegyzoi-halozat-enn-european-notarial-network-rne-reseau-notarial-europeen/', '129:1ec05866f5aa45b97dc6e8ee38d2eaad', 92, 'post', 'int_contacts', 1, 0, NULL, NULL, 'Európai Közjegyzői Hálózat (ENN-European Notarial Network- RNE-Réseau Notarial Européen)', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 60, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-15 06:34:05', '2020-09-15 04:34:31', 1, NULL, NULL, NULL, NULL, 0),
(30, 'http://localhost/mokk-aali.hu/int_contacts/francia-magyar-bizottsag-comite-franco-hongrois/', '91:856a1b7592609e120a3c88ad9a1cc079', 93, 'post', 'int_contacts', 1, 0, NULL, NULL, 'Francia-Magyar Bizottság (Comité franco-hongrois)', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 60, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-15 06:35:00', '2020-09-15 04:36:00', 1, NULL, NULL, NULL, NULL, 0),
(31, 'http://localhost/mokk-aali.hu/int_contacts/kozep-europai-kozjegyzoi-egyuttmukodes-hexagonale/', '93:6f2ea3f8c8d9870a96bbdccbbbc94d13', 94, 'post', 'int_contacts', 1, 0, NULL, NULL, 'Közép-Európai Közjegyzői Együttműködés (Hexagonale)', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 60, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-15 06:35:21', '2020-09-15 04:35:56', 1, NULL, NULL, NULL, NULL, 0),
(32, 'http://localhost/mokk-aali.hu/int_contacts/europai-kozjegyzoi-halozat-enn-european-notarial-network-rne-reseau-notarial-europeen-2/', '131:1ec860603c6979f83e21e0f916526b9a', 95, 'post', 'int_contacts', 1, 0, NULL, NULL, 'Európai Közjegyzői Hálózat (ENN-European Notarial Network-RNE-Réseau Notarial Européen', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 60, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-15 06:35:37', '2020-09-15 04:36:30', 1, NULL, NULL, NULL, NULL, 0),
(33, 'http://localhost/mokk-aali.hu/a-futurologiai-forum-2020-szeptember-14-en-online-kerul-megrendezesre__trashed/', '109:cd14e724170f0d361c74d8d0a377b32f', 96, 'post', 'post', 1, 0, NULL, NULL, 'A Futurológiai Fórum 2020 szeptember 14-én online kerül megrendezésre', 'trash', 0, 0, NULL, NULL, NULL, NULL, NULL, 60, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/esemeny-1.jpg', NULL, '97', 'featured-image', NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/esemeny-1.jpg', '97', 'featured-image', '{\"width\":1920,\"height\":1080,\"url\":\"http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/esemeny-1.jpg\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\mokk-aali.hu/wp-content/uploads/2020/09/esemeny-1.jpg\",\"size\":\"full\",\"id\":97,\"alt\":\"\",\"pixels\":2073600,\"type\":\"image/jpeg\"}', 0, NULL, NULL, '2020-09-15 09:03:19', '2020-09-15 07:14:15', 1, NULL, NULL, NULL, NULL, 0),
(35, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/esemeny-1.jpg', '70:c6c55d1f291f9d6799265dc7d30a236e', 97, 'post', 'attachment', 1, 96, NULL, NULL, 'esemeny-1', 'inherit', NULL, 0, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/esemeny-1.jpg', NULL, '97', 'attachment-image', NULL, NULL, NULL, '97', 'attachment-image', NULL, 0, NULL, NULL, '2020-09-15 09:04:40', '2020-09-15 09:14:15', 1, NULL, NULL, NULL, NULL, 1),
(36, 'http://localhost/mokk-aali.hu/tag/kiemelt/', '42:a848f50a8efeda1b46032c3fdd390440', 4, 'term', 'post_tag', NULL, NULL, NULL, NULL, 'Kiemelt', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-15 09:04:49', '2020-09-15 07:04:49', 1, NULL, NULL, NULL, NULL, 0),
(37, 'http://localhost/mokk-aali.hu/events/a-futurologiai-forum-2020-szeptember-14-en-online-kerul-megrendezesre/', '107:7be55616c48757967e18ae849d044344', 99, 'post', 'events', 1, 0, NULL, NULL, 'A Futurológiai Fórum 2020 szeptember 14-én online kerül megrendezésre', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 60, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/esemeny-1.jpg', NULL, '97', 'featured-image', NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/esemeny-1.jpg', '97', 'featured-image', '{\"width\":1920,\"height\":1080,\"url\":\"http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/esemeny-1.jpg\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\mokk-aali.hu/wp-content/uploads/2020/09/esemeny-1.jpg\",\"size\":\"full\",\"id\":97,\"alt\":\"\",\"pixels\":2073600,\"type\":\"image/jpeg\"}', 0, NULL, NULL, '2020-09-15 09:17:37', '2020-09-15 07:20:40', 1, NULL, NULL, NULL, NULL, 0),
(38, 'http://localhost/mokk-aali.hu/category/tudomany/', '48:fbb7b7e373738dabe7be47cec6db7774', 5, 'term', 'category', NULL, NULL, NULL, NULL, 'Tudomány', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-15 09:18:11', '2020-09-15 07:18:11', 1, NULL, NULL, NULL, NULL, 0),
(39, 'http://localhost/mokk-aali.hu/category/technologia/', '51:ea8a747a3792677c28570d4384b7e332', 6, 'term', 'category', NULL, NULL, NULL, NULL, 'Technológia', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-15 09:18:17', '2020-09-15 07:18:17', 1, NULL, NULL, NULL, NULL, 0),
(40, 'http://localhost/mokk-aali.hu/partner/partner-1/', '48:565b70f5e431d323fbcc6cfa96a3e597', 35, 'post', 'partner', 1, 0, NULL, NULL, 'KÁROLI GÁSPÁR REFORMÁTUS EGYETEM', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 30, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/KRE_logo_A4_magyar.png', NULL, '104', 'featured-image', NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/KRE_logo_A4_magyar.png', '104', 'featured-image', '{\"width\":414,\"height\":181,\"url\":\"http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/KRE_logo_A4_magyar.png\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\mokk-aali.hu/wp-content/uploads/2020/03/KRE_logo_A4_magyar.png\",\"size\":\"full\",\"id\":104,\"alt\":\"\",\"pixels\":74934,\"type\":\"image/png\"}', 0, NULL, NULL, '2020-09-15 11:29:01', '2020-09-15 09:40:06', 1, NULL, NULL, NULL, NULL, 0),
(41, 'http://localhost/mokk-aali.hu/partner/partner-2/', '48:9a91ab7206cebb90b746064b923d30fd', 38, 'post', 'partner', 1, 0, NULL, NULL, 'MOKK', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 30, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/mokk_logo.png', NULL, '101', 'featured-image', NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/mokk_logo.png', '101', 'featured-image', '{\"width\":191,\"height\":192,\"url\":\"http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/mokk_logo.png\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\mokk-aali.hu/wp-content/uploads/2020/03/mokk_logo.png\",\"size\":\"full\",\"id\":101,\"alt\":\"\",\"pixels\":36672,\"type\":\"image/png\"}', 0, NULL, NULL, '2020-09-15 11:29:01', '2020-09-15 09:34:23', 1, NULL, NULL, NULL, NULL, 0),
(42, 'http://localhost/mokk-aali.hu/partner/partner-3/', '48:2b41ba41a2fefc0577c42b66112f0230', 39, 'post', 'partner', 1, 0, NULL, NULL, 'Műegyetem', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 30, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/muegyetem.png', NULL, '102', 'featured-image', NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/muegyetem.png', '102', 'featured-image', '{\"width\":368,\"height\":98,\"url\":\"http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/muegyetem.png\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\mokk-aali.hu/wp-content/uploads/2020/03/muegyetem.png\",\"size\":\"full\",\"id\":102,\"alt\":\"\",\"pixels\":36064,\"type\":\"image/png\"}', 0, NULL, NULL, '2020-09-15 11:29:01', '2020-09-15 09:34:27', 1, NULL, NULL, NULL, NULL, 0),
(43, 'http://localhost/mokk-aali.hu/partner/partner-4/', '48:4ab48b6a7561f6947fdb081bdde32cfe', 40, 'post', 'partner', 1, 0, NULL, NULL, 'Kürt Akadémia', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 30, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/kurt-akademia.png', NULL, '103', 'featured-image', NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/kurt-akademia.png', '103', 'featured-image', '{\"width\":416,\"height\":133,\"url\":\"http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/kurt-akademia.png\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\mokk-aali.hu/wp-content/uploads/2020/03/kurt-akademia.png\",\"size\":\"full\",\"id\":103,\"alt\":\"\",\"pixels\":55328,\"type\":\"image/png\"}', 0, NULL, NULL, '2020-09-15 11:29:01', '2020-09-15 09:34:15', 1, NULL, NULL, NULL, NULL, 0),
(45, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/mokk_logo.png', '70:0f71633dc292d9f8c4b724cbd80ff4c2', 101, 'post', 'attachment', 1, 38, NULL, NULL, 'mokk_logo', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/mokk_logo.png', NULL, '101', 'attachment-image', NULL, NULL, NULL, '101', 'attachment-image', NULL, 0, NULL, NULL, '2020-09-15 11:32:02', '2020-09-15 09:32:02', 1, NULL, NULL, NULL, NULL, 1),
(46, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/muegyetem.png', '70:588a98da65b09f2103eb25315efb34ef', 102, 'post', 'attachment', 1, 39, NULL, NULL, 'muegyetem', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/muegyetem.png', NULL, '102', 'attachment-image', NULL, NULL, NULL, '102', 'attachment-image', NULL, 0, NULL, NULL, '2020-09-15 11:32:34', '2020-09-15 09:32:34', 1, NULL, NULL, NULL, NULL, 1),
(47, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/kurt-akademia.png', '74:8ab85d1c3acac37ed3db13f34022b7db', 103, 'post', 'attachment', 1, 40, NULL, NULL, 'kurt-akademia', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/kurt-akademia.png', NULL, '103', 'attachment-image', NULL, NULL, NULL, '103', 'attachment-image', NULL, 0, NULL, NULL, '2020-09-15 11:33:41', '2020-09-15 09:33:41', 1, NULL, NULL, NULL, NULL, 1),
(48, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/KRE_logo_A4_magyar.png', '79:065e9b60d2ea31c5bcf9f8d44764d829', 104, 'post', 'attachment', 1, 35, NULL, NULL, 'KRE_logo_A4_magyar', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/KRE_logo_A4_magyar.png', NULL, '104', 'attachment-image', NULL, NULL, NULL, '104', 'attachment-image', NULL, 0, NULL, NULL, '2020-09-15 11:40:00', '2020-09-15 09:40:00', 1, NULL, NULL, NULL, NULL, 1),
(49, 'http://localhost/mokk-aali.hu/kapcsolat/', '40:3967fa2bf36d1352c821794e39b6ce13', 105, 'post', 'nav_menu_item', 1, 0, NULL, NULL, 'Kapcsolat', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-15 12:00:19', '2020-09-18 08:36:34', 1, NULL, NULL, NULL, NULL, 0),
(50, 'http://localhost/mokk-aali.hu/rolunk/', '37:602cdb523a1bca0d2dfe2bcfca2a11be', 106, 'post', 'nav_menu_item', 1, 0, NULL, NULL, 'Rólunk', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-15 12:00:26', '2020-09-18 08:36:34', 1, NULL, NULL, NULL, NULL, 0),
(51, 'http://localhost/mokk-aali.hu/munkatarsaink/', '44:1c94b5484c556e025813155f57b649b4', 107, 'post', 'nav_menu_item', 1, 0, NULL, NULL, 'Munkatársaink', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-15 12:00:36', '2020-09-18 08:36:34', 1, NULL, NULL, NULL, NULL, 0),
(53, 'http://localhost/mokk-aali.hu/nemzetkozi-kapcsolatok/', '53:e55700f6f3a40df3dafb92e9eee720a1', 109, 'post', 'nav_menu_item', 1, 0, NULL, NULL, 'Nemzetközi Kapcsolatok', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-15 12:01:04', '2020-09-18 08:36:34', 1, NULL, NULL, NULL, NULL, 0),
(55, 'http://localhost/mokk-aali.hu/110/', '34:ee965d5c0cfd807c4ed72aef1522c2d0', 110, 'post', 'nav_menu_item', 1, 0, NULL, NULL, '', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-15 12:03:52', '2020-09-18 08:36:34', 1, NULL, NULL, NULL, NULL, 0),
(56, 'http://localhost/mokk-aali.hu/publikacioink/', '44:3438c5cc54dafdf566fd81a36bb18bcb', 42, 'post', 'page', 1, 0, NULL, NULL, 'Publikációink', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-15 12:45:57', '2020-09-18 04:49:24', 1, NULL, NULL, NULL, NULL, 0),
(57, 'http://localhost/mokk-aali.hu/cpt__trashed/', '43:0eb914eb58c236e94c6f5ad3b0503150', 64, 'post', 'page', 1, 0, NULL, NULL, 'CPT', 'trash', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-15 12:45:57', '2020-09-15 10:46:03', 1, NULL, NULL, NULL, NULL, 0),
(58, 'http://localhost/mokk-aali.hu/newsletter__trashed/', '50:c8abd3cbde74862b233237e2b22a0b9f', 9, 'post', 'page', 1, 0, NULL, NULL, 'Newsletter', 'trash', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-15 12:45:57', '2020-09-15 10:46:01', 1, NULL, NULL, NULL, NULL, 0),
(59, 'http://localhost/mokk-aali.hu/koszonto/', '39:7e73d43395bb3e6a97c299eb595245e7', 112, 'post', 'page', 1, 0, NULL, NULL, 'Köszöntő', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 30, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/parti-tamas-img.jpg', NULL, '114', 'featured-image', NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/parti-tamas-img.jpg', '114', 'featured-image', '{\"width\":993,\"height\":1083,\"url\":\"http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/parti-tamas-img.jpg\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\mokk-aali.hu/wp-content/uploads/2020/09/parti-tamas-img.jpg\",\"size\":\"full\",\"id\":114,\"alt\":\"\",\"pixels\":1075419,\"type\":\"image/jpeg\"}', 0, NULL, NULL, '2020-09-15 12:46:06', '2020-09-18 04:34:16', 1, NULL, NULL, NULL, NULL, 0),
(60, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/parti-tamas-img.jpg', '76:76b35e2d924023c61e4fce990aa7498d', 114, 'post', 'attachment', 1, 112, NULL, NULL, 'parti-tamas-img', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/09/parti-tamas-img.jpg', NULL, '114', 'attachment-image', NULL, NULL, NULL, '114', 'attachment-image', NULL, 0, NULL, NULL, '2020-09-15 13:03:54', '2020-09-15 11:03:54', 1, NULL, NULL, NULL, NULL, 1),
(61, 'http://localhost/mokk-aali.hu/publikacioink__trashed/', '53:252c1dd25203cee247bab5693bc4888f', 115, 'post', 'page', 1, 0, NULL, NULL, 'Publikációink', 'trash', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-15 13:42:55', '2020-09-18 04:42:43', 1, NULL, NULL, NULL, NULL, 0),
(62, 'http://localhost/mokk-aali.hu/publikacioink/', '44:3438c5cc54dafdf566fd81a36bb18bcb', NULL, 'post-type-archive', 'publications', NULL, NULL, '%%pt_plural%%%%page%% %%sep%% %%sitename%%', '', 'Publikációk', NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-15 13:45:17', '2020-09-18 05:10:19', 1, NULL, NULL, NULL, NULL, 0),
(63, 'http://localhost/mokk-aali.hu/hirek-es-esemenyek/', '49:8bf36d5f92f1a18d3d69ad7569269263', 118, 'post', 'page', 1, 0, NULL, NULL, 'Hírek és Események', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-15 14:08:52', '2020-09-15 12:08:59', 1, NULL, NULL, NULL, NULL, 0),
(64, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/placeholder-img-3.jpg', '78:5ded16ae4c15237a63b0ca393013c232', 13, 'post', 'attachment', 1, 12, NULL, NULL, 'placeholder-img-3', 'inherit', NULL, 0, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/placeholder-img-3.jpg', NULL, '13', 'attachment-image', NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/placeholder-img-3.jpg', '13', 'attachment-image', '{\"width\":800,\"height\":600,\"url\":\"http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/placeholder-img-3.jpg\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\mokk-aali.hu/wp-content/uploads/2020/03/placeholder-img-3.jpg\",\"size\":\"full\",\"id\":13,\"alt\":\"\",\"pixels\":480000,\"type\":\"image/jpeg\"}', 0, NULL, NULL, '2020-09-15 14:09:28', '2020-09-15 12:09:28', 1, NULL, NULL, NULL, NULL, 1),
(65, 'http://localhost/mokk-aali.hu/our_team/team-member-1/', '53:124def97cf00be35ae5cb3d25cc6b93f', 20, 'post', 'our_team', 1, 0, NULL, NULL, 'Team Member 1', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 60, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/unknown-person.png', NULL, '22', 'featured-image', NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/unknown-person.png', '22', 'featured-image', '{\"width\":450,\"height\":450,\"url\":\"http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/unknown-person.png\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\mokk-aali.hu/wp-content/uploads/2020/03/unknown-person.png\",\"size\":\"full\",\"id\":22,\"alt\":\"\",\"pixels\":202500,\"type\":\"image/png\"}', 0, NULL, NULL, '2020-09-15 14:09:28', '2020-09-15 12:09:28', 1, NULL, NULL, NULL, NULL, 0),
(66, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/unknown-person.png', '75:95b7ad72956995bfe2a4aa3721fd72eb', 22, 'post', 'attachment', 1, 20, NULL, NULL, 'unknown-person', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/unknown-person.png', NULL, '22', 'attachment-image', NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/unknown-person.png', '22', 'attachment-image', '{\"width\":450,\"height\":450,\"url\":\"http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/unknown-person.png\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\mokk-aali.hu/wp-content/uploads/2020/03/unknown-person.png\",\"size\":\"full\",\"id\":22,\"alt\":\"\",\"pixels\":202500,\"type\":\"image/png\"}', 0, NULL, NULL, '2020-09-15 14:09:28', '2020-09-15 12:09:28', 1, NULL, NULL, NULL, NULL, 1),
(67, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/partner-logo.png', '73:3c2d0a6b5b3fadc8cabba37007e88e97', 37, 'post', 'attachment', 1, 35, NULL, NULL, 'partner-logo', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/partner-logo.png', NULL, '37', 'attachment-image', NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/partner-logo.png', '37', 'attachment-image', '{\"width\":960,\"height\":624,\"url\":\"http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/partner-logo.png\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\mokk-aali.hu/wp-content/uploads/2020/03/partner-logo.png\",\"size\":\"full\",\"id\":37,\"alt\":\"\",\"pixels\":599040,\"type\":\"image/png\"}', 0, NULL, NULL, '2020-09-15 14:09:28', '2020-09-15 12:09:28', 1, NULL, NULL, NULL, NULL, 1),
(68, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/dx-engine-fav.png', '74:57022a53addd9f5454d4b85dd6adaa49', 71, 'post', 'attachment', 1, 0, NULL, NULL, 'dx-engine-fav', 'inherit', 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/dx-engine-fav.png', NULL, '71', 'attachment-image', NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/dx-engine-fav.png', '71', 'attachment-image', '{\"width\":178,\"height\":178,\"url\":\"http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/dx-engine-fav.png\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\mokk-aali.hu/wp-content/uploads/2020/03/dx-engine-fav.png\",\"size\":\"full\",\"id\":71,\"alt\":\"\",\"pixels\":31684,\"type\":\"image/png\"}', 0, NULL, NULL, '2020-09-15 14:09:28', '2020-09-15 12:09:28', 1, NULL, NULL, NULL, NULL, 0),
(69, 'http://localhost/mokk-aali.hu/7f0de3f479c81f5c6510af51edf5bbb9/', '63:a4b272eb4948a1abbf156a49b8f4fb73', 120, 'post', 'oembed_cache', 1, 0, NULL, NULL, '', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-15 14:09:29', '2020-09-15 12:09:29', 1, NULL, NULL, NULL, NULL, 0),
(70, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/cropped-dx-engine-fav.png', '82:470d22fd940999e9e33d4eeb41b6fd80', 72, 'post', 'attachment', 1, 0, NULL, NULL, 'cropped-dx-engine-fav.png', 'inherit', 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/cropped-dx-engine-fav.png', NULL, '72', 'attachment-image', NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/cropped-dx-engine-fav.png', '72', 'attachment-image', '{\"width\":512,\"height\":512,\"url\":\"http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/cropped-dx-engine-fav.png\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\mokk-aali.hu/wp-content/uploads/2020/03/cropped-dx-engine-fav.png\",\"size\":\"full\",\"id\":72,\"alt\":\"\",\"pixels\":262144,\"type\":\"image/png\"}', 0, NULL, NULL, '2020-09-15 14:09:29', '2020-09-15 12:09:29', 1, NULL, NULL, NULL, NULL, 0),
(71, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/dx-engine-fav-1.png', '76:2ed81999dd4e5e7f3f88017fb23cb322', 73, 'post', 'attachment', 1, 0, NULL, NULL, 'dx-engine-fav', 'inherit', 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/dx-engine-fav-1.png', NULL, '73', 'attachment-image', NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/dx-engine-fav-1.png', '73', 'attachment-image', '{\"width\":142,\"height\":128,\"url\":\"http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/dx-engine-fav-1.png\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\mokk-aali.hu/wp-content/uploads/2020/03/dx-engine-fav-1.png\",\"size\":\"full\",\"id\":73,\"alt\":\"\",\"pixels\":18176,\"type\":\"image/png\"}', 0, NULL, NULL, '2020-09-15 14:09:29', '2020-09-15 12:09:29', 1, NULL, NULL, NULL, NULL, 0),
(72, 'http://localhost/mokk-aali.hu/951794e709cb8a6c7feb965371d1d8bf/', '63:294a9d4e41ed43833f01a03ecd8513bb', 121, 'post', 'oembed_cache', 1, 0, NULL, NULL, '', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-15 14:09:29', '2020-09-15 12:09:29', 1, NULL, NULL, NULL, NULL, 0),
(73, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/cropped-dx-engine-fav-1.png', '84:6cd6cae9625a39523cba02d0acb1a59a', 74, 'post', 'attachment', 1, 0, NULL, NULL, 'cropped-dx-engine-fav-1.png', 'inherit', 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/cropped-dx-engine-fav-1.png', NULL, '74', 'attachment-image', NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/cropped-dx-engine-fav-1.png', '74', 'attachment-image', '{\"width\":512,\"height\":512,\"url\":\"http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/cropped-dx-engine-fav-1.png\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\mokk-aali.hu/wp-content/uploads/2020/03/cropped-dx-engine-fav-1.png\",\"size\":\"full\",\"id\":74,\"alt\":\"\",\"pixels\":262144,\"type\":\"image/png\"}', 0, NULL, NULL, '2020-09-15 14:09:29', '2020-09-15 12:09:29', 1, NULL, NULL, NULL, NULL, 0),
(74, 'http://localhost/mokk-aali.hu/our_team/team-member-2/', '53:297e2c1e02030476c28fa001feaefb1c', 23, 'post', 'our_team', 1, 0, NULL, NULL, 'Team Member 2', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 60, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/unknown-person.png', NULL, '22', 'featured-image', NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/unknown-person.png', '22', 'featured-image', '{\"width\":450,\"height\":450,\"url\":\"http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/unknown-person.png\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\mokk-aali.hu/wp-content/uploads/2020/03/unknown-person.png\",\"size\":\"full\",\"id\":22,\"alt\":\"\",\"pixels\":202500,\"type\":\"image/png\"}', 0, NULL, NULL, '2020-09-15 14:09:29', '2020-09-15 12:09:29', 1, NULL, NULL, NULL, NULL, 0),
(75, 'http://localhost/mokk-aali.hu/our_team/team-member-3/', '53:99abfb831e08e63abbdcef3d840df15b', 24, 'post', 'our_team', 1, 0, NULL, NULL, 'Team Member 3', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 60, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/unknown-person.png', NULL, '22', 'featured-image', NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/unknown-person.png', '22', 'featured-image', '{\"width\":450,\"height\":450,\"url\":\"http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/unknown-person.png\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\mokk-aali.hu/wp-content/uploads/2020/03/unknown-person.png\",\"size\":\"full\",\"id\":22,\"alt\":\"\",\"pixels\":202500,\"type\":\"image/png\"}', 0, NULL, NULL, '2020-09-15 14:09:29', '2020-09-15 12:09:29', 1, NULL, NULL, NULL, NULL, 0),
(76, 'http://localhost/mokk-aali.hu/our_team/team-member-4/', '53:42fa909ce263dd6f5a92d723b0cecf8d', 25, 'post', 'our_team', 1, 0, NULL, NULL, 'Team Member 4', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 60, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/unknown-person.png', NULL, '22', 'featured-image', NULL, NULL, 'http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/unknown-person.png', '22', 'featured-image', '{\"width\":450,\"height\":450,\"url\":\"http://localhost/mokk-aali.hu/wp-content/uploads/2020/03/unknown-person.png\",\"path\":\"C:\\\\xampp\\\\htdocs\\\\mokk-aali.hu/wp-content/uploads/2020/03/unknown-person.png\",\"size\":\"full\",\"id\":22,\"alt\":\"\",\"pixels\":202500,\"type\":\"image/png\"}', 0, NULL, NULL, '2020-09-15 14:09:29', '2020-09-15 12:09:29', 1, NULL, NULL, NULL, NULL, 0),
(77, 'http://localhost/mokk-aali.hu/testimonial/testimonial-1/', '56:70f2ab0acd4d47d71f0bc988c9d40bd7', 30, 'post', 'testimonial', 1, 0, NULL, NULL, 'Testimonial 1', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 60, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-15 14:09:29', '2020-09-15 12:09:29', 1, NULL, NULL, NULL, NULL, 0),
(78, 'http://localhost/mokk-aali.hu/testimonial/testimonial-2/', '56:961a50485c2feae4a1f7ec82ef64b33d', 32, 'post', 'testimonial', 1, 0, NULL, NULL, 'Testimonial 2', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 60, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-15 14:09:29', '2020-09-15 12:09:29', 1, NULL, NULL, NULL, NULL, 0),
(79, 'http://localhost/mokk-aali.hu/testimonial/testimonial-3/', '56:a841690007d4787937f076b1de4209e0', 33, 'post', 'testimonial', 1, 0, NULL, NULL, 'Testimonial 3', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 60, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-15 14:09:29', '2020-09-15 12:09:29', 1, NULL, NULL, NULL, NULL, 0),
(80, 'http://localhost/mokk-aali.hu/testimonial/testimonial-4/', '56:93b45bb12004a96c5e58733e54c0895f', 34, 'post', 'testimonial', 1, 0, NULL, NULL, 'Testimonial 4', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 60, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-15 14:09:29', '2020-09-15 12:09:29', 1, NULL, NULL, NULL, NULL, 0),
(81, 'http://localhost/mokk-aali.hu/category/egyeb/', '45:196d0e0f74f40a797cd94f1b896d661f', 1, 'term', 'category', NULL, NULL, NULL, NULL, 'Egyéb', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-15 14:09:29', '2020-09-15 12:09:29', 1, NULL, NULL, NULL, NULL, 0),
(82, NULL, NULL, NULL, 'system-page', 'search-result', NULL, NULL, 'You searched for %%searchphrase%% %%page%% %%sep%% %%sitename%%', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-15 14:09:29', '2020-09-15 12:09:29', 1, NULL, NULL, NULL, NULL, 0),
(83, NULL, NULL, NULL, 'date-archive', NULL, NULL, NULL, '%%date%% %%page%% %%sep%% %%sitename%%', '', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-15 14:09:29', '2020-09-15 12:09:29', 1, NULL, NULL, NULL, NULL, 0),
(84, 'http://localhost/mokk-aali.hu/our_team/', '39:c65fc4ae37caef0a016e1c5a6b521cb2', NULL, 'post-type-archive', 'our_team', NULL, NULL, '%%pt_plural%% Archívált %%page%% %%sep%% %%sitename%%', '', 'Our Team', NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-15 14:09:29', '2020-09-15 12:09:29', 1, NULL, NULL, NULL, NULL, 0),
(85, 'http://localhost/mokk-aali.hu/hirek-es-esemenyek/', '49:8bf36d5f92f1a18d3d69ad7569269263', NULL, 'post-type-archive', 'events', NULL, NULL, '%%pt_plural%% %%page%% %%sep%% %%sitename%%', '', 'Események', NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-15 14:09:59', '2020-09-15 12:22:18', 1, NULL, NULL, NULL, NULL, 0),
(86, 'http://localhost/mokk-aali.hu/?p=122', '36:83944bed37f76dc0b4bf1814b4793328', 122, 'post', 'post', 1, 0, NULL, NULL, 'Automatikus vázlat', 'auto-draft', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-18 06:31:30', '2020-09-18 04:31:30', 1, NULL, NULL, NULL, NULL, 0),
(87, 'http://localhost/mokk-aali.hu/esemenyek-2/', '42:ae1b33d4e0ad1ba0dbd08162a1730aaa', 123, 'post', 'nav_menu_item', 1, 0, NULL, NULL, '', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-18 06:32:01', '2020-09-18 08:36:34', 1, NULL, NULL, NULL, NULL, 0),
(88, 'http://localhost/mokk-aali.hu/ad76cc06-1286-4016-9947-871f118bc262/', '67:9d59670b6b39237d80ebb0a927f2f744', 125, 'post', 'customize_changeset', 1, 0, NULL, NULL, '', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-18 06:48:05', '2020-09-18 04:48:05', 1, NULL, NULL, NULL, NULL, 0),
(89, 'http://localhost/mokk-aali.hu/c9cccb87-0ed4-432a-a511-62e692e0433c/', '67:aaa0d11a7bde68005a9138b20e7a4760', 126, 'post', 'customize_changeset', 1, 0, NULL, NULL, '', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-18 06:50:24', '2020-09-18 04:50:24', 1, NULL, NULL, NULL, NULL, 0),
(90, 'http://localhost/mokk-aali.hu/127/', '34:84af719dbf3afc1650ef893af495fc4f', 127, 'post', 'nav_menu_item', 1, 0, NULL, NULL, '', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-18 06:58:51', '2020-09-18 08:36:34', 1, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_yoast_indexable_hierarchy`
--

CREATE TABLE `wp_yoast_indexable_hierarchy` (
  `indexable_id` int(11) UNSIGNED NOT NULL,
  `ancestor_id` int(11) UNSIGNED NOT NULL,
  `depth` int(11) UNSIGNED DEFAULT NULL,
  `blog_id` bigint(20) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `wp_yoast_indexable_hierarchy`
--

INSERT INTO `wp_yoast_indexable_hierarchy` (`indexable_id`, `ancestor_id`, `depth`, `blog_id`) VALUES
(20, 19, 1, 1),
(21, 19, 1, 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_yoast_migrations`
--

CREATE TABLE `wp_yoast_migrations` (
  `id` int(11) UNSIGNED NOT NULL,
  `version` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `wp_yoast_migrations`
--

INSERT INTO `wp_yoast_migrations` (`id`, `version`) VALUES
(1, '20171228151840'),
(2, '20171228151841'),
(3, '20190529075038'),
(4, '20191011111109'),
(5, '20200408101900'),
(6, '20200420073606'),
(7, '20200428123747'),
(8, '20200428194858'),
(9, '20200429105310'),
(10, '20200430075614'),
(11, '20200430150130'),
(12, '20200507054848'),
(13, '20200513133401'),
(14, '20200609154515'),
(15, '20200616130143'),
(16, '20200617122511'),
(17, '20200702141921'),
(18, '20200728095334');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_yoast_primary_term`
--

CREATE TABLE `wp_yoast_primary_term` (
  `id` int(11) UNSIGNED NOT NULL,
  `post_id` int(11) UNSIGNED NOT NULL,
  `term_id` int(11) UNSIGNED NOT NULL,
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `blog_id` bigint(20) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `wp_yoast_primary_term`
--

INSERT INTO `wp_yoast_primary_term` (`id`, `post_id`, `term_id`, `taxonomy`, `created_at`, `updated_at`, `blog_id`) VALUES
(1, 96, 3, 'category', '2020-09-15 09:04:49', '2020-09-15 07:14:15', 1),
(2, 99, 5, 'category', '2020-09-15 09:20:40', '2020-09-15 07:20:40', 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_yoast_seo_links`
--

CREATE TABLE `wp_yoast_seo_links` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `target_post_id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `indexable_id` int(11) UNSIGNED DEFAULT NULL,
  `target_indexable_id` int(11) UNSIGNED DEFAULT NULL,
  `height` int(11) UNSIGNED DEFAULT NULL,
  `width` int(11) UNSIGNED DEFAULT NULL,
  `size` int(11) UNSIGNED DEFAULT NULL,
  `language` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `wp_yoast_seo_meta`
--

CREATE TABLE `wp_yoast_seo_meta` (
  `object_id` bigint(20) UNSIGNED NOT NULL,
  `internal_link_count` int(10) UNSIGNED DEFAULT NULL,
  `incoming_link_count` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `wp_yoast_seo_meta`
--

INSERT INTO `wp_yoast_seo_meta` (`object_id`, `internal_link_count`, `incoming_link_count`) VALUES
(1, 0, 0),
(2, 0, 0),
(3, 0, 0),
(4, 0, 0),
(6, 0, 0),
(7, 0, 0),
(8, 0, 0),
(9, 0, 0),
(11, 0, 0),
(12, 0, 0),
(18, 0, 0),
(20, 0, 0),
(21, 0, 0),
(23, 0, 0),
(24, 0, 0),
(25, 0, 0),
(28, 0, 0),
(30, 0, 0),
(31, 0, 0),
(32, 0, 0),
(33, 0, 0),
(34, 0, 0),
(35, 0, 0),
(36, 0, 0),
(38, 0, 0),
(39, 0, 0),
(40, 0, 0),
(41, 0, 0),
(42, 0, 0),
(43, 0, 0),
(47, 0, 0),
(48, 0, 0),
(49, 0, 0),
(50, 0, 0),
(51, 0, 0),
(52, 0, 0),
(53, 0, 0),
(57, 0, 0),
(58, 0, 0),
(59, 0, 0),
(60, 0, 0),
(61, 0, 0),
(62, 0, 0),
(63, 0, 0),
(64, 0, 0),
(65, 0, 0),
(69, 0, 0),
(70, 0, 0);

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- A tábla indexei `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- A tábla indexei `wp_db7_forms`
--
ALTER TABLE `wp_db7_forms`
  ADD PRIMARY KEY (`form_id`);

--
-- A tábla indexei `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- A tábla indexei `wp_newsletter`
--
ALTER TABLE `wp_newsletter`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `wp_user_id` (`wp_user_id`);

--
-- A tábla indexei `wp_newsletter_emails`
--
ALTER TABLE `wp_newsletter_emails`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `wp_newsletter_sent`
--
ALTER TABLE `wp_newsletter_sent`
  ADD PRIMARY KEY (`email_id`,`user_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `email_id` (`email_id`);

--
-- A tábla indexei `wp_newsletter_stats`
--
ALTER TABLE `wp_newsletter_stats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email_id` (`email_id`),
  ADD KEY `user_id` (`user_id`);

--
-- A tábla indexei `wp_newsletter_user_logs`
--
ALTER TABLE `wp_newsletter_user_logs`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- A tábla indexei `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- A tábla indexei `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- A tábla indexei `wp_smush_dir_images`
--
ALTER TABLE `wp_smush_dir_images`
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `path_hash` (`path_hash`),
  ADD KEY `image_size` (`image_size`);

--
-- A tábla indexei `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- A tábla indexei `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- A tábla indexei `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- A tábla indexei `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- A tábla indexei `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- A tábla indexei `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- A tábla indexei `wp_yoast_indexable`
--
ALTER TABLE `wp_yoast_indexable`
  ADD PRIMARY KEY (`id`),
  ADD KEY `object_type_and_sub_type` (`object_type`,`object_sub_type`),
  ADD KEY `object_id_and_type` (`object_id`,`object_type`),
  ADD KEY `permalink_hash_and_object_type` (`permalink_hash`,`object_type`),
  ADD KEY `subpages` (`post_parent`,`object_type`,`post_status`,`object_id`),
  ADD KEY `prominent_words` (`prominent_words_version`,`object_type`,`object_sub_type`,`post_status`);

--
-- A tábla indexei `wp_yoast_indexable_hierarchy`
--
ALTER TABLE `wp_yoast_indexable_hierarchy`
  ADD PRIMARY KEY (`indexable_id`,`ancestor_id`),
  ADD KEY `indexable_id` (`indexable_id`),
  ADD KEY `ancestor_id` (`ancestor_id`),
  ADD KEY `depth` (`depth`);

--
-- A tábla indexei `wp_yoast_migrations`
--
ALTER TABLE `wp_yoast_migrations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `wp_yoast_migrations_version` (`version`);

--
-- A tábla indexei `wp_yoast_primary_term`
--
ALTER TABLE `wp_yoast_primary_term`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_taxonomy` (`post_id`,`taxonomy`),
  ADD KEY `post_term` (`post_id`,`term_id`);

--
-- A tábla indexei `wp_yoast_seo_links`
--
ALTER TABLE `wp_yoast_seo_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `link_direction` (`post_id`,`type`),
  ADD KEY `indexable_link_direction` (`indexable_id`,`type`);

--
-- A tábla indexei `wp_yoast_seo_meta`
--
ALTER TABLE `wp_yoast_seo_meta`
  ADD UNIQUE KEY `object_id` (`object_id`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT a táblához `wp_db7_forms`
--
ALTER TABLE `wp_db7_forms`
  MODIFY `form_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `wp_newsletter`
--
ALTER TABLE `wp_newsletter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT a táblához `wp_newsletter_emails`
--
ALTER TABLE `wp_newsletter_emails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `wp_newsletter_stats`
--
ALTER TABLE `wp_newsletter_stats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `wp_newsletter_user_logs`
--
ALTER TABLE `wp_newsletter_user_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2884;

--
-- AUTO_INCREMENT a táblához `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=483;

--
-- AUTO_INCREMENT a táblához `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;

--
-- AUTO_INCREMENT a táblához `wp_smush_dir_images`
--
ALTER TABLE `wp_smush_dir_images`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT a táblához `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT a táblához `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT a táblához `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT a táblához `wp_yoast_indexable`
--
ALTER TABLE `wp_yoast_indexable`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT a táblához `wp_yoast_migrations`
--
ALTER TABLE `wp_yoast_migrations`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT a táblához `wp_yoast_primary_term`
--
ALTER TABLE `wp_yoast_primary_term`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT a táblához `wp_yoast_seo_links`
--
ALTER TABLE `wp_yoast_seo_links`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
