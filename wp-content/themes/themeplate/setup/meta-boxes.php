<?php

/**
 * Register meta boxes
 *
 * @package ThemePlate
 * @since 0.1.0
 */

ThemePlate()->post_meta( array(
	'id'          => 'field',
	'title'       => __( 'ThemePlate Fields', 'themeplate' ),
	'description' => __( 'All available types.', 'themeplate' ),
	'fields'      => array(
		'text'     => array(
			'title'       => __( 'Text', 'themeplate' ),
			'description' => __( 'Enter a text.', 'themeplate' ),
			'type'        => 'text',
		),
		'date'     => array(
			'title'       => __( 'Date', 'themeplate' ),
			'description' => __( 'Set a date.', 'themeplate' ),
			'type'        => 'date',
		),
		'time'     => array(
			'title'       => __( 'Time', 'themeplate' ),
			'description' => __( 'Set a time.', 'themeplate' ),
			'type'        => 'time',
		),
		'email'    => array(
			'title'       => __( 'Email', 'themeplate' ),
			'description' => __( 'Enter an email.', 'themeplate' ),
			'type'        => 'email',
		),
		'url'      => array(
			'title'       => __( 'URL', 'themeplate' ),
			'description' => __( 'Enter a url.', 'themeplate' ),
			'type'        => 'url',
		),
		'textarea' => array(
			'title'       => __( 'Textarea', 'themeplate' ),
			'description' => __( 'Enter in a textarea.', 'themeplate' ),
			'type'        => 'textarea',
		),
		'selects'  => array(
			'title'       => __( 'Selects', 'themeplate' ),
			'description' => __( 'Basic and Advanced select.', 'themeplate' ),
			'type'        => 'group',
			'fields'      => array(
				'basic'    => array(
					'title'       => __( 'Basic', 'themeplate' ),
					'description' => __( 'Single and Multiple select.', 'themeplate' ),
					'type'        => 'group',
					'style'       => 'boxed',
					'fields'      => array(
						'single'   => array(
							'title'       => __( 'Single', 'themeplate' ),
							'description' => __( 'Select a value.', 'themeplate' ),
							'options'     => array( 'One', 'Two', 'Three' ),
							'type'        => 'select',
						),
						'multiple' => array(
							'title'       => __( 'Multiple', 'themeplate' ),
							'description' => __( 'Select values.', 'themeplate' ),
							'options'     => array( 'One', 'Two', 'Three', 'Four', 'Five', 'Six' ),
							'type'        => 'select',
							'multiple'    => true,
						),
					),
				),
				'advanced' => array(
					'title'       => __( 'Advanced', 'themeplate' ),
					'description' => __( 'Single and Multiple select.', 'themeplate' ),
					'type'        => 'group',
					'style'       => 'boxed',
					'fields'      => array(
						'single'   => array(
							'title'       => __( 'Single', 'themeplate' ),
							'description' => __( 'Select a value.', 'themeplate' ),
							'options'     => array( 'One', 'Two', 'Three' ),
							'type'        => 'select2',
						),
						'multiple' => array(
							'title'       => __( 'Multiple', 'themeplate' ),
							'description' => __( 'Select values.', 'themeplate' ),
							'options'     => array( 'One', 'Two', 'Three', 'Four', 'Five', 'Six' ),
							'type'        => 'select2',
							'multiple'    => true,
						),
					),
				),
			),
		),
		'choices'  => array(
			'title'       => __( 'Choices', 'themeplate' ),
			'description' => __( 'Single and Multiple choices.', 'themeplate' ),
			'type'        => 'group',
			'style'       => 'boxed',
			'fields'      => array(
				'radios'     => array(
					'title'       => __( 'Radios', 'themeplate' ),
					'description' => __( 'Inline and List radio.', 'themeplate' ),
					'type'        => 'group',
					'fields'      => array(
						'inline' => array(
							'title'       => __( 'Inline', 'themeplate' ),
							'description' => __( 'Select from radio.', 'themeplate' ),
							'options'     => array( 'First', 'Second', 'Third' ),
							'type'        => 'radio',
						),
						'list'   => array(
							'title'       => __( 'List', 'themeplate' ),
							'description' => __( 'Select from radio.', 'themeplate' ),
							'options'     => array( 'Fourth', 'Fifth', 'Sixth' ),
							'type'        => 'radiolist',
						),
					),
				),
				'checkboxes' => array(
					'title'       => __( 'Checkboxes', 'themeplate' ),
					'description' => __( 'Inline and List checkbox.', 'themeplate' ),
					'type'        => 'group',
					'fields'      => array(
						'inline' => array(
							'title'       => __( 'Inline', 'themeplate' ),
							'description' => __( 'Check a box.', 'themeplate' ),
							'options'     => array( 'Uno', 'Dos', 'Tres' ),
							'type'        => 'checkbox',
						),
						'list'   => array(
							'title'       => __( 'List', 'themeplate' ),
							'description' => __( 'Check a box.', 'themeplate' ),
							'options'     => array( 'Uno', 'Dos', 'Tres' ),
							'type'        => 'checklist',
						),
					),
				),
			),
		),
		'color'    => array(
			'title'       => __( 'Color', 'themeplate' ),
			'description' => __( 'Select a color.', 'themeplate' ),
			'type'        => 'color',
		),
		'files'    => array(
			'title'       => __( 'Files', 'themeplate' ),
			'description' => __( 'Single and Multiple file.', 'themeplate' ),
			'type'        => 'group',
			'fields'      => array(
				'single'   => array(
					'title'       => __( 'Single', 'themeplate' ),
					'description' => __( 'Select a file.', 'themeplate' ),
					'type'        => 'file',
				),
				'multiple' => array(
					'title'       => __( 'Multiple', 'themeplate' ),
					'description' => __( 'Select files.', 'themeplate' ),
					'type'        => 'file',
					'multiple'    => true,
				),
			),
		),
		'number'   => array(
			'title'       => __( 'Number', 'themeplate' ),
			'description' => __( 'Set a number.', 'themeplate' ),
			'type'        => 'number',
		),
		'range'    => array(
			'title'       => __( 'Range', 'themeplate' ),
			'description' => __( 'Set a range.', 'themeplate' ),
			'type'        => 'range',
		),
		'editor'   => array(
			'title'       => __( 'Editor', 'themeplate' ),
			'description' => __( 'Enter in an editor.', 'themeplate' ),
			'type'        => 'editor',
		),
		'objects'  => array(
			'title'  => __( 'Objects', 'themeplate' ),
			'type'   => 'group',
			'style'  => 'boxed',
			'fields' => array(
				'post' => array(
					'title'       => __( 'Posts', 'themeplate' ),
					'description' => __( 'Single and Multiple post.', 'themeplate' ),
					'type'        => 'group',
					'style'       => 'boxed',
					'fields'      => array(
						'single'   => array(
							'title'       => __( 'Single', 'themeplate' ),
							'description' => __( 'Select a post.', 'themeplate' ),
							'type'        => 'post',
							'options'     => 'post',
						),
						'multiple' => array(
							'title'       => __( 'Multiple', 'themeplate' ),
							'description' => __( 'Select posts.', 'themeplate' ),
							'type'        => 'post',
							'options'     => 'post',
							'multiple'    => true,
						),
					),
				),
				'page' => array(
					'title'       => __( 'Pages', 'themeplate' ),
					'description' => __( 'Single and Multiple page.', 'themeplate' ),
					'type'        => 'group',
					'style'       => 'boxed',
					'fields'      => array(
						'single'   => array(
							'title'       => __( 'Single', 'themeplate' ),
							'description' => __( 'Select a page.', 'themeplate' ),
							'type'        => 'page',
							'options'     => 'page',
						),
						'multiple' => array(
							'title'       => __( 'Multiple', 'themeplate' ),
							'description' => __( 'Select pages.', 'themeplate' ),
							'type'        => 'page',
							'options'     => 'page',
							'multiple'    => true,
						),
					),
				),
				'user' => array(
					'title'       => __( 'Users', 'themeplate' ),
					'description' => __( 'Single and Multiple user.', 'themeplate' ),
					'type'        => 'group',
					'style'       => 'boxed',
					'fields'      => array(
						'single'   => array(
							'title'       => __( 'Single', 'themeplate' ),
							'description' => __( 'Select a user.', 'themeplate' ),
							'type'        => 'user',
							'options'     => 'administrator',
						),
						'multiple' => array(
							'title'       => __( 'Multiple', 'themeplate' ),
							'description' => __( 'Select users.', 'themeplate' ),
							'type'        => 'user',
							'options'     => 'administrator',
							'multiple'    => true,
						),
					),
				),
				'term' => array(
					'title'       => __( 'Terms', 'themeplate' ),
					'description' => __( 'Single and Multiple term.', 'themeplate' ),
					'type'        => 'group',
					'style'       => 'boxed',
					'fields'      => array(
						'single'   => array(
							'title'       => __( 'Single', 'themeplate' ),
							'description' => __( 'Select a term.', 'themeplate' ),
							'type'        => 'term',
							'options'     => 'category',
						),
						'multiple' => array(
							'title'       => __( 'Multiple', 'themeplate' ),
							'description' => __( 'Select terms.', 'themeplate' ),
							'type'        => 'term',
							'options'     => 'category',
							'multiple'    => true,
						),
					),
				),
			),
		),
		'html'     => array(
			'type'    => 'html',
			'default' => '
				<div style="background-color: #d32f2f; padding: 1rem; border-radius: 0.25rem;">
					<p style="color: #fff; text-align: center;">Display custom content using an <code>html</code> field.</p>
				</div>
			',
		),
	),
) );
