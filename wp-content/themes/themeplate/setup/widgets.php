<?php

/**
 * Register widget areas
 *
 * @package ThemePlate
 * @since 0.1.0
 */

if ( ! function_exists( 'themeplate_widgets_init' ) ) {
	function themeplate_widgets_init() {
		register_sidebar( array(
			'id'            => 'sidebar',
			'name'          => __( 'Sidebar Area', 'themeplate' ),
			'description'   => __( 'Add widgets here to appear in sidebar.', 'themeplate' ),
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
			'before_widget' => '<section class="widget %2$s">',
			'after_widget'  => '</section>',
		) );
		register_sidebars( 3, array(
			'id'            => 'footer',
			'name'          => __( 'Footer Area %d', 'themeplate' ),
			'description'   => __( 'Add widgets here to appear in footer.', 'themeplate' ),
			'before_title'  => '<h4 class="widget-title">',
			'after_title'   => '</h4>',
			'before_widget' => '<section class="widget %2$s">',
			'after_widget'  => '</section>',
		) );

		$widgets = glob( THEME_PATH . 'widgets/*_widget.php' );
		foreach ( $widgets as $widget ) {
			require_once $widget;
			if ( class_exists( 'ThemePlate_' . basename( $widget, '.php' ) ) ) {
				register_widget( 'ThemePlate_' . basename( $widget, '.php' ) );
			}
		}
	}
	add_action( 'widgets_init', 'themeplate_widgets_init' );
}
