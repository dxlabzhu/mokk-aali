<?php

/**
 * CPT: events
 *
 * @package ThemePlate
 * @since 0.1.0
 */
?>

<?php get_header(); ?>
<section id="page-events">
  <div class="section-title-box">
    <?php picture('events-icon', 'png', '',  true, 'events-icon'); ?>
      <div class="gradient-layer_white"></div>    
      <div class="container">
        <h1 class="section-title"><span>A Közelmúltban Aktuális</span> Hírek és Események</h1>
    <!-- Marketing Icons Section -->
       </div>
       <?php picture('grey-pattern', 'png', '',  true, 'grey-pattern'); ?>
       <?php picture('slash-pattern-right-blue-big', 'png', 'Dr. ',  true, 'slash-pattern_right_blue_big'); ?>
  </div>
    <div class="container">
    <div class="row">


      <?php if ( have_posts() ) :  while ( have_posts() ) :  the_post(); ?>

              <div class="col-lg-6 mb-6">
                <?php picture('card-bg-pattern', 'png', '',  true, 'card-bg-pattern'); ?>
                <div class="card-box">
                  <div class="card-img">
                    <?php if ( has_post_thumbnail() ) { ?>
                      <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('post-thumbnail', ['class' => 'card-img-top']); ?></a>
                    <?php } else { ?>
                      <a href="<?php the_permalink(); ?>"><img src="https://placehold.it/700x400"></a>
                    <?php } ?>
                    <div class="card-header">
                      <?php
                        $categories = get_the_category();
                        $separator = ' ';
                        $output = '';
                        if ( ! empty( $categories ) ) {
                            foreach( $categories as $category ) {
                                echo '<p>' . $category->name . '</p>';
                            }
                        } ?>
                        <div class="semidark-blue-pillar"></div>
                        <div class="dark-blue-pillar"></div>
                    </div>
                    <?php picture('slash-pattern-middle', 'png', 'Dr. ',  true, 'slash-pattern_middle'); ?>
                    </div>
                    <div class="card-body">
                      <div class="card-text">
                        <a href="<?php the_permalink(); ?>">
                        <h5 class="entry-content"><?php the_title(); ?></h5>
                        </a>
                        <div class="main-content">
                          <?php echo get_excerpt(125); ?>
                        </div>
                      </div>
                    </div>
                    <div class="three-color-separator">
                        <p>Dr. Parti Tamás<!-- <?php the_author(); ?> --></p>
                        <p><?php the_date(); ?></p>
                        <div class="semidark-blue-pillar"></div>
                        <div class="dark-blue-pillar"></div> 
                        <a class="read-more" href="<?php the_permalink(); ?>">Tovább<span><?php picture('read-more-icon', 'png', 'Dr. ',  true, 'read-more-icon'); ?></span></a>
                    </div>
                  </div>
              </div>

          <?php endwhile; ?>
          <div class="pagination-box">
          <?php 
          the_posts_pagination( array(
            'prev_text'          => __( 'Previous page', 'cm' ),
            'next_text'          => __( 'Next page', 'cm' ),
          ) ); ?>
          </div>
        <?php endif; ?>
      </div>
      <!-- /.row -->
  </div>
</section>
<?php get_footer(); ?>