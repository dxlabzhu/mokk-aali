<?php
/**
 * The main template file
 *
 * @package ThemePlate
 * @since 0.1.0
 */

get_header();

?>
  <header id="hero">
      <div class="hero-title">
        <div class="gradient-white">
          <div class="container">
            <h1>Magyar Országos Közjegyzői Kamara <span>Adatkutató Alintézet</span></h1>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="hero-desc">
              <p>Minden fejlesztés alapja a múlt és a jelen lehető legrészletesebb megismerése. A Magyar Országos Közjegyzői Kamara az Adatkutató Alintézet létrehozásával a rá vonatkozó törvényi keretek között a legmodernebb technológiát alkalmazva törekszik a hivatásrendi folyamatok minél részletesebb elemzésére és megismerésére a közjegyzői eljárások fejlesztése érdekében.</p>
        <a href="#" class="button-cta">AZ INTÉZMÉNYRŐL BŐVEBBEN</a>
        </div>
      </div>
  </header>

<!-- Page Content -->
<section id="welcome">
  <?php picture('parti-tamas-img', 'jpeg', 'Dr. Parti Tamás közjegyző',  true, 'parti-img'); ?>
  <div class="gradient-separator">
    <?php picture('slash-pattern-right', 'png', '',  true, 'slash-pattern_right'); ?>
  </div>
  <div class="welcome-text">
    <div class="semidark-blue-pillar"></div>
    <div class="dark-blue-pillar"></div>
    <div class="container">
      <h3 class="section-title">KÖSZÖNTŐ<div class="cube"></div><div class="cube"></div><div class="cube"></div></h3>
      <h2 class="section-sub-title">Dr. Parti Tamás Intézetvezető</h2>
      <p>Üdvözlöm a Magyar Országos Közjegyzői Kamara (MOKK) Intézete <strong>Adatkutató Alintézetének</strong> weboldalán. Az Alintézet létrehozásával a MOKK jelentős lépést tett a technológiai alkalmazkodás irányában azzal, hogy a közjegyzői hivatásrend belső folyamatainak mélyebb és kiterjedtebb megismerésére, és a hivatásrend fejlesztésére az eddig rendelkezésre állt eszközök mellett igénybe veszi a legmodernebb technológia által kínált lehetőségeket is.</p>
      <p class="name"><strong>Dr. Parti Tamás</strong></p>
      <p>intézetvezető</p>
      <a href="<?php echo get_site_url();?>/koszonto" class="button-cta_white">TOVÁBB</a>
    </div>
    <?php picture('blue-pattern', 'png', '',  true, 'blue-pattern'); ?>
  </div>
    <div class="gradient-separator">
    <?php picture('slash-pattern-left', 'png', 'Dr. ',  true, 'slash-pattern_left'); ?>
  </div>
</section>

<section id="publications">
  <div class="container">
    <h2 class="section-title"><div class="cube_small"></div><div class="cube_small"></div><div class="cube_small cube_blue"></div>Publikációink<div class="cube_small cube_blue"></div><div class="cube_small"></div><div class="cube_small"></div></h2>
    <?php picture('publications-icon', 'png', '',  true, 'publications-icon'); ?>
    <div class="row">

      <?php 
        $args_publications = array(
          'post_type' => 'publications',
          'posts_per_page' => 3,
          'orderby' => 'post_date',
          'order' => 'DESC',
        );

        $loop_publications = new WP_Query( $args_publications );
      ?>
      <?php if ( $loop_publications->have_posts() ) : ?>
        <?php while ( $loop_publications->have_posts() ) : ?>
          <?php $loop_publications->the_post(); ?>

          <div class="col-lg-6 mb-6">
            <?php picture('card-bg-pattern', 'png', '',  true, 'card-bg-pattern'); ?>
            <div class="card-box">
              <div class="card-img">
                <?php if ( has_post_thumbnail() ) { ?>
                  <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('post-thumbnail', ['class' => 'card-img-top']); ?></a>
                <?php } else { ?>
                  <a href="<?php the_permalink(); ?>"><img src="https://placehold.it/700x400"></a>
                <?php } ?>
                </div>
              <div class="card-header">
               <a href="<?php the_permalink(); ?>">
                <h4><?php the_field('publikacio_megjelenesi_helye');?></h4>
              </a>
                  <div class="semidark-blue-pillar"></div>
                  <div class="dark-blue-pillar"></div>
              </div>
                <div class="card-body">
                  <div class="card-text">
                    <a href="<?php the_permalink(); ?>">
                    <h5 class="entry-content"><?php the_title(); ?></h5>
                    </a>
                    <p class="author"><?php the_field('publikacio_szerzoje');?>,</p>
                    <p class="date"><?php the_field('publikalas_datuma');?></p>
                  </div>
                </div>
                <?php picture('grey-pattern', 'png', '',  true, 'grey-pattern'); ?>
                <?php picture('slash-pattern-right-blue', 'png', 'Dr. ',  true, 'slash-pattern_right_blue'); ?>
              </div>
          </div>
        <!-- #post-<?php the_ID(); ?> -->

          <?php endwhile; ?>
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>
      </div>
      <a href="#" class="button-cta">MINDEN TANULMÁNY ÉS SZAKCIKK</a>
      <?php picture('slash-pattern-right-blue-big', 'png', 'Dr. ',  true, 'slash-pattern_right_blue_big'); ?>
      <!-- /.row -->
  </div>
</section>

<section id="international-contacts">
  <div class="three-color-separator">
      <div class="semidark-blue-pillar"></div>
      <div class="dark-blue-pillar"></div> 
  </div>
  <div class="container">
        <h2 class="section-title"><div class="cube_small"></div><div class="cube_small"></div><div class="cube_small cube_blue"></div><span>Nemzetközi</span>Kapcsolatok<div class="cube_small cube_blue"></div><div class="cube_small"></div><div class="cube_small"></div></h2>
        <?php picture('international-contacts-icon', 'png', '',  true, 'international-contacts-icon'); ?>
         <?php 
             
          $args_int_contacts = array(
            'post_type' => 'int_contacts',
            'posts_per_page' => 4,
            'orderby' => 'date',
            'order'   => 'DESC',
          );

              $loop_int_contacts = new WP_Query( $args_int_contacts );
            ?>
            <?php if ( $loop_int_contacts->have_posts() ) : ?>
              <?php while ( $loop_int_contacts->have_posts() ) : ?>
                <?php $loop_int_contacts->the_post(); ?>
                  <div class="accordion">
                    <img class="decoration" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/nyito-jel.png" alt=""/>
                    <h4 class="service-name"><?php the_title(); ?></h4>
                  </div>
                  <div class="panel">
                    <p><?php the_content(); ?></p>
                  </div>

              <?php endwhile; ?>
              <?php endif; ?>
              <?php wp_reset_postdata(); ?>

  </div>
    <?php picture('grey-pattern', 'png', '',  true, 'grey-pattern'); ?>
    <?php picture('slash-pattern-right-blue', 'png', 'Dr. ',  true, 'slash-pattern_right_blue'); ?>
    <div class="one-color-separator"> 
  </div>
</section>
<section id="events">
  <div class="container">
        <h2 class="section-title"><div class="cube_small"></div><div class="cube_small"></div><div class="cube_small cube_blue"></div>Események<div class="cube_small cube_blue"></div><div class="cube_small"></div><div class="cube_small"></div></h2>
        <?php picture('events-icon', 'png', '',  true, 'events-icon'); ?>
        <div class="row">
         <?php 
             
          $args_events = array(
            'post_type' => 'events',
            'tag' => 'kiemelt',
            'posts_per_page' => 1,
          );

              $loop_events = new WP_Query( $args_events );
            ?>
            <?php if ( $loop_events->have_posts() ) : ?>
              <?php while ( $loop_events->have_posts() ) : ?>
                <?php $loop_events->the_post(); ?>
              <div class="col-lg-6 mb-6">
                <?php picture('card-bg-pattern', 'png', '',  true, 'card-bg-pattern'); ?>
                <div class="card-box">
                  <div class="card-img">
                    <?php if ( has_post_thumbnail() ) { ?>
                      <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('post-thumbnail', ['class' => 'card-img-top']); ?></a>
                    <?php } else { ?>
                      <a href="<?php the_permalink(); ?>"><img src="https://placehold.it/700x400"></a>
                    <?php } ?>
                    <div class="card-header">
                      <?php
                        $categories = get_the_category();
                        $separator = ' ';
                        $output = '';
                        if ( ! empty( $categories ) ) {
                            foreach( $categories as $category ) {
                                echo '<p>' . $category->name . '</p>';
                            }
                        } ?>
                        <div class="semidark-blue-pillar"></div>
                        <div class="dark-blue-pillar"></div>
                    </div>
                    <?php picture('slash-pattern-middle', 'png', 'Dr. ',  true, 'slash-pattern_middle'); ?>
                    </div>
                    <div class="card-body">
                      <div class="card-text">
                        <a href="<?php the_permalink(); ?>">
                        <h5 class="entry-content"><?php the_title(); ?></h5>
                        </a>
                        <div class="main-content">
                          <?php the_content(); ?>
                        </div>
                      </div>
                    </div>
                    <div class="dark-three-color-separator">
                        <p>Dr. Parti Tamás<!-- <?php the_author(); ?> --></p>
                        <p><?php the_date(); ?></p>
                        <div class="semidark-pillar"></div>
                        <div class="dark-pillar"></div> 
                    </div>
                  </div>
              </div>

              <?php endwhile; ?>
              <?php endif; ?>
              <?php wp_reset_postdata(); ?>
        </div>
        <a href="#" class="button-cta">MINDEN HÍR ÉS ESEMÉNY</a>
  </div>
    <?php picture('grey-pattern', 'png', '',  true, 'grey-pattern'); ?>
  </div>
</section>


<section id="partners">
	<div class="container">
      <h2 class="section-title"><div class="cube_small"></div><div class="cube_small"></div><div class="cube_small cube_blue"></div><span>Együttműködő</span>Partnereink<div class="cube_small cube_blue"></div><div class="cube_small"></div><div class="cube_small"></div></h2>
      <?php picture('partners-icon', 'png', '',  true, 'partners-icon'); ?>
		<div class="logos">
			<?php
			    $args_partners = array(
			        'post_type' => 'partner',
			        'posts_per_page' => -1,
			    );
			    $loop_partners = new WP_Query( $args_partners );
			if ( $loop_partners->have_posts() ) : while ( $loop_partners->have_posts() ) : $loop_partners->the_post(); ?>
			  <?php echo get_the_post_thumbnail( $post->ID, 'medium', ['class' => 'partner-logo-img'] ); ?>
			  <?php endwhile; ?>
			  <?php endif; ?>
        <?php wp_reset_postdata(); ?>
		</div>
	</div>
</section>


  <?php

  get_footer();
