<?php

/**
 * ThemePlate functions and definitions
 *
 * @package ThemePlate
 * @since 0.1.0
 */

/* ==================================================
Global constants
================================================== */
// phpcs:disable Generic.Functions.FunctionCallArgumentSpacing.TooMuchSpaceAfterComma
define( 'THEME_NAME',    wp_get_theme()->get( 'Name' ) );
define( 'THEME_VERSION', wp_get_theme()->get( 'Version' ) );
define( 'THEME_URI',     wp_get_theme()->get( 'ThemeURI' ) );
define( 'THEME_AUTHOR',  wp_get_theme()->get( 'Author' ) );
define( 'AUTHOR_URI',    wp_get_theme()->get( 'AuthorURI' ) );
define( 'PARENT_THEME',  wp_get_theme()->get( 'Template' ) );
define( 'THEME_URL',     get_template_directory_uri() . '/' );
define( 'THEME_PATH',    get_template_directory() . '/' );
define( 'THEME_INC',     THEME_PATH . 'includes/' );
define( 'WEBP', 		 strpos( $_SERVER['HTTP_ACCEPT'], 'image/webp' ));
define( 'THEME_DEBUG',   true );
// phpcs:enable

/* ==================================================
Setup Theme
================================================== */

if ( class_exists( 'ThemePlate' ) ) :
	ThemePlate( array(
		'title' => 'ThemePlate',
		'key'   => 'themeplate',
	) );
//	require_once THEME_PATH . 'setup/post-types.php';
	require_once THEME_PATH . 'setup/settings.php';
	require_once THEME_PATH . 'setup/meta-boxes.php';
endif;

require_once THEME_PATH . 'setup/plugins.php';
require_once THEME_PATH . 'setup/features.php';
require_once THEME_PATH . 'setup/navigations.php';
require_once THEME_PATH . 'setup/widgets.php';
require_once THEME_PATH . 'setup/scripts-styles.php';
require_once THEME_PATH . 'setup/actions-filters.php';
require_once THEME_PATH . 'setup/hooks.php';
require_once THEME_PATH . 'setup/utilities.php';

/* ==================================================
Extra custom functions
================================================== */

function wpbsearchform( $form ) {
 
    $form = '<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
    <div><label class="screen-reader-text search-text" for="s">' . __('Search for:') . '</label>
    <input type="text" value="' . get_search_query() . '" name="s" id="s" />
    <input type="submit" id="searchsubmit" class="btn btn-primary" value="'. esc_attr__('Search') .'" />
    </div>
    </form>';
 
    return $form;
}
 
add_shortcode('wpbsearch', 'wpbsearchform');


// Limit except length to 125 characters.
// tn limited excerpt length by number of characters
function get_excerpt( $count ) {
$permalink = get_permalink($post->ID);
$excerpt = get_the_content();
$excerpt = strip_tags($excerpt);
$excerpt = substr($excerpt, 0, $count);
$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
$excerpt = '<p>'.$excerpt.'...';
return $excerpt;
}