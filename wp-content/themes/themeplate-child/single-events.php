<?php

/**
 * The template for displaying single posts - Events
 *
 * @package ThemePlate
 * @since 0.1.0
 */

get_header();

?>
<section id="single-events-post">
		<main class="content">

			<?php while ( have_posts() ) : ?>
				<?php the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

					<div class="page-header">
						<div class="card-img">
							<?php echo get_the_post_thumbnail( $post->ID, 'medium', ['class' => 'featured-img'] ); ?>
							<?php picture('slash-pattern-right-big', 'png', '',  true, 'slash-pattern_right_big'); ?>
						</div>
	                    <div class="card-header three-color-separator">
	                      <?php
	                        $categories = get_the_category();
	                        $separator = ' ';
	                        $output = '';
	                        if ( ! empty( $categories ) ) {
	                            foreach( $categories as $category ) {
	                                echo '<span>' . $category->name . ' / </span>';
	                            }
	                        } ?>
	                        <div class="semidark-blue-pillar"></div>
	                        <div class="dark-blue-pillar"></div>
	                    </div>
						<div class="title-box">
	                        <h4>Dr. Parti Tamás<!-- <?php the_author(); ?> --></h4>
                        	<h3><?php echo get_the_date(); ?><div class="cube"></div><div class="cube"></div><div class="cube"></div></h3>
							<h2 class="section-sub-title"><?php the_title(); ?></h2>
						</div>
					</div>
				<div class="container">
					<div class="entry-wrap">
						<div class="entry-content">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
				<?php picture('grey-pattern', 'png', '',  true, 'grey-pattern'); ?>
				</article><!-- #post-<?php the_ID(); ?> -->

			<?php endwhile; ?>

		</main><!-- .content -->

	</div><!-- .content-sidebar.container -->
</section>
<?php

get_footer();
