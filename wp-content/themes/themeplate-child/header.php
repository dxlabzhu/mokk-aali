<?php

/**
 * The template for displaying the header
 *
 * @package ThemePlate
 * @since 0.1.0
 */

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="https://fonts.gstatic.com" rel="preconnect" crossorigin>
    <link rel="preconnect" href="https://fonts.googleapis.com/" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:wght@700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;800&display=swap" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/themeplate.min.css?v=<?php echo time(); ?>">
    <script>
      var pluginsUrl = '<?php echo plugins_url(); ?>';
      var themePath = '<?php echo get_stylesheet_directory_uri(); ?>';

      function constructElement(type, attributes) {
        let element = document.createElement(type);

        for (let attribute in attributes) {
          element[attribute] = attributes[attribute];
        }

        return element;
      }

      function loadBaseStyles() {
        let documentFragment = document.createDocumentFragment();

        var clientWidth = document.documentElement.clientWidth;

        if (clientWidth > 767) {
          documentFragment.appendChild(constructElement('link', {
            rel: 'stylesheet',
            media: '(min-width: 768px)',
            href: themePath + '/assets/css/themeplate-tablet.min.css<?php echo '?v=' . time(); ?>'
          }));
        }


        if (clientWidth > 1199) {
          documentFragment.appendChild(constructElement('link', {
            rel: 'stylesheet',
            media: '(min-width: 1200px)',
            href: themePath + '/assets/css/themeplate-desktop.min.css<?php echo '?v=' . time(); ?>'
          }));
        }

        document.head.appendChild(documentFragment);
      }

      loadBaseStyles();
    </script>
    <?php wp_head(); ?>
  </head>

  <body <?php body_class(WEBP ? 'webP' : ''); ?>>
    <?php wp_body(); ?>

<!--    <a class="screen-reader-text" href="#site-content">--><?php //esc_html_e( 'Skip to content', 'themeplate' ); ?><!--</a>-->

    <!-- Search -->
<!--     <div id="search">
      <div class="container">
        <div class="search-box">
          <?php echo do_shortcode('[wpbsearch]'); ?>
        </div>
      </div>
    </div> -->
    <div class="topbar hide-on-mobile">
    	<div class="container">
    		<h2 class="title">MOKK. ADATKUTATÓ ALINTÉZET</h2>
    	</div>
    </div>
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark">
      <div class="container">
        <a class="navbar-brand" href="<?php bloginfo('url'); ?>"><?php picture('mokk-adatkutatoalintezet-logo-small', 'png', 'MOKK Adatkutató Alintézet Logó',  true, 'site-logo'); ?></a>
        <button onclick="toggleMenu();"  class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
              <div class="bar1"></div>
			  <div class="bar2"></div>
			  <div class="bar3"></div>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <?php themeplate_primary_menu(); ?>
        </div>
      </div>
      <?php picture('menu-pattern', 'png', '',  true, 'menu-pattern'); ?>
    </nav>

