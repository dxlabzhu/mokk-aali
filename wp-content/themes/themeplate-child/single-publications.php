<?php

/**
 * The template for displaying single posts - Publications
 *
 * @package ThemePlate
 * @since 0.1.0
 */

get_header();

?>
<section id="single-publications-post">
		<main class="content">

			<?php while ( have_posts() ) : ?>
				<?php the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

					<div class="page-header">
						<?php echo get_the_post_thumbnail( $post->ID, 'medium', ['class' => 'featured-img'] ); ?>
						<div class="title-box">
							<?php picture('slash-pattern-middle', 'png', '',  true, 'slash-pattern_right_top'); ?>
					      	<h3 class="section-title">Publikációk<div class="cube"></div><div class="cube"></div><div class="cube"></div></h3>
							<h2 class="section-sub-title"><?php the_title(); ?></h2>
							<?php picture('grey-pattern', 'png', '',  true, 'grey-pattern'); ?>
							<?php picture('slash-pattern-middle', 'png', '',  true, 'slash-pattern_right_bottom'); ?>
						</div>
					</div>
				<div class="container">
					<div class="entry-wrap">
						<header class="entry-header">
							<div class="entry-meta">
                    		<p class="author"><?php the_field('publikacio_szerzoje');?>,</p>
                    		<p class="date"><?php the_field('publikalas_datuma');?></p>
							</div>
						</header>

						<div class="entry-content">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
				</article><!-- #post-<?php the_ID(); ?> -->

			<?php endwhile; ?>

		</main><!-- .content -->

	</div><!-- .content-sidebar.container -->
</section>
<?php

get_footer();
