<?php

/**
 * CPT: publications
 *
 * @package ThemePlate
 * @since 0.1.0
 */
?>

<?php get_header(); ?>
<section id="page-publications">
  <div class="section-title-box">
    <?php picture('publications-icon', 'png', '',  true, 'publications-icon'); ?>
      <div class="gradient-layer_white"></div>    
      <div class="container">
        <h1 class="section-title"><span>A Témában Megjelent</span> Tanulmányok és Szakcikkek</h1>
    <!-- Marketing Icons Section -->
       </div>
       <?php picture('grey-pattern', 'png', '',  true, 'grey-pattern'); ?>
       <?php picture('slash-pattern-right-blue-big', 'png', 'Dr. ',  true, 'slash-pattern_right_blue_big'); ?>
  </div>
    <div class="container">
    <div class="row">


      <?php if ( have_posts() ) :  while ( have_posts() ) :  the_post(); ?>

          <div class="col-lg-6 mb-6">
            <?php picture('card-bg-pattern', 'png', '',  true, 'card-bg-pattern'); ?>
            <div class="card-box">
              <div class="card-img">
                <?php if ( has_post_thumbnail() ) { ?>
                  <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('post-thumbnail', ['class' => 'card-img-top']); ?></a>
                <?php } else { ?>
                  <a href="<?php the_permalink(); ?>"><img src="https://placehold.it/700x400"></a>
                <?php } ?>
                </div>
              <div class="card-header">
               <a href="<?php the_permalink(); ?>">
                <h4><?php the_field('publikacio_megjelenesi_helye');?></h4>
              </a>
                  <div class="semidark-blue-pillar"></div>
                  <div class="dark-blue-pillar"></div>
              </div>
                <div class="card-body">
                  <div class="card-text">
                    <a href="<?php the_permalink(); ?>">
                    <h5 class="entry-content"><?php the_title(); ?></h5>
                    </a>
                    <p class="author"><?php the_field('publikacio_szerzoje');?>,</p>
                    <p class="date"><?php the_field('publikalas_datuma');?></p>
                  </div>
                </div>
                <?php picture('grey-pattern', 'png', '',  true, 'grey-pattern'); ?>
                <?php picture('slash-pattern-right-blue', 'png', 'Dr. ',  true, 'slash-pattern_right_blue'); ?>
              </div>
          </div>

          <?php endwhile; ?>
          <div class="pagination-box">
          <?php 
          the_posts_pagination( array(
            'prev_text'          => __( 'Previous page', 'cm' ),
            'next_text'          => __( 'Next page', 'cm' ),
          ) ); ?>
          </div>
        <?php endif; ?>
      </div>
      <!-- /.row -->
  </div>
</section>
<?php get_footer(); ?>