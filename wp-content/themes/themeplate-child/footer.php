<?php

/**
 * The template for displaying the footer
 *
 * @package ThemePlate
 * @since 0.1.0
 */

?>

</div><!-- .site-content -->

<!-- <?php get_sidebar( 'footer' ); ?> -->

    <!-- Footer -->
    <footer>
      <div class="container">
<!--   		<nav class="sitelinks">
			<?php themeplate_footer_menu(); ?>
		</nav> -->
        <p class="text-center text-white">Copyright &copy; 2020 minden jog fenntartva.</p>
			<p class="text-center text-white">Design by<a href="https://dxlabz.hu/"><img class="dxlogo" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/dxlogo-white.png" alt="Dx-Labz"/></a></p>

      </div>
      <!-- /.container -->
      <button onclick="topFunction()" id="scroll-to-top" title="Go to top">&#11165;<span>FEL</span></button>
    </footer>

	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" async></script>
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" defer></script>
  <!-- <script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js"></script> -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
  <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
  <script>
    //Get the button:
      mybutton = document.getElementById("scroll-to-top");

      // When the user scrolls down 20px from the top of the document, show the button
      window.onscroll = function() {scrollFunction()};

      function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
          mybutton.style.display = "block";
        } else {
          mybutton.style.display = "none";
        }
      }

      // When the user clicks on the button, scroll to the top of the document
      function topFunction() {
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
      }
  </script>
  <script>
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
         var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
          panel.style.display = "none";
        } else {
          panel.style.display = "block";
        }
      });
    }
</script>

	<?php wp_footer(); ?>
</body>
</html>
