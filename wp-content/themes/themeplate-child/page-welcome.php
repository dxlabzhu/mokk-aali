<?php

/**
 * Template Name: Köszöntő
 *
 * @package ThemePlate
 * @since 0.1.0
 */

get_header();

?>
<div id="page-welcome">
	<main class="content container">
		<?php if ( ! is_front_page() ) : ?>
			<div class="page-header">
				<?php echo get_the_post_thumbnail( $post->ID, 'medium', ['class' => 'featured-img'] ); ?>
				<div class="title-box">
					<?php picture('slash-pattern-middle', 'png', '',  true, 'slash-pattern_right_top'); ?>
			      	<h3 class="section-title"><?php single_post_title(); ?><div class="cube"></div><div class="cube"></div><div class="cube"></div></h3>
					<h2 class="section-sub-title">Dr. Parti Tamás Intézetvezető</h2>
					<?php picture('grey-pattern', 'png', '',  true, 'grey-pattern'); ?>
					<?php picture('slash-pattern-middle', 'png', '',  true, 'slash-pattern_right_bottom'); ?>
				</div>
			</div>

		<?php endif; ?>

		<?php while ( have_posts() ) : ?>
			<?php the_post(); ?>
			<div class="main-content">
				<?php the_content(); ?>
				<?php picture('grey-pattern', 'png', '',  true, 'grey-pattern'); ?>
                <?php picture('slash-pattern-right-blue', 'png', 'Dr. ',  true, 'slash-pattern_right_blue'); ?>
			</div>
		<?php endwhile; ?>

	</main><!-- .content -->
</div>
<?php

get_footer();
